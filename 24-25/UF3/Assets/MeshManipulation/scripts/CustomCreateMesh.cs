using UnityEditor;
using UnityEngine;


namespace m17.uf3
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class CustomCreateMesh : MonoBehaviour
    {
        public void CreateMesh(int width, int height, float unitsPerTile)
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            if(mesh is null)
                mesh = new();
            else
                mesh.Clear();

            Vector3[] vertices = new Vector3[height * width];
            Vector2[] uv = new Vector2[height * width];
            for (int x = 0; x < height; x++)
                for (int z = 0; z < width; z++)
                {
                    vertices[x * width + z] = new Vector3(x, 0, z);
                    uv[x * width + z] = new Vector2(z / ((float)width-1), x / ((float)height-1));
                }

            mesh.vertices = vertices;
            mesh.uv = uv;

            int[] triangles = new int[(height-1) * (width-1) * 2 * 3];
            for (int x = 0; x < (height-1); x++)
                for (int z = 0; z < (width-1); z++)
                {
                    triangles[x * ((width - 1) * 6) + z * 6] = x * width + z;
                    triangles[x * ((width - 1) * 6) + z * 6 + 1] = (x + 1) * width + z + 1;
                    triangles[x * ((width - 1) * 6) + z * 6 + 2] = (x + 1) * width + z;

                    triangles[x * (width - 1) * 6 + z * 6 + 3] = x * width + z;
                    triangles[x * (width - 1) * 6 + z * 6 + 4] = x * width + z + 1;
                    triangles[x * (width - 1) * 6 + z * 6 + 5] = (x + 1) * width + z + 1;
                }

            mesh.triangles = triangles;

            mesh.RecalculateNormals();
        }
    }

    //Custom editor
    [CustomEditor(typeof(CustomCreateMesh))]
    public class LevelScriptEditor : Editor 
    {
        private int Width;
        private int Height;
        private float UnitsToTile;
        public override void OnInspectorGUI()
        {
            CustomCreateMesh componentCreateMesh = (CustomCreateMesh)target;
            
            Height = EditorGUILayout.IntField("Height", Height);
            Width = EditorGUILayout.IntField("Width", Width);
            UnitsToTile = EditorGUILayout.FloatField("Units To Tile", UnitsToTile);

            if(GUILayout.Button("Create Mesh"))
                if(Width > 0 && Height > 0 && UnitsToTile > 0)
                    componentCreateMesh.CreateMesh(Width, Height, UnitsToTile);
        }
    }
}

