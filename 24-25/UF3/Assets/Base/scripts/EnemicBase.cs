using UnityEngine;
using UnityEngine.AI;

namespace m17.uf3
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemicBase : MonoBehaviour
    {
        [SerializeField] private PlayerBase _Player;

        private NavMeshAgent _Agent;

        private void Awake()
        {
            _Agent = GetComponent<NavMeshAgent>();

            _Player.OnSetDestination += SetDestination;
        }

        private void SetDestination(Vector3 destination)
        {
            _Agent.SetDestination(destination);
        }
    }
}

