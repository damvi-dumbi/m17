using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace m17.uf3
{
    public class RagdollController : MonoBehaviour
    {
        [SerializeField] private Animator _Animator;
        [SerializeField] private Rigidbody _PushboxRB;
        [SerializeField] private Transform _Root;
        private Rigidbody[] _Rigidbodies;

        private void Awake()
        {
            Debug.Assert(_Root is not null, $"Sel·lecciona un root pel ragdoll.");
            _Rigidbodies = _Root.GetComponentsInChildren<Rigidbody>();

            Debug.Assert(_Animator is not null, $"Sel·lecciona un animator.");
            Debug.Assert(_PushboxRB is not null, $"Sel·lecciona el rigidbody del pushbox.");

            SetRagdoll(false);
        }

        [SerializeField] private float _ImpulseForce = 3;
        public void SetRagdoll(bool active)
        {
            _PushboxRB.isKinematic = active;
            _Animator.enabled = !active;

            foreach(Rigidbody rb in _Rigidbodies)
                rb.isKinematic = !active;

            
            //donem-li alegria al ragdoll
            if(active)
            {
                Vector3 forceVector = new Vector3(Random.Range(-1f,1f), Random.Range(-1f,1f), Random.Range(-1f,1f)).normalized;
                Rigidbody randomRB = _Rigidbodies[Random.Range(0, _Rigidbodies.Length)];
                randomRB.AddForce(forceVector * _ImpulseForce, ForceMode.VelocityChange);
                Debug.DrawRay(randomRB.transform.position + randomRB.centerOfMass, forceVector, Color.magenta, 3f);
            }
        }
        
    }
}

