using UnityEngine;
using UnityEngine.InputSystem;

namespace uf2
{
    public class GameObjectTeleporter : MonoBehaviour
    {
        [SerializeField]
        private InputActionAsset _InputAsset;
        private InputActionAsset _Input;
        private InputAction _PointerPosition;
        [SerializeField]
        [Tooltip("The teleportable object's layer")]
        private LayerMask _LayerMask;

        private GameObject _SelectedGameObject;

        private void Awake()
        {
            Debug.Assert(_InputAsset is not null, $"Input asset no assignat.");
            _Input = Instantiate(_InputAsset);

            _PointerPosition = _Input.FindActionMap("Player").FindAction("MousePosition");
            _Input.FindActionMap("Player").FindAction("Attack").performed  += OnTargetGameObject;
            _Input.FindActionMap("Player").FindAction("Attack2").performed += OnTeleportGameObject;
            _Input.FindActionMap("Player").Enable();
        }

        private void OnDestroy()
        {
            _Input.FindActionMap("Player").FindAction("Attack").performed  -= OnTargetGameObject;
            _Input.FindActionMap("Player").FindAction("Attack2").performed -= OnTeleportGameObject;
            _Input.FindActionMap("Player").Disable();
        }

        private void OnTargetGameObject(InputAction.CallbackContext context)
        {
            //coordenades de mouse
            Vector2 pointerPosition = _PointerPosition.ReadValue<Vector2>();

            //transformem de coordenades pantalla a raig per poder fer Raycast
            Ray worldPosition = Camera.main.ScreenPointToRay(pointerPosition);

            //comprovem si hem clicat sobre un objecte v�lid
            RaycastHit2D hit = Physics2D.Raycast(worldPosition.origin, worldPosition.direction, 20f, _LayerMask);
            if(hit.rigidbody != null)
                _SelectedGameObject = hit.rigidbody.gameObject;
        }

        private void OnTeleportGameObject(InputAction.CallbackContext context)
        {
            if(_SelectedGameObject == null)
            {
                Debug.LogError("GameObjectTeleporter: No has sel·leccionat cap objecte a teletransportar.");
                return;
            }

            //coordenades de mouse
            Vector2 pointerPosition = _PointerPosition.ReadValue<Vector2>();

            //transformem de coordenades pantalla a posici� m�n per a teletransportar
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(pointerPosition);

            _SelectedGameObject.TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody);
            if(rigidbody != null)
            {
                rigidbody.position = worldPosition; 
                rigidbody.velocity = Vector2.zero;
                rigidbody.angularVelocity = 0;
                rigidbody.rotation = 0f;
            }
        }
    }
}
