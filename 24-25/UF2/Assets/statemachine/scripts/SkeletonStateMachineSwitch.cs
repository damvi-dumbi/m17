using UnityEngine;
using UnityEngine.InputSystem;

namespace uf2
{
    [RequireComponent(typeof(Animator))]
    public class SkeletonStateMachineSwitch : MonoBehaviour
    {
        private Animator _Animator;
        [SerializeField] private InputActionAsset _ActionAsset;
        private InputActionAsset _InputAction;

        [SerializeField] private AnimationClip _AttackClip;
        [SerializeField] private AnimationClip _Attack2Clip;
        private void Awake()
        {
            _Animator = GetComponent<Animator>();
            Debug.Assert(_ActionAsset is not null, $"No has seleccionat input asset.");

            _InputAction = Instantiate(_ActionAsset);

            _InputAction.FindActionMap("Player").FindAction("Attack").performed  += OnAttack;
            _InputAction.FindActionMap("Player").FindAction("Attack2").performed += OnAttackStrong;

            _InputAction.FindActionMap("Player").Enable();
        }

        private enum SkeletonStates { NULL, IDLE, PUNCH, MEGAPUNCH }
        [SerializeField] private SkeletonStates _CurrentState;
        [SerializeField] private float _StateTime;
        private bool _ComboAvailable;

        public void StartCombo()
        {
            _ComboAvailable = true;
        }

        public void EndCombo()
        {
            _ComboAvailable = false;
        }

        private void Start()
        {
            ChangeState(SkeletonStates.IDLE);
        }

        private void ChangeState(SkeletonStates newState)
        {
            //tornar al mateix estat o no
            if (newState == _CurrentState)
                return;

            ExitState(_CurrentState);
            InitState(newState);
        }

        private void InitState(SkeletonStates initState)
        {
            _CurrentState = initState;
            _StateTime = 0f;

            switch (_CurrentState)
            {
                case SkeletonStates.IDLE:
                    _Animator.Play("IdleSM");
                    break;
                case SkeletonStates.PUNCH:
                    _Animator.Play("attackSM");
                    break;
                case SkeletonStates.MEGAPUNCH:
                    _Animator.Play("attack2SM");
                    break;
                default:
                    break;
            }
        }


        private void UpdateState(SkeletonStates updateState)
        {
            _StateTime += Time.deltaTime;

            switch (updateState)
            {
                case SkeletonStates.IDLE:
                    break;

                case SkeletonStates.PUNCH:
                    if (_StateTime >= _AttackClip.length)
                        ChangeState(SkeletonStates.IDLE);
                    break;

                case SkeletonStates.MEGAPUNCH:
                    if (_StateTime >= _Attack2Clip.length)
                        ChangeState(SkeletonStates.IDLE);
                    break;
                default:
                    break;
            }
        }

        private void ExitState(SkeletonStates exitState)
        {
            switch (exitState)
            {
                case SkeletonStates.IDLE:
                    break;
                case SkeletonStates.PUNCH:
                    _ComboAvailable = false;
                    break;
                case SkeletonStates.MEGAPUNCH:
                    break;
                default:
                    break;
            }
        }
        private void OnAttack(InputAction.CallbackContext context)
        {
            switch (_CurrentState)
            {
                case SkeletonStates.IDLE:
                    ChangeState(SkeletonStates.PUNCH);
                    break;
                case SkeletonStates.PUNCH:
                    if (_ComboAvailable)
                        ChangeState(SkeletonStates.MEGAPUNCH);
                    break;
                case SkeletonStates.MEGAPUNCH:
                    break;
                default:
                    break;
            }
        }
        private void OnAttackStrong(InputAction.CallbackContext context)
        {
            switch (_CurrentState)
            {
                case SkeletonStates.IDLE:
                    ChangeState(SkeletonStates.MEGAPUNCH);
                    break;
                case SkeletonStates.PUNCH:
                    break;
                case SkeletonStates.MEGAPUNCH:
                    break;
                default:
                    break;
            }
        }

        private void Update()
        {
            UpdateState(_CurrentState);
        }

    }
}