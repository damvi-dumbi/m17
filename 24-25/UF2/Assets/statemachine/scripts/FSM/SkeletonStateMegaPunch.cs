using UnityEngine;
using FiniteStateMachine;
using NUnit.Framework;

namespace uf2
{
    public class SkeletonStateMegaPunch : IState
    {
        private FSM _StateMachine = null;
        public FSM StateMachine
        {
            get => _StateMachine;
            set
            {
                if (_StateMachine == null)
                    _StateMachine = value;
                else
                    Debug.Assert(false, $"La State Machine ha estat assignada previament");
            }
        }

        private SkeletonStateMachineFSM _Character;
        private float _StateTime = 0f;

        public SkeletonStateMegaPunch(SkeletonStateMachineFSM character)
        {
            _Character = character;
        }

        public void Init()
        {
            _Character.GetComponent<Animator>().Play("attack2SM");
            _StateTime = 0f;
        }

        public void Update()
        {
            _StateTime += Time.deltaTime;
            if(_StateTime >= _Character.Attack2Clip.length)
                _StateMachine.ChangeState<SkeletonStateIdle>();
        }

        public void Exit()
        {
        }
    }

}
