using FiniteStateMachine;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

namespace uf2
{
    public class SkeletonStateMachineFSM : MonoBehaviour
    {
        [SerializeField] private InputActionAsset _ActionAsset;
        private InputActionAsset _InputAction;

        [SerializeField] private AnimationClip _AttackClip;
        public AnimationClip AttackClip => _AttackClip;
        [SerializeField] private AnimationClip _Attack2Clip;
        public AnimationClip Attack2Clip => _Attack2Clip;

        private FSM _FiniteStateMachine;

        private void Awake()
        {
            Debug.Assert(_ActionAsset is not null, $"No has seleccionat input asset.");

            _InputAction = Instantiate(_ActionAsset);

            _InputAction.FindActionMap("Player").Enable();

            //creem la m�quina d'estats
            _FiniteStateMachine = new FSM(gameObject, _InputAction);

            //creem els estats i els afegim
            _FiniteStateMachine.AddState(new SkeletonStateIdle(this));
            _FiniteStateMachine.AddState(new SkeletonStatePunch(this));
            _FiniteStateMachine.AddState(new SkeletonStateMegaPunch(this));
        }

        void Start()
        {
            //iniciem la m�quina d'estats
            _FiniteStateMachine.ChangeState<SkeletonStateIdle>();
        }

        private bool _ComboAvailable;
        public bool ComboAvailable => _ComboAvailable;

        public void StartCombo()
        {
            _ComboAvailable = true;
        }

        public void EndCombo()
        {
            _ComboAvailable = false;
        }

        private void Update()
        {
            _FiniteStateMachine.Update();
        }
    }
}