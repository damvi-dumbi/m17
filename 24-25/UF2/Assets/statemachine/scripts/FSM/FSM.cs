using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace FiniteStateMachine
{
    public interface IStateWithInputDependency
    {
        public enum InputPerformCondition { started, performed, cancelled };
        public struct InputKeyHook
        {
            string _ActionMap;
            public string ActionMap => _ActionMap;

            string _Action;
            public string Action => _Action;
            InputPerformCondition _Condition;

            public InputPerformCondition Condition => _Condition;


            private Action<InputAction.CallbackContext> _Function;
            public Action<InputAction.CallbackContext> Function => _Function;

            public InputKeyHook(string actionMap, string action, InputPerformCondition condition, Action<InputAction.CallbackContext> function)
            {
                _ActionMap = actionMap;
                _Action = action;
                _Condition = condition;
                _Function = function;
            }
        }

        public InputKeyHook[] InputHooks { get; }
    }
    public interface IState
    {
        public FSM StateMachine { get; set; }

        public void Init();
        public void Update();
        public void Exit();
    }

    public class FSM
    {
        private GameObject _Owner;
        public GameObject Owner => _Owner;

        private InputActionAsset _ActionAsset;

        private IState? _CurrentState = null;
        private List<IState> _States = new List<IState>();

        public FSM(GameObject owner, InputActionAsset actionAsset)
        {
            _Owner = owner;
            _ActionAsset = actionAsset;
        }

        private void RegisterStateInputDependencies(bool subscribe = true)
        {
            IStateWithInputDependency stateInputDependencies = _CurrentState as IStateWithInputDependency;
            if (stateInputDependencies == null)
                return;

            foreach (var inputHook in stateInputDependencies.InputHooks)
            {
                InputAction inputAction = _ActionAsset.FindActionMap(inputHook.ActionMap).FindAction(inputHook.Action);

                switch(inputHook.Condition)
                {
                    case IStateWithInputDependency.InputPerformCondition.started:
                        if(subscribe)
                            inputAction.started += inputHook.Function;
                        else
                            inputAction.started -= inputHook.Function;

                        break;

                    case IStateWithInputDependency.InputPerformCondition.performed:
                        if (subscribe) 
                            inputAction.performed += inputHook.Function;
                        else
                            inputAction.performed -= inputHook.Function;

                        break;

                    case IStateWithInputDependency.InputPerformCondition.cancelled:
                        if(subscribe)
                            inputAction.canceled += inputHook.Function;
                        else
                            inputAction.canceled -= inputHook.Function;

                        break;
                }
            }   
        }

        public void AddState(IState state)
        {
            Debug.Assert(!_States.Contains(state), $"L'estat {state} ja es troba dins la m�quina.");
            _States.Add(state);
            state.StateMachine = this;
        }

        public T GetState<T>() where T : IState
        {
            foreach (var state in _States)
                if (state is T)
                    return (T)state;

            Debug.Assert(false, $"L'estat {typeof(T)} cercat no existeix a la m�quina.");
            return default;
        }

        public void ChangeState<T>() where T : IState
        {
            T state = GetState<T>();

            RegisterStateInputDependencies(false);
            _CurrentState?.Exit();

            _CurrentState = state;

            RegisterStateInputDependencies();
            _CurrentState.Init();
        }

        public void Update()
        {
            _CurrentState?.Update();
        }
    }
}
