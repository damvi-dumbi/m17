using UnityEngine;
using FiniteStateMachine;
using NUnit.Framework;
using UnityEngine.InputSystem;

namespace uf2
{
    public class SkeletonStatePunch : IState, IStateWithInputDependency
    {
        private FSM _StateMachine = null;
        public FSM StateMachine
        {
            get => _StateMachine;
            set
            {
                if (_StateMachine == null)
                    _StateMachine = value;
                else
                    Debug.Assert(false, $"La State Machine ha estat assignada previament");
            }
        }

        private IStateWithInputDependency.InputKeyHook[] _InputHooks;
        public IStateWithInputDependency.InputKeyHook[] InputHooks => _InputHooks;

        private SkeletonStateMachineFSM _Character;
        private float _StateTime;

        public SkeletonStatePunch(SkeletonStateMachineFSM character)
        {
            _Character = character;

            _InputHooks = new IStateWithInputDependency.InputKeyHook[]
            {
                new IStateWithInputDependency.InputKeyHook("Player", "Attack", IStateWithInputDependency.InputPerformCondition.performed, OnAttack)
            };
        }

        private void OnAttack(InputAction.CallbackContext context)
        {
            if (_Character.ComboAvailable)
                _StateMachine.ChangeState<SkeletonStateMegaPunch>();
        }
        public void Init()
        {
            _Character.GetComponent<Animator>().Play("attackSM");
            _StateTime = 0f;
        }

        public void Update()
        {
            _StateTime += Time.deltaTime;
            if(_StateTime >= _Character.AttackClip.length)
                _StateMachine.ChangeState<SkeletonStateIdle>();
        }
        public void Exit()
        {
            _Character.EndCombo();
        }
    }
}

