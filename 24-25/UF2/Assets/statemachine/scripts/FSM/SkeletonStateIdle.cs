using UnityEngine;
using FiniteStateMachine;
using UnityEngine.InputSystem;

namespace uf2
{
    public class SkeletonStateIdle : FiniteStateMachine.IState, FiniteStateMachine.IStateWithInputDependency
    {
        private FSM _StateMachine = null;
        public FSM StateMachine
        {
            get => _StateMachine;
            set
            {
                if(_StateMachine == null)
                    _StateMachine = value;
                else
                    Debug.Assert(false, $"La State Machine ha estat assignada previament");
            }
        }

        private IStateWithInputDependency.InputKeyHook[] _InputHooks;
        public IStateWithInputDependency.InputKeyHook[] InputHooks => _InputHooks;

        private SkeletonStateMachineFSM _Character;

        public SkeletonStateIdle(SkeletonStateMachineFSM character)
        {
            _Character = character;
            //InputKeyHook( actionMap,  action, condition, function)

            _InputHooks = new IStateWithInputDependency.InputKeyHook[]
            {
                new IStateWithInputDependency.InputKeyHook("Player", "Attack", IStateWithInputDependency.InputPerformCondition.performed, OnAttack),
                new IStateWithInputDependency.InputKeyHook("Player", "Attack2", IStateWithInputDependency.InputPerformCondition.performed, OnAttack2)
            };
        }

        private void OnAttack(InputAction.CallbackContext context)
        {
            _StateMachine.ChangeState<SkeletonStatePunch>();
        }

        private void OnAttack2(InputAction.CallbackContext context)
        {
            _StateMachine.ChangeState<SkeletonStateMegaPunch>();
        }

        public void Init()
        {
            _Character.GetComponent<Animator>().Play("IdleSM");
        }

        public void Update() { }

        public void Exit() { }
    }
}

