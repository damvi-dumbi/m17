using Unity.VisualScripting;
using UnityEngine;

namespace uf2
{
    public class SubscripcioAEventGlobal : MonoBehaviour
    {
        void Start()
        {
            EventsGlobals.OnEventPausa += EventPausa;
            EventsGlobals.OnEventFinalPausa += EventFinalPausa;
        }

        private void EventPausa()
        {
            Debug.Log($"Event pausa rebut per {gameObject}");
        }

        private void EventFinalPausa(float valor)
        {
            Debug.Log($"Event float amb valor {valor} rebut per {gameObject}");
        }

        void OnDestroy()
        {
            //Alerta! Achtung! Warning! Zu yi! A!
            //imprescindible dessubcriure perquè EventsGlobals
            //és estàtic i per tant sempre existeix al programa
            //mentre aquest s'estigui executant.
            EventsGlobals.OnEventPausa -= EventPausa;
            EventsGlobals.OnEventFinalPausa -= EventFinalPausa;
        }
    }
}
