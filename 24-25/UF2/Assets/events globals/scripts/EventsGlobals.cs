using System;

namespace uf2
{
    //Aquesta classe existeix sempre, aneu amb compte al
    //fer-la servir.
    //Tot subscripció que feu es mantindrà durant l'execució
    //completa del programa. Per tant heu de gestionar degudament
    //les subscripcions i dessubscripcions als events o començareu
    //a cridar classes que ja no existeixen.
    public static class EventsGlobals
    {
        public static event Action OnEventPausa;
        public static void EventPausa() => OnEventPausa?.Invoke();
        public static event Action<float> OnEventFinalPausa;
        public static void EventFinalPausa(float value) => OnEventFinalPausa?.Invoke(value);
    }
}