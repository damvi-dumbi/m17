using UnityEngine;

namespace uf2
{
    public class GameManagerEvents : MonoBehaviour
    {
        private float _tempsIniciPausa;
        public void ActivarPausa()
        {
            _tempsIniciPausa = Time.fixedTime;

            EventsGlobals.EventPausa();
        }

        public void DesactivarPausa()
        {
            float tempsTranscorregut = Time.fixedTime - _tempsIniciPausa;

            EventsGlobals.EventFinalPausa(tempsTranscorregut);
        }
    }
}
