using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace uf2
{
    [CreateAssetMenu(fileName = "ScriptableCompra", menuName = "Scriptable Objects/ScriptableCompra")]
    public class ScriptableCompra : ScriptableObject
    {
        [SerializeField]
        private int TotalObjectes = 0;
        [SerializeField]
        private List<ScriptableExemple> Scriptables;
        public ReadOnlyCollection<ScriptableExemple> ScriptableExemples => Scriptables.AsReadOnly();

        public void AfegirALaCompra(ScriptableExemple comprat)
        {
            Scriptables.Add(comprat);
            TotalObjectes = Scriptables.Count;
        }

        public void CarregarDades(List<ScriptableExemple> scriptableExemples)
        {
            Scriptables = scriptableExemples;
            TotalObjectes = Scriptables.Count;
        }
    }
}
