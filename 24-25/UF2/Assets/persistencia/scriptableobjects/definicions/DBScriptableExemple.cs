using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace uf2
{
    [CreateAssetMenu(fileName = "DB Scriptable Exemple", menuName = "Scriptable Objects/DB Scriptables")]
    public class DBScriptableExemple : ScriptableObject
    {
        [SerializeField]
        List<ScriptableExemple> ScriptableExemples;

        public List<ScriptableExemple> FromIDs(int[] listIDs)
        {
            ScriptableExemple ScriptableFromID(int id)
            {
                foreach(ScriptableExemple currentScriptable in ScriptableExemples)
                {
                    if(currentScriptable.id == id)
                        return currentScriptable;
                }
                return null;
            }

            List<ScriptableExemple> scriptableExemples = new List<ScriptableExemple>(listIDs.Length);

            foreach(int id in listIDs)
                scriptableExemples.Add(ScriptableFromID(id));
            
            return scriptableExemples;
        }

        public int[] ToIDs(ReadOnlyCollection<ScriptableExemple> listScriptables)
        {
            int[] listIDs = new int[listScriptables.Count];

            for(int i = 0; i < listScriptables.Count; ++i)
            {
                listIDs[i] = listScriptables[i].id;
            }

            return listIDs;
        }
    }
}
