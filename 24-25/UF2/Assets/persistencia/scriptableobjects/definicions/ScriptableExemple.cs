using UnityEngine;

namespace uf2
{
    [CreateAssetMenu(fileName = "ScriptableExemple", menuName = "Scriptable Objects/ScriptableExemple")]
    public class ScriptableExemple : ScriptableObject
    {
        public int id;
        public Sprite sprite;
    }
}
