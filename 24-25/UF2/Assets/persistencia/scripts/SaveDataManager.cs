using System;
using System.IO;
using UnityEngine;

namespace uf2
{
    public class SaveDataManager : MonoBehaviour
    {
        private const string saveFileName = "savegame.json";

        [SerializeField] DBScriptableExemple _DBScriptables;
        [SerializeField] ScriptableCompra _ScriptableCompra;

        //Estructura que conté la informació a desar
        [Serializable]
        private class SaveDataInfo
        {
            public int[] ids;
        }

        public void SaveData()
        {
#if UNITY_EDITOR
            string filePath = saveFileName;
#else
            string filePath = Path.Combine(Application.persistentDataPath, saveFileName);
#endif

            SaveDataInfo savedata = new SaveDataInfo
            {
                ids = _DBScriptables.ToIDs(_ScriptableCompra.ScriptableExemples)
            };

            try
            {
                string jsonData = JsonUtility.ToJson(savedata);
                File.WriteAllText(filePath, jsonData);
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to save {filePath} with exception {e}");
            }
        }

        public void LoadData()
        {
#if UNITY_EDITOR
            string filePath = saveFileName;
#else
            string filePath = Path.Combine(Application.persistentDataPath, saveFileName);
#endif
            try
            {
                string jsonData = File.ReadAllText(filePath);

                SaveDataInfo saveData = JsonUtility.FromJson<SaveDataInfo>(jsonData);

                _ScriptableCompra.CarregarDades(_DBScriptables.FromIDs(saveData.ids));
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to load {filePath} with exception {e}");
            }
        }
    }
}
