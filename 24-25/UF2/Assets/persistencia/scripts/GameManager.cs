using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace uf2
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance {get; private set;}
        private SaveDataManager _SaveDataManager;

        //valor per ensenyar persistència
        [SerializeField]
        private string _Valor = "";
        public string Valor => _Valor;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Debug.Assert(false, $"GameManager ja declarat.");

            //Aquest objecte és persistent, sempre viu al canvi d'escenes.
            DontDestroyOnLoad(gameObject);

            //Volem saber quan es canvia d'escena, per tant ens hi subscrivim
            SceneManager.sceneLoaded += OnSceneLoaded;
            SceneManager.sceneUnloaded += OnSceneUnloaded;

            _SaveDataManager = GetComponent<SaveDataManager>();
        }

        private void Start()
        {
            SceneManager.LoadScene("Escena2");
        }

        //Es crida al carregar una escena
        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log($"GameManager - OnSceneLoaded: \"{scene.name}\".");
            switch(scene.name)
            {
                case "Escena2":
                    _SaveDataManager.LoadData();
                    break;
            }
        }

        void OnSceneUnloaded(Scene scene)
        {
            Debug.Log($"GameManager - OnSceneUnloaded: \"{scene.name}\".");
            switch(scene.name)
            {
                case "Escena1":
                    _SaveDataManager.SaveData();
                    break;
            }
        }
    }
}