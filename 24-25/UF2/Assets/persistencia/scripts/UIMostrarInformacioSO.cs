using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace uf2
{
    public class UIMostrarInformacioSO : MonoBehaviour
    {
        [SerializeField] private ScriptableExemple _Scriptable;
        [SerializeField] Image _Image;
        [SerializeField] TextMeshProUGUI _Text;

        public void MostrarInformacio(ScriptableExemple scriptable)
        {
            _Scriptable = scriptable;

            _Image.sprite = _Scriptable.sprite;
            _Text.text = _Scriptable.id.ToString();
        }
    }
}
