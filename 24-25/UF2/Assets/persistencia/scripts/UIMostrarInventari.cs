using UnityEngine;

namespace uf2
{
    public class UIMostrarInventari : MonoBehaviour
    {
        [SerializeField] ScriptableCompra _ScriptableCompra;
        [SerializeField] GameObject _MostraDInventari;
        void Start()
        {
            Debug.Assert(_MostraDInventari.GetComponent<UIMostrarInformacioSO>() is not null, $"L'element de mostrar informació no correpson amb el format esperat.");

            var scriptables = _ScriptableCompra.ScriptableExemples;
            foreach(ScriptableExemple scriptable in scriptables)
            {
                GameObject elementAMostrar = Instantiate(_MostraDInventari, transform);
                elementAMostrar.GetComponent<UIMostrarInformacioSO>().MostrarInformacio(scriptable);
            }
        }
    }
}
