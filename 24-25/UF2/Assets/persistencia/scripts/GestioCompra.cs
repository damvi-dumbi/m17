using UnityEngine;

namespace uf2
{
    public class GestioCompra : MonoBehaviour
    {
        [SerializeField] ScriptableCompra _ScriptableCompra;
        public void Comprar(ScriptableExemple scriptable)
        {
            _ScriptableCompra.AfegirALaCompra(scriptable);
        }
    }
}
