using UnityEngine;

namespace uf2
{
    public class UIButtonCompra : MonoBehaviour
    {
        [SerializeField] GestioCompra _GestioCompra;
        [SerializeField] ScriptableExemple _ScriptableExemple;
        [SerializeField] UIMostrarInformacioSO _MostraInfo;

        void Awake()
        {
            _MostraInfo.MostrarInformacio(_ScriptableExemple);
        }

        public void Comprar()
        {
            _GestioCompra.Comprar(_ScriptableExemple);
        }
    }
}
