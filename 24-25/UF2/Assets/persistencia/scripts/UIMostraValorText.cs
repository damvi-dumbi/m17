using TMPro;
using UnityEngine;


namespace uf2
{
    public class UIMostraValorText : MonoBehaviour
    {
        public void Awake()
        {
            GetComponent<TextMeshProUGUI>().text = GameManager.Instance.Valor;
        }
    }
}
