using UnityEngine;
using UnityEngine.SceneManagement;

namespace uf2
{
    public class UICanviEscena : MonoBehaviour
    {
        public void CanviEscena(string escena)
        {
            SceneManager.LoadScene(escena);
        }
    }
}
