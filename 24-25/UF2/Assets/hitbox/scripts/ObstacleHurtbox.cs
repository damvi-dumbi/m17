using System;
using UnityEngine;

namespace uf2
{
    public class ObstacleHurtbox : MonoBehaviour, IDamageable
    {
        [SerializeField] private int _DamageThreshold = 2;

        public event Action<float> OnDamaged;

        public void ReceiveDamage(float damage)
        {
            if(damage >= _DamageThreshold)
                Destroy(gameObject);
        }
    }
}

