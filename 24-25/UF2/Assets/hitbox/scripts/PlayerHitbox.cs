using System.Collections.Generic;
using System.Linq;
using uf2;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Animator))]
public class PlayerHitbox : MonoBehaviour
{
    private List<IAttack> _Hitboxes;
    [SerializeField] private InputActionAsset _ActionAsset;
    private InputActionAsset _InputAction;

    private Animator _Animator;

    [SerializeField] private int _DamageAttack1 = 1;
    [SerializeField] private int _DamageAttack2 = 2;

    private void Awake()
    {
        Debug.Assert(_ActionAsset is not null, $"No has seleccionat input asset.");
        
        _InputAction = Instantiate(_ActionAsset);

        _InputAction.FindActionMap("Player").FindAction("Attack").performed += OnAttack;
        _InputAction.FindActionMap("Player").FindAction("Attack2").performed += OnAttackStrong;


        _InputAction.FindActionMap("Player").Enable();


        _Animator = GetComponent<Animator>();
        _Hitboxes = transform.GetComponentsInChildren<IAttack>(true).ToList<IAttack>();
    }

    private void OnAttack(InputAction.CallbackContext context)
    {
        foreach(IAttack hitbox in _Hitboxes)
            hitbox.Damage = _DamageAttack1;

        _Animator.Play("attack");
    }
    private void OnAttackStrong(InputAction.CallbackContext context)
    {
        foreach(IAttack hitbox in _Hitboxes)
            hitbox.Damage = _DamageAttack2;
            
        _Animator.Play("attack2");
    }

    private void OnDestroy()
    {
        if(_InputAction == null)
            return;

        _InputAction.FindActionMap("Player").FindAction("Attack").performed -= OnAttack;
        _InputAction.FindActionMap("Player").FindAction("Attack2").performed -= OnAttackStrong;
    }
}
