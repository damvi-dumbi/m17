using System;
using UnityEngine;

namespace uf2
{
    public interface IDamageable
    {
        public event Action<float> OnDamaged;
        void ReceiveDamage(float damage);
    }

    public interface IAttack
    {
        public int Damage { set; }
    }
}
