using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

namespace uf2
{
    public class Hitbox : MonoBehaviour, IAttack
    {
        private int _Damage = 0;
        public int Damage
        {
            set => _Damage = value;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.TryGetComponent<IDamageable>(out IDamageable objective))
                objective.ReceiveDamage(_Damage);
        }
    }
}

