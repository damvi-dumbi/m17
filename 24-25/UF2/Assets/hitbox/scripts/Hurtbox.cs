using System;
using UnityEngine;

namespace uf2
{
    public class Hurtbox : MonoBehaviour, IDamageable
    {

        [SerializeField] [Range(0f,5f)] private float _DamageMultiplier = 1.0f;
        public event Action<float> OnDamaged;

        public void ReceiveDamage(float damage)
        {
            OnDamaged?.Invoke(damage*_DamageMultiplier);
        }
    }
}

