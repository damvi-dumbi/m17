using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace uf2
{
    public class EnemyHitbox : MonoBehaviour
    {
        [SerializeField] private int _HitPoints = 10;

        private List<IDamageable> _Damageables;

        private void Awake()
        {
            _Damageables = transform.GetComponentsInChildren<IDamageable>(true).ToList<IDamageable>();

            _Damageables.ForEach(damageable => damageable.OnDamaged += ReceiveDamage );
        }
            
        private void ReceiveDamage(float damage)
        {
            _HitPoints -= (int)damage;

            if(_HitPoints <= 0)
                Destroy(gameObject);
        }

        private void OnDestroy()
        {
            _Damageables.ForEach(
                damageable =>
                {
                    if (damageable != null)
                        damageable.OnDamaged -= ReceiveDamage;
                });
        }
    }
}