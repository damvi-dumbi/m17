using UnityEngine;
using UnityEngine.InputSystem;

namespace uf2
{
    public class SkeletonCastExample : MonoBehaviour
    {
        [SerializeField] private InputActionAsset _ActionAsset;
        private InputActionAsset _InputAction;

        private void Awake()
        {
            Debug.Assert(_ActionAsset is not null, $"No has seleccionat input asset.");

            _InputAction = Instantiate(_ActionAsset);

            _InputAction.FindActionMap("Player").FindAction("Attack").performed += OnCircleCast;
            _InputAction.FindActionMap("Player").FindAction("Attack2").performed += OnRayCast;

            _InputAction.FindActionMap("Player").Enable();
        }

        [SerializeField] LayerMask _CastLayerMask;

        [SerializeField] Collider2D[] _CircleCastFound;
        [SerializeField] Collider2D _RayCastFound;
        private void OnCircleCast(InputAction.CallbackContext context)
        {
            _CircleCastFound = Physics2D.OverlapCircleAll(transform.position, 5f, _CastLayerMask);
        }

        private void OnRayCast(InputAction.CallbackContext context)
        {
            Vector2 mousePosition = _InputAction.FindActionMap("Player").FindAction("MousePosition").ReadValue<Vector2>();
            Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            Vector3 direction = mouseWorldPosition - transform.position;

            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, 5f, _CastLayerMask);

            if (hit.rigidbody != null)
            {
                Debug.DrawLine(transform.position, hit.point, Color.magenta, 3f);
                _RayCastFound = hit.collider;
            };
        }
    }
}

