using System.Collections;
using System.Collections.Generic;
using m17;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    [SerializeField] private MapGenerator _MapGenerator;
    [SerializeField] private GameObject _Quad;
    [SerializeField] private TextMeshProUGUI _QuadInfo;
    [SerializeField] private TextMeshProUGUI _Help;
    [SerializeField] private TextMeshProUGUI _Manual;


    private void Awake()
    {
        _Help.gameObject.SetActive(true);
        _Manual.gameObject.SetActive(false);

        _MapGenerator.OnToggleHelp      += ToggleHelp;
        _MapGenerator.OnToggleQuad      += ToggleQuad;
        _MapGenerator.OnTextureChanged  += SetQuadTexture;
    }

    private void OnDestroy()
    {
        if(_MapGenerator is null) return;

        _MapGenerator.OnToggleHelp      -= ToggleHelp;
        _MapGenerator.OnToggleQuad      -= ToggleQuad;
        _MapGenerator.OnTextureChanged  -= SetQuadTexture;
    }

    private void ToggleQuad()
    {
        _Quad.SetActive(!_Quad.activeSelf);
    }

    private void ToggleHelp()
    {
        _Help.gameObject.SetActive(!_Help.gameObject.activeSelf);
        _Manual.gameObject.SetActive(!_Manual.gameObject.activeSelf);
    }

    private void SetQuadTexture(Texture2D texture, int octave, int mode)
    {
        _Quad.GetComponent<RawImage>().texture = texture;
        _QuadInfo.text = $"Mode: {(mode == 0 ? "base" : "combinada")}\n" +
                         $"{(octave == 0 ? "resultat final" : "octava: " + octave)}";
    }
}
