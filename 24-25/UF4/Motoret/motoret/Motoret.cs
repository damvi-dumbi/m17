using System.Data;
using Raylib_cs;
namespace motoret;

public class Motoret
{
    private List<IGameObject> _GameObjects = new();
    public bool Finalitzar {get; set;}

    public void Inicialitzar(int amplada = 800, int alcada = 480, string nom = "Motoret Project", Color colorFons = default)
    {
        _GameObjects = new List<IGameObject>();
        Renderer.CreateWindow(amplada, alcada, nom, colorFons);
    }

    //TODO:
    // Aquesta funció l'heu de refer vosaltres. Ara mateix afegeix objectes directament a la llista.
    // Per tant, si afegiu objectes durant l'execució, estareu afegint objectes dins una llista a la vegada
    //que recorreu aquesta llista.
    public void AfegirGO(IGameObject go)
    {
        _GameObjects.Add(go);
    }

    public void Correr()
    {
        Finalitzar = false;
        float deltaTime = 0;
        while(!Finalitzar && !Raylib.WindowShouldClose())
        {
            //TODO: calcular deltaTime segons frame anterior

            //recórrer la llista de GO per a Update
            foreach(IGameObject go in _GameObjects)
                go.Update(deltaTime);

            //recórrer la llista de GO per a Render
            Render();

            //TODO: Treure elements de la llista de GO

            //TODO: Afegir elements a la llista de GO

            //TODO: Fer una petita espera o esperar a una interrupció per tal d'acabar el frame actual
            Thread.Sleep(1);
        }
    }

    private void Render()
    {
        Renderer.InitRender();

        foreach(IGameObject go in _GameObjects)
            go.Render();

        Renderer.EndRender();
    }

    public void Aturar()
    {
        foreach(IGameObject go in _GameObjects)
            go.Dispose();
        _GameObjects.Clear();

        Renderer.CloseWindow();
    }
}