namespace motoret;

public interface IGameObject
{
    public void Update(float deltaTime);
    public void Render();
    public void Dispose();
}