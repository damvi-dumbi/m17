﻿using motoret;

namespace m17;

class ElVostreMain
{
    public static void Main()
    {
        Motoret motoret = new Motoret();

        //Inicialització de la finestra i elements interns del motor
        motoret.Inicialitzar(640, 480, "El vostre main", Raylib_cs.Color.White);

        //TODO: aqui creeu els vostres objectes que necessitareu a l'escena del joc i aneu-los afegint.
        motoret.AfegirGO(new ObjecteProva());

        //Executem el Run del motor i ara funcionarà el joc.
        motoret.Correr();

        //Quan alguna cosa interna del nostre joc aturi el motor "Motoret.Instancia.Finalitzar = true"
        //caldrà, doncs, que aturem el motor per tal que s'alliberin els recursos reservats
        motoret.Aturar();
    }
}

class ObjecteProva : IGameObject
{
    private int _OffsetX;
    private int _OffsetY;

    private int _Radi;
    public void Dispose()
    {
        Console.WriteLine("Esborrant GO");
    }

    public void Update(float deltaTime)
    {
        _OffsetX = (int)(Random.Shared.NextSingle() * 20)-10;
        _OffsetY = (int)(Random.Shared.NextSingle() * 20)-10;
        _Radi = (int)(Random.Shared.NextSingle() * 20)+100;
    }

    public void Render()
    {
        Renderer.DibuixarCercle(320+_OffsetX, 240+_OffsetY, _Radi, Raylib_cs.Color.Black);
    }
}