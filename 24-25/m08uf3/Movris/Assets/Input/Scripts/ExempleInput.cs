using UnityEngine;
using UnityEngine.InputSystem;

namespace m08
{
    public class ExempleInput : MonoBehaviour
    {
        [SerializeField] private InputActionAsset _ActionAsset;
        private InputActionAsset _Input;

        private InputAction _MousePosition;
        private InputAction _Gamepad;
        

        void Start()
        {
            //Creem una inst�ncia de l'input
            _Input = Instantiate(_ActionAsset);

            //subscripci� a l'event
            _Input.FindActionMap("Player").FindAction("ProvaBot�").started   += BotoPerformed;
            _Input.FindActionMap("Player").FindAction("ProvaBot�").performed += BotoPerformed;
            _Input.FindActionMap("Player").FindAction("ProvaBot�").canceled  += BotoPerformed;

            //desem unes refer�ncies als input per no haver de fer la cerca cada cop que les volguem
            _MousePosition = _Input.FindActionMap("Player").FindAction("Posici�Mouse");
            _Gamepad = _Input.FindActionMap("Player").FindAction("ProvaValor");

            //important, cal activar l'input que estar� funcional
            _Input.FindActionMap("Player").Enable();
        }

        private void BotoPerformed(InputAction.CallbackContext callbackContext)
        {
            //amb la fase veureu la difer�ncia entre si �s started o performed, perqu�
            //estem subscrits a ambd�s amb la mateixa funci�.
            Debug.Log(callbackContext.phase);
        }

        //Aqu� simplement estem llegint el valor del nostre joystick virtual
        private void Update()
        {
            //Debug.Log(_Gamepad.ReadValue<Vector2>());
        }


        //exemple de click a un element amb rigidbody i collider
        private void OnMouseDown()
        {
            Debug.Log("Hem clicat al player");
        }

        //Sempre que utilitzem subscripcions hem de ser responsables de desubscriure'ns
        private void OnDestroy()
        {
            //desubscripci� a l'event
            _Input.FindActionMap("Player").FindAction("ProvaBot�").started   -= BotoPerformed;
            _Input.FindActionMap("Player").FindAction("ProvaBot�").performed -= BotoPerformed;
            _Input.FindActionMap("Player").FindAction("ProvaBot�").canceled  -= BotoPerformed;
        }
    }
}


