using NUnit.Framework;
using TMPro;
using UnityEngine;

namespace m08
{
    public class OnShowText : MonoBehaviour
    {
        [SerializeField] private OnClickDelegate _OnClick;

        private TextMeshProUGUI _Text;
        private int _Puntuacio = 0;

        private void Awake()
        {
            _Text = GetComponent<TextMeshProUGUI>();

            if(_OnClick)
                _OnClick.OnClick += IncrementarPuntuacio;
        }

        public void IncrementarPuntuacio()
        {
            _Text.text = (++_Puntuacio).ToString();
        }

        private void OnDestroy()
        {
            if (_OnClick)
                _OnClick.OnClick -= IncrementarPuntuacio;
        }
    }
}

