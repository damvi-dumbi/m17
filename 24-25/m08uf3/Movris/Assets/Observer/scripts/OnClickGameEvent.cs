using System;
using UnityEngine;

namespace m08
{
    public class OnClickGameEvent : MonoBehaviour
    {
        [SerializeField] private GameEvent _Event;

        private void OnMouseDown()
        {
            _Event.Raise();
        }
    }
}

