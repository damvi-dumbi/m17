using System;
using UnityEngine;

namespace m08
{
    public class OnClickDelegate : MonoBehaviour
    {
        public event Action OnClick;

        private void OnMouseDown()
        {
            OnClick?.Invoke();
        }
    }
}

