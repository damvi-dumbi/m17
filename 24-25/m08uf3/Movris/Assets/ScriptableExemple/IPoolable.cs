using System;
using UnityEngine;

namespace m08
{
    public interface IPoolable
    {
        public event Action<GameObject> OnReturn;
    }

}