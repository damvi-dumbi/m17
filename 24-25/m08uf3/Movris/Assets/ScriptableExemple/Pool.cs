using NUnit.Framework;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace m08
{
    public class Pool : MonoBehaviour
    {
        [SerializeField] private GameObject _Prefab;
        [SerializeField] private int _Size;
        
        private struct PoolableGameObject
        {
            public GameObject gameObject;
            public bool availability;
        }

        private PoolableGameObject[] _Pool;

        private void Awake()
        {
            Assert.IsFalse(_Size <= 0);
            Assert.IsNotNull(_Prefab.GetComponent<IPoolable>());

            _Pool = new PoolableGameObject[_Size];

            for (int i = 0; i < _Size; i++)
            {
                GameObject poolable = Instantiate(_Prefab, transform);
                poolable.SetActive(false);

                _Pool[i].gameObject   = poolable;
                _Pool[i].availability = true;
            }
        }

        public GameObject GetElement()
        {
            for (int i = 0; i < _Size; i++)
            {
                if (_Pool[i].availability)
                {
                    _Pool[i].availability = false;
                    return _Pool[i].gameObject;
                }
            }

            return null;
        }

        public void ReturnElement(GameObject element)
        {
            for (int i = 0; i < _Size; i++)
            {
                if (_Pool[i].gameObject == element)
                {
                    element.SetActive(false);
                    _Pool[i].availability = true;
                    return;
                }
            }

            Assert.Fail($"L'element {element} no s'ha trobat dins el pool {gameObject}/{this}");
        }
    }

}