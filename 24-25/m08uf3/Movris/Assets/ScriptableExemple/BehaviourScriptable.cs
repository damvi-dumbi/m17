using System;
using System.Collections;
using UnityEngine;

namespace m08
{
    [RequireComponent (typeof(Rigidbody2D))]
    [RequireComponent (typeof(SpriteRenderer))]
    public class BehaviourScriptable : MonoBehaviour, IPoolable
    {
        public event Action<GameObject> OnReturn;
        public void Init(ScriptableExemple info, Vector2 position)
        {
            transform.position = position;
            gameObject.SetActive(true);

            GetComponent<Rigidbody2D>().velocity = info.velocity;
            GetComponent<SpriteRenderer>().sprite = info.sprite;

            StartCoroutine(ReturnToPool());
        }

        private IEnumerator ReturnToPool()
        {
            yield return new WaitForSeconds(2f);
            gameObject.SetActive(false);
            OnReturn.Invoke(gameObject);
        }
    }
}