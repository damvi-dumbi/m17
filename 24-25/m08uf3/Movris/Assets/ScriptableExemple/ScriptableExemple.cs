using UnityEngine;
using UnityEngine.InputSystem;

namespace m08
{
    [CreateAssetMenu(fileName = "Scriptable Exemple", menuName = "Scriptable Objects/Exemple")]
    public class ScriptableExemple : ScriptableObject
    {
        public Vector2 velocity;
        public Sprite sprite;
    }
}

