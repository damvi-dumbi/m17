using UnityEngine;
using UnityEngine.InputSystem;

namespace m08
{
    [RequireComponent (typeof(Pool))]
    public class SpawnerScriptables : MonoBehaviour
    {
        [SerializeField] private InputActionAsset _InputActionAsset;
        private InputActionAsset _Input;
        private InputAction _PointerPosition;

        private Pool _Pool;

        [SerializeField] private ScriptableExemple _Scriptable1;
        [SerializeField] private ScriptableExemple _Scriptable2;
        [SerializeField] private ScriptableExemple _Scriptable3;

        private void Awake()
        {
            _Pool = GetComponent<Pool>();

            _Input = Instantiate(_InputActionAsset);
            _PointerPosition = _Input.FindActionMap("Player").FindAction("PosicióMouse");

            _Input.FindActionMap("Player").FindAction("Spawn1").performed += Spawn1;
            _Input.FindActionMap("Player").FindAction("Spawn2").performed += Spawn2;
            _Input.FindActionMap("Player").FindAction("Spawn3").performed += Spawn3;

            _Input.FindActionMap("Player").Enable();
        }

        private void Spawn(ScriptableExemple info, Vector2 position)
        {
            GameObject nouObjecte = _Pool.GetElement();

            if (nouObjecte == null)
                return;

            nouObjecte.GetComponent<BehaviourScriptable>().Init(info, position);
            nouObjecte.GetComponent<IPoolable>().OnReturn += ReturnToPool;
        }

        private void ReturnToPool(GameObject go)
        {
            go.GetComponent<IPoolable>().OnReturn -= ReturnToPool;
            _Pool.ReturnElement(go);
        }

        private Vector2 GetPointerPosition()
        {
            return Camera.main.ScreenToWorldPoint(_PointerPosition.ReadValue<Vector2>());
        }

        private void Spawn1(InputAction.CallbackContext context)
        {
            Spawn(_Scriptable1, GetPointerPosition());
        }

        private void Spawn2(InputAction.CallbackContext context)
        {
            Spawn(_Scriptable2, GetPointerPosition());
        }

        private void Spawn3(InputAction.CallbackContext context)
        {
            Spawn(_Scriptable3, GetPointerPosition());
        }

    }
}

