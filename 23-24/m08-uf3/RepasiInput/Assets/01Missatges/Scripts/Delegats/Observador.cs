using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    public class Observador : MonoBehaviour
    {
        [SerializeField]
        private Observable m_Observable;

        private SpriteRenderer m_Sprite;

        private void Awake()
        {
            m_Sprite = GetComponent<SpriteRenderer>();
        }

        private void EventObservat()
        {
            Debug.Log(string.Format("S'ha produit un event a {0} i s�c {1}",
                m_Observable.name,
                name));
            transform.position = new Vector3(Random.Range(-9f, 9f), Random.Range(-5f, 5f), 0);
            m_Sprite.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }

        private void OnEnable()
        {
            if (m_Observable)
                m_Observable.OnSucceit += EventObservat;
        }

        private void OnDisable()
        {
            if (m_Observable)
                m_Observable.OnSucceit -= EventObservat;
        }
    }
}
