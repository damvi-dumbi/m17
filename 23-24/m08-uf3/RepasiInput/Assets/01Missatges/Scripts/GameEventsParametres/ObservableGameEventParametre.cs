using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    public class ObservableGameEventParametre : MonoBehaviour
    {
        [SerializeField]
        private GameEventVector3 m_Event;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                m_Event.Raise(new Vector3(Random.Range(-1f,1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f))); // ==> if(OnSucceit != null) OnSucceit.Invoke();
        }
    }
}