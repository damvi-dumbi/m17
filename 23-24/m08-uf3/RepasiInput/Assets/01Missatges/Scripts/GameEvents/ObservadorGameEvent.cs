using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    public class ObservadorGameEvent : MonoBehaviour
    {
        private SpriteRenderer m_Sprite;

        private void Awake()
        {
            m_Sprite = GetComponent<SpriteRenderer>();
        }

        public void EventObservat()
        {
            Debug.Log(string.Format("S'ha produit un GameEvent i s�c {0}",
                name));
            transform.position = new Vector3(Random.Range(-9f, 9f), Random.Range(-5f, 5f), 0);
            m_Sprite.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }
    }
}
