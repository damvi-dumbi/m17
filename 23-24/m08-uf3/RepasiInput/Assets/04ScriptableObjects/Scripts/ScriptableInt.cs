using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    [CreateAssetMenu(fileName = "ScriptableInt", menuName = "Scriptable Objects/Scriptable Int")]
    public class ScriptableInt : ScriptableObject
    {
        [SerializeField]
        private int m_MaxValue;
        [SerializeField]
        private int m_CurrentValue;

        //getter per� en una propietat. Es diuen Accessors
        public int MaxValue => m_MaxValue;
        public int CurrentValue => m_CurrentValue;

        public void ModifyBy(int value)
        {
            m_CurrentValue += value;
            if (m_CurrentValue >= m_MaxValue) m_CurrentValue = m_MaxValue;
            if (m_CurrentValue < 0) m_CurrentValue = 0;
        }

        public void SetNewMaxValue(int value)
        {
            if (value > 0)
            {
                m_MaxValue = value;
                if (m_CurrentValue >= m_MaxValue) m_CurrentValue = m_MaxValue;
            }
        }

        public override string ToString()
        {
            return m_CurrentValue + "/" + m_MaxValue;
        }
    }
}