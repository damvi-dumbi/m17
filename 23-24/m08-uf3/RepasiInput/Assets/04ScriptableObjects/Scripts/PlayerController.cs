using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        private ScriptableInt m_HP;

        [SerializeField]
        private GameEvent m_OnValueChangedHP;

        [SerializeField]
        private ScriptableInt m_MP;

        [SerializeField]
        private GameEvent m_OnValueChangedMP;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
                ModifyScriptableInt(m_HP, Random.Range(0, 20), m_OnValueChangedHP);

            if (Input.GetKeyDown(KeyCode.S))
                ModifyScriptableInt(m_HP, Random.Range(-20, 0), m_OnValueChangedHP);
                

            if (Input.GetKeyDown(KeyCode.D))
                ModifyScriptableInt(m_MP, Random.Range(0, 20), m_OnValueChangedMP);

            if (Input.GetKeyDown(KeyCode.F))
                ModifyScriptableInt(m_MP, Random.Range(-20, 0), m_OnValueChangedMP);
        }

        private void ModifyScriptableInt(ScriptableInt scriptable, int value, GameEvent gameEvent)
        {
            scriptable.ModifyBy(value);
            gameEvent.Raise();
        }
    }
}
