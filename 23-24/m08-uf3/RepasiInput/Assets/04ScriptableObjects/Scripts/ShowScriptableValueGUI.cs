using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace m08m17
{
    public class ShowScriptableValueGUI : MonoBehaviour
    {
        [SerializeField]
        private ScriptableInt m_ScriptableInt;

        [SerializeField]
        private TextMeshProUGUI m_TextMeshProUGUI;

        void Start()
        {
            UpdateValue();
        }

        public void UpdateValue()
        {
            m_TextMeshProUGUI.text = m_ScriptableInt.ToString();
        }
    }
}

