using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace m08m17
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_ObjectePrefab;

        private void Awake()
        {
            Assert.IsNotNull(m_ObjectePrefab.GetComponent<ObjecteBehaviour>());
        }

        public void CreateObjecte(InformacioObjecte obj)
        {
            GameObject objecte = Instantiate(m_ObjectePrefab);
            objecte.transform.position = new Vector3(Random.Range(-9f,9f),
                                                     Random.Range(-2f,2f));

            objecte.GetComponent<ObjecteBehaviour>().Init(obj);
        }
    }
}