using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    [CreateAssetMenu(fileName = "GameEventObjecte", menuName = "06-InformacioObjecte/GameEvent - InformacioObjecte")]
    public class GameEventInfoObjecte : GameEvent<InformacioObjecte> { }
}
