using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager.Requests;
using UnityEngine;
using UnityEngine.Assertions;

namespace m08m17
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class ObjecteBehaviour : MonoBehaviour
    {
        private InformacioObjecte m_Info;
        private SpriteRenderer m_SpriteRenderer;
        private Rigidbody2D m_Rigidbody;

        private void Awake()
        {
            m_SpriteRenderer = GetComponent<SpriteRenderer>();
            m_Rigidbody = GetComponent<Rigidbody2D>();
        }

        public void Init(InformacioObjecte info)
        {
            Assert.IsNotNull(info);
            m_Info = info;

            transform.localScale = Vector3.one * info.Scale;

            m_SpriteRenderer.sprite = info.Image;
            m_SpriteRenderer.color = info.Color;

            m_Rigidbody.gravityScale = info.Gravity;
            m_Rigidbody.angularVelocity = info.AngularSpeed;
        }

        private void FixedUpdate()
        {
            if (transform.position.y < -10 || transform.position.y > 10)
                Destroy(gameObject);
        }
    }
}
