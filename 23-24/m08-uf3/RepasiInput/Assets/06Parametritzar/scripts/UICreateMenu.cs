using m08m17;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class UICreateMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject m_MenuItem;

    [SerializeField]
    private InformacioObjecte[] m_Infos;

    void Start()
    {
        Assert.IsNotNull(m_MenuItem.GetComponent<UIObjecte>());

        foreach(InformacioObjecte info in m_Infos)
        {
            GameObject go = Instantiate(m_MenuItem, transform);
            go.GetComponent<UIObjecte>().Init(info);
        }
    }
}
