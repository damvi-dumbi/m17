using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    [CreateAssetMenu(fileName = "Objecte", menuName = "06-InformacioObjecte/Objecte")]
    public class InformacioObjecte : ScriptableObject
    {
        [Header("Image Properties")]
        
        [SerializeField]
        private Sprite m_Image;
        public Sprite Image => m_Image;

        [SerializeField]
        private Color m_Color = Color.white;
        public Color Color => m_Color;
        
        [SerializeField]
        [Range(0.1f, 3f)]
        public float m_Scale = 1f;
        public float Scale => m_Scale;
        
        [Space(10)]

        [Header("Logic Properties")]
        
        [SerializeField]
        private float m_AngularSpeed = 0f;
        public float AngularSpeed => m_AngularSpeed;

        [SerializeField]
        private float m_Gravity = 1f;
        public float Gravity => m_Gravity;
    }
}
