using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace m08m17
{
    public class UIObjecte : MonoBehaviour
    {
        private InformacioObjecte m_Info;
        [SerializeField]
        private GameEventInfoObjecte m_GameEventInfo;

        [SerializeField]
        private Image m_Image;

        public void Init(InformacioObjecte info)
        {
            m_Info = info;
            m_Image.sprite = m_Info.Image;
            m_Image.color = m_Info.Color;
        }

        public void OnCreateObjecte()
        {
            m_GameEventInfo.Raise(m_Info);
        }
    }
}