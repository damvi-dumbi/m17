using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace m08m17
{
    public class ScoreSceneController : MonoBehaviour
    {
        private int m_Score = 0;

        [SerializeField]
        private TextMeshProUGUI m_ScoreText;
        private void Awake()
        {
            m_ScoreText.text = m_Score.ToString();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                AddScore(Random.Range(0, 200));

            if (Input.GetKeyDown(KeyCode.Escape))
                EndLevel();
        }

        private void AddScore(int score)
        {
            m_Score += score;
            m_ScoreText.text = m_Score.ToString();
        }

        private void EndLevel()
        {
            GameManager.Instance.TopScore = m_Score;
            GameManager.Instance.ChangeScene(GameManager.MenuScene);
        }
    }
}
