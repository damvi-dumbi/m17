using m08m17;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace m08m17
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI m_MaxScoreText;

        private void Awake()
        {
            m_MaxScoreText.text = GameManager.Instance.TopScore.ToString();
        }

        public void OnChangePlay()
        {
            GameManager.Instance.ChangeScene(GameManager.PlayScene);
        }
    }
}