using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;

namespace m08m17
{
    public class ChonkusTilewalker : MonoBehaviour
    {

        [SerializeField]
        private InputActionAsset m_InputAsset;
        private InputActionAsset m_Input;

        private InputAction m_MovementAction;
        private InputAction m_PointerPosition;

        private Rigidbody2D m_Rigidbody;
        private float m_Speed = 3f;

        private int m_WaterLayer = 0;

        [SerializeField]
        private Tilemap m_Tilemap;

        // Start is called before the first frame update
        void Start()
        {
            m_Rigidbody = GetComponent<Rigidbody2D>();

            m_WaterLayer = LayerMask.NameToLayer("Water");

            m_Input = Instantiate(m_InputAsset);
            m_MovementAction = m_Input.FindActionMap("Default").FindAction("MovementAction");
            //exemple tilemap
            m_Input.FindActionMap("Default").FindAction("ButtonAction").performed += ActionPerformed;
            m_PointerPosition = m_Input.FindActionMap("Default").FindAction("PointerPosition");

            m_Input.FindActionMap("Default").Enable();
        }

        private void ActionPerformed(InputAction.CallbackContext context)
        {
            //coordenades de mouse
            Vector2 pointerPosition = m_PointerPosition.ReadValue<Vector2>();

            //transformem de coordenades pantalla a coordenades del tilemap
            Vector3Int tilePosition = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(pointerPosition));
            tilePosition.z = 0; //si no agafa el valor z de la -c�mera

            //agafem el tile que es troba a les coordenades de tilemap desitjades
            TileBase tile = m_Tilemap.GetTile(tilePosition);
            string tilename = "null";
            if (tile != null)
                tilename = tile.name;

            Debug.Log(string.Format("Trobat el tile {0} a casella {1}",
                tilename,
                tilePosition));
        }

        Vector2 m_Movement = Vector2.zero;
        // Update is called once per frame
        void Update()
        {
            m_Movement = m_MovementAction.ReadValue<Vector2>();
        }

        private void FixedUpdate()
        {
            m_Rigidbody.velocity = m_Movement * m_Speed;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.attachedRigidbody.gameObject.layer == m_WaterLayer)
                Debug.Log("MEOWWWW");
        }
    }
}
