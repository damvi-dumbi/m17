using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

namespace m08m17
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class InputActionExample : MonoBehaviour
    {
        [SerializeField]
        private InputActionAsset m_InputAsset;
        private InputActionAsset m_Input;
        private InputAction m_MovementAction;
        private InputAction m_PointerPosition;

        private SpriteRenderer m_SpriteRenderer;

        [SerializeField]
        private float m_MovementSpeed = 3f;

        [SerializeField]
        private LayerMask m_ActionLayerMask;


        void Awake()
        {
            m_SpriteRenderer = GetComponent<SpriteRenderer>();

            m_Input = Instantiate(m_InputAsset);

            m_Input.FindActionMap("Default").FindAction("ButtonAction").performed += ActionPerformed;
            m_Input.FindActionMap("Default").FindAction("ButtonAction").performed += ButtonAction;
            m_MovementAction = m_Input.FindActionMap("Default").FindAction("MovementAction");
            m_MovementAction.performed += ActionPerformed;
            m_PointerPosition = m_Input.FindActionMap("Default").FindAction("PointerPosition");

            //Existeixen altres fases de l'acci�
            m_Input.FindActionMap("Default").FindAction("ButtonAction").started += ActionPerformed;
            m_Input.FindActionMap("Default").FindAction("ButtonAction").canceled += ActionPerformed;

            m_Input.FindActionMap("Default").Enable();
        }

        private void ActionPerformed(InputAction.CallbackContext context)
        {
            Debug.Log(context);
        }

        private void ButtonAction(InputAction.CallbackContext context)
        {
            m_SpriteRenderer.color = new Color(Random.Range(0f,1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

            Vector2 pointerPosition = m_PointerPosition.ReadValue<Vector2>();
            //Raig que parteix de la c�mera i va cap a l'escena
            Ray cameraRay = Camera.main.ScreenPointToRay(pointerPosition);

            //Efectuem el raycast i apliquem filtre de layers
            RaycastHit2D hit = Physics2D.Raycast(cameraRay.origin, cameraRay.direction, Mathf.Infinity, m_ActionLayerMask);
            //Nom�s en cas d'impactar quelcom
            if(hit.rigidbody != null)
            {
                Debug.Log(string.Format("Hem tocat {0}", hit.rigidbody.name));
                
                //S� que nom�s s�n enemics perqu� he filtrat per layer
                MoususController mousus = hit.rigidbody.GetComponent<MoususController>();
                //Per� igualment podem fer la comprovaci�
                if (mousus != null)
                    mousus.OnRaycasted();
            }
            else
            {
                Debug.Log("No hem tocat res");
            }
        }

        void Update()
        {
            Vector2 delta = m_MovementAction.ReadValue<Vector2>();
            //Les coses f�siques no es mouen per position (a no ser que volguem un teleport)
            //Aix� �s codi antic de quan no hi havia f�sica. No feu aix� a les vostres cases o vindr� el llop.
            transform.position += m_MovementSpeed * new Vector3(delta.x, delta.y, 0) * Time.deltaTime;
        }
    }
}
