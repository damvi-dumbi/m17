using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m08m17
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class MoususController : MonoBehaviour
    {
        private SpriteRenderer m_SpriteRenderer;

        private void Awake()
        {
            m_SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void OnRaycasted()
        {
            Debug.Log(string.Format("I'VE BEEN RAYCASTED! {0}", name));
            m_SpriteRenderer.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }
    }
}
