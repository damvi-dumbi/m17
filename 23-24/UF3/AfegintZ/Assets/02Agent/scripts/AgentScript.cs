using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentScript : MonoBehaviour
{
    [SerializeField]
    private Transform m_Target;

    private NavMeshAgent m_Agent;

    private void Awake()
    {
        m_Agent = GetComponent<NavMeshAgent>();
    }
    
    private void Update()
    {
        if(!m_Agent.pathPending)
            m_Agent.SetDestination(m_Target.position);
    }
}
