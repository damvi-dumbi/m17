using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    public interface IDamageable
    {
        public event Action<int> OnDamage;
        void Damage(int amount);
    }

    public interface IPushable
    {
        void Push(Vector3 direction, float impulse);
    }
}