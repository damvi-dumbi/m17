using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using static UnityEditor.PlayerSettings;

namespace m17
{
    public class BaseCharController3D : MonoBehaviour
    {
        [SerializeField]
        private float m_Speed;

        [SerializeField]
        private float m_RotationSpeed = 180f;
        private float m_MouseSensitivity = 1f;

        [SerializeField]
        private float m_JumpHeight;

        CharacterController m_CharacterController;
        void Awake()
        {
            m_CharacterController = GetComponent<CharacterController>();
        }

        void Update()
        {
            //rotate
            if (Input.GetKey(KeyCode.A))
                transform.Rotate(-Vector3.up * m_RotationSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.D))
                transform.Rotate(Vector3.up * m_RotationSpeed * Time.deltaTime);

            Vector3 movement = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
                movement += transform.forward;
            if (Input.GetKey(KeyCode.S))
                movement -= transform.forward;

            //strafe
            if (Input.GetKey(KeyCode.Q))
                movement -= transform.right;
            if (Input.GetKey(KeyCode.E))
                movement += transform.right;

            movement.Normalize();
            movement *= m_Speed;

            if (m_CharacterController.isGrounded && Input.GetKey(KeyCode.Space))
            {
                movement.y = m_JumpHeight;
                Debug.Log(movement);
            }


            m_CharacterController.SimpleMove(movement);
        }
    }
}