using UnityEngine;

namespace m17
{
    public class BaseController3D : MonoBehaviour
    {
        [SerializeField]
        private float m_Speed = 3f;

        [SerializeField]
        private float m_RotationSpeed = 180f;
        private float m_MouseSensitivity = 1f;

        Vector3 m_Movement = Vector3.zero;
        private Rigidbody m_Rigidbody;

        //Camera
        [SerializeField]
        private GameObject m_Camera;
        [SerializeField]
        private bool m_InvertY = false;

        [SerializeField]
        private LayerMask m_ShootMask;

        private void Awake()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            //rotate
            if (Input.GetKey(KeyCode.LeftArrow))
                transform.Rotate(-Vector3.up * m_RotationSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.RightArrow))
                transform.Rotate(Vector3.up * m_RotationSpeed * Time.deltaTime);

            m_Movement = Vector3.zero;

            if (Input.GetKey(KeyCode.UpArrow))
                m_Movement += transform.forward;
            if (Input.GetKey(KeyCode.DownArrow))
                m_Movement -= transform.forward;

            //strafe
            if (Input.GetKey(KeyCode.Q))
                m_Movement -= transform.right;
            if (Input.GetKey(KeyCode.E))
                m_Movement += transform.right;

            if (Input.GetKey(KeyCode.PageUp))
                m_Camera.transform.Rotate((m_InvertY ? 1 : -1) * Vector3.right * m_RotationSpeed * Time.deltaTime);
            if (Input.GetKey(KeyCode.PageDown))
                m_Camera.transform.Rotate((m_InvertY ? -1 : 1) * Vector3.right * m_RotationSpeed * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.Keypad0))
                Shoot();
        }

        private void FixedUpdate()
        {
            //m_Rigidbody.MovePosition(transform.position + m_Movement.normalized * m_Speed * Time.fixedDeltaTime);
            m_Rigidbody.velocity = m_Movement.normalized * m_Speed + Vector3.up * m_Rigidbody.velocity.y;
        }

        private void Shoot()
        {
            RaycastHit hit;
            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, 20f, m_ShootMask))
            {
                Debug.Log($"He tocat {hit.collider.gameObject} a la posici� {hit.point} amb normal {hit.normal}");
                Debug.DrawLine(m_Camera.transform.position, hit.point, Color.green, 2f);

                if(hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    target.Damage(10);

                if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                    pushable.Push(m_Camera.transform.forward, 10);
            }
        }
    }

}