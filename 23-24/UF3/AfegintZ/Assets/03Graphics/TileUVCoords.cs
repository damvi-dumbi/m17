using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileUVCoords : MonoBehaviour
{
    [SerializeField] private float m_XTileAmount = 2;
    private void Awake()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        Vector2[] uvs = mesh.uv;
        for (int i = 0; i < uvs.Length; i++)
            if (uvs[i].x == 1)
                    uvs[i].x = m_XTileAmount;
        mesh.uv = uvs;

        for (int i = 0; i < mesh.uv.Length; i++)
            Debug.Log($"{i}: ({mesh.uv[i].x}, {mesh.uv[i].y})");
    }
}
