using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

readonly partial struct ProximityAspect : IAspect
{
    private readonly Entity entity;

    private readonly RefRO<LocalTransform> m_Transform;
    private readonly RefRO<ProximityTag> m_Tag;

    public bool CheckProximity(float3 point, float distanceSQ)
    {
        if (math.distancesq(point, m_Transform.ValueRO.Position) <= distanceSQ)
            return true;

        return false;
    }

    public void EnableInRangeComponent(bool enabled, EntityCommandBuffer ECB, ComponentLookup<IsInRangeTag> lookup)
    {
        if (lookup.IsComponentEnabled(entity) != enabled)
            ECB.SetComponentEnabled<IsInRangeTag>(entity, enabled);
    }
}
