using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;

[BurstCompile]
[UpdateAfter(typeof(MovementSystem))]
partial struct ProximitySystem : ISystem
{
    ComponentLookup<IsInRangeTag> m_Lookup;

    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        m_Lookup = state.GetComponentLookup<IsInRangeTag>();
        state.RequireForUpdate<ProximityTag>();
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {

    }

    //Estem accedint a Managed Objects (GameObjects) i per tant
    //No podem realitzar un burst compile :(
    //[BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        if (PlayerBehaviour.Instance == null)
        {
            state.Enabled = false;
            return;
        }

        //Aquest sistema far� canvis estructurals (afegir, treure, activar o crear components).
        //�s per aix� que necessitem una refer�ncia al commandBuffer.
        //En concret just despr�s de fer les simulacions, hi ha diversos segons la prefer�ncia.
        var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        EntityCommandBuffer ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

        //Agafar valors del player
        float playerDistanceSQ = PlayerBehaviour.Instance.RangeRadiusSQ;
        float3 playerPosition = PlayerBehaviour.Instance.Position;

        //cal actualitzar el ComponentLookup
        m_Lookup.Update(ref state);

        ProximityJob proximityJob = new ProximityJob
        {
            ECB = ecb,
            distanceSQ = playerDistanceSQ,
            point = playerPosition,
            lookup = m_Lookup
        };

        proximityJob.Schedule();
    }
}

[BurstCompile]
partial struct ProximityJob : IJobEntity
{
    public EntityCommandBuffer ECB;
    public float distanceSQ;
    public float3 point;
    public ComponentLookup<IsInRangeTag> lookup;

    [BurstCompile]
    void Execute(ProximityAspect aspect)
    {
        if(aspect.CheckProximity(point, distanceSQ))
            aspect.EnableInRangeComponent(true, ECB, lookup);
        
        else
            aspect.EnableInRangeComponent(false, ECB, lookup);
    }
}