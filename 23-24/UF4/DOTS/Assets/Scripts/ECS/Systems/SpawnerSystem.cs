using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;

[BurstCompile]
partial struct SpawnerSystem : ISystem
{ 
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        state.RequireForUpdate<Spawner>();
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        //No estem a MonoBehaviour, per tal d'accedir a les eines
        //que coneixeu del sistema, hem d'anar al SystemAPI
        float deltaTime = SystemAPI.Time.DeltaTime;

        // Utilitzem la coneguda sintaxi del foreach per a iterar per a totes les Entitats.
        // Al agafar components individuals (que no Aspects) cal indicar si fem Read Only o Read Write
        foreach (var spawner in SystemAPI.Query<SpawnerAspect>())
        {
            spawner.ElapseTime(deltaTime, state.EntityManager);
        }
    }
}