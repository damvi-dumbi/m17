using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[BurstCompile]
partial struct MovementSystem : ISystem
{
    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        //Si no hi ha res amb un Speed component, no s'executa el sistema
        state.RequireForUpdate<Speed>();
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {
    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        //No estem a MonoBehaviour, per tal d'accedir a les eines
        //que coneixeu del sistema, hem d'anar al SystemAPI.
        //A m�s dins la Query sou a Burst i no podeu accedir a la
        //SystemAPI.
        float deltaTime = SystemAPI.Time.DeltaTime;

        MovementJob movement = new MovementJob
        {
            deltaTime = deltaTime
        };

        //Podem paral�lelitzar
        movement.ScheduleParallel();
    }
}


[BurstCompile]
partial struct MovementJob : IJobEntity
{
    public float deltaTime;

    //Com a par�metres de l'execute vindriem a posar les condicions de cerca del nostre sistema:
    //in -> read-only
    //ref -> read-write
    //
    //En el nostre cas agafem l'aspecte que no cal dir com es passa perqu� ho t� configurat dins.
    [BurstCompile]
    void Execute(ref LocalTransform transform, in Speed speed)
    {
        float3 direction = speed.direction;
        transform.Position += new float3(direction.x, direction.y, direction.z) * speed.speed * deltaTime;
    }
}