using TMPro;
using Unity.Entities;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField]
    private float m_MovementSpeed = 5f;

    [SerializeField]
    private float m_RadiusIncreaseSpeed = 1f;

    [SerializeField]
    private Transform m_VisualRadius;

    [SerializeField]
    private TextMeshProUGUI m_Text;

    public static PlayerBehaviour Instance { get; internal set; }
    public float RangeRadiusSQ { get; internal set; }
    public Unity.Mathematics.float3 Position { get; internal set; }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        Position = transform.position;

        UpdateRadius();
    }

    private void Update()
    {
        Vector3 movement = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
            movement += Vector3.forward;
        if (Input.GetKey(KeyCode.S))
            movement -= Vector3.forward;
        if (Input.GetKey(KeyCode.A))
            movement -= Vector3.right;
        if (Input.GetKey(KeyCode.D))
            movement += Vector3.right;
        if(Input.GetKey(KeyCode.Q))
            movement += Vector3.up;
        if (Input.GetKey(KeyCode.E))
            movement -= Vector3.up;

        transform.position += movement.normalized * m_MovementSpeed * Time.deltaTime;
        Position = transform.position;

        if (Input.GetKey(KeyCode.F))
            ChangeRadius(-1);
        if (Input.GetKey(KeyCode.G))
            ChangeRadius(1);
    }

    private void LateUpdate()
    {
        EntityQuery inRangeEntitiesQuery = World.DefaultGameObjectInjectionWorld.EntityManager.CreateEntityQuery(typeof(IsInRangeTag));
        Unity.Collections.NativeArray<Entity> inRangeEntities = inRangeEntitiesQuery.ToEntityArray(Unity.Collections.Allocator.Temp);
        
        m_Text.text = inRangeEntities.Length.ToString();
        /*foreach(Entity entity in inRangeEntities)
        {
            Vector3 entityPosition= World.DefaultGameObjectInjectionWorld.EntityManager.GetComponentData<LocalToWorld>(entity).Position;
            Debug.DrawLine(transform.position, entityPosition, Color.green);
        }*/
    }

    private void UpdateRadius()
    {
        RangeRadiusSQ = m_VisualRadius.localScale.x / 2;
        RangeRadiusSQ *= RangeRadiusSQ;
    }

    private void ChangeRadius(int increase)
    {
        if(m_VisualRadius.localScale.x > 1f)
        {
            m_VisualRadius.localScale = m_VisualRadius.localScale + Vector3.one * m_RadiusIncreaseSpeed * increase * Time.deltaTime;
        }
        UpdateRadius();
    }
}
