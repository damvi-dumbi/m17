using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEditor.PackageManager.UI;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace m17
{
    public class MapGenerator : MonoBehaviour
    {
        [SerializeField]
        private bool m_Verbose = false;

        //graphic
        [SerializeField]
        private Gradient m_Gradient;
        [SerializeField]
        private Material m_MeshMaterial;
        private Texture2D m_MeshTexture;
        private Texture2D[] m_Texture;
        private float[][,] m_Heights;

        //internal logic
        private int m_TextureMode = 0;
        private int m_TextureOctave = 0;

        //texture GUI
        [SerializeField]
        private GameObject m_GUI;
        [SerializeField]
        [Min(0.1f)]
        [Tooltip("Seconds in order to generate map")]
        private float m_GenerationTime = 1f;

        [Header("Size")]
        //size of the area we will paint
        [SerializeField]
        [Min(1)]
        private int m_Width = 100;
        [Min(1)]
        [SerializeField]
        private int m_Height = 100;
        [SerializeField]
        [Min(0.1f)]
        private float m_Amplitude = 10f;

        [Header("Base Parameters")]
        [SerializeField]
        //offset from the perlin map
        private float m_OffsetX;
        [SerializeField]
        private float m_OffsetY;
        [SerializeField]
        private float m_Frequency = 4f;

        //octaves
        private const int MAX_OCTAVES = 8;
        [Header("Octave Parameters")]
        [SerializeField]
        [Range(0, MAX_OCTAVES)]
        private int m_Octaves = 0;
        [Range(2, 3)]
        [SerializeField]
        private int m_Lacunarity = 2;
        [SerializeField]
        [Range(0.1f, 0.9f)]
        private float m_Persistence = 0.5f;
        [Tooltip("Do the octaves carve the terrain?")]
        [SerializeField]
        private bool m_Carve = true;

        void Start()
        {
            //Crearem una textura per cada perlin i les seves octaves amb el resultat conjunt
            //i un altre amb el resultat base.
            m_Texture = new Texture2D[(MAX_OCTAVES + 1) * 2];
            for (int i = 0; i < (MAX_OCTAVES + 1) * 2; i++)
            {
                m_Texture[i] = new Texture2D(m_Width, m_Height);
                m_Texture[i].filterMode = FilterMode.Point;
            }

            m_MeshTexture = new Texture2D(m_Width, m_Height);
            m_MeshTexture.filterMode = FilterMode.Bilinear;
            m_MeshMaterial.mainTexture = m_MeshTexture;

            GeneratePerlinMap();
        }

        private void GeneratePerlinMap()
        {
            GenerateMeshAndTextures();
            m_Heights = new float[(MAX_OCTAVES + 1) * 2][, ];
            Color[][] colors = new Color[(MAX_OCTAVES + 1) * 2][];
            for (int i = 0; i < (MAX_OCTAVES + 1) * 2; i++)
            {
                m_Heights[i] = new float[m_Height, m_Width];
                colors[i] = new Color[m_Height * m_Width];
            }

            float elapsedTime = Time.realtimeSinceStartup;
            Debug.Log("Calculant Perlin Noise");
            //recorrem el mapa
            for (int y = 0; y < m_Height; y++)
            {
                for (int x = 0; x < m_Width; x++)
                {
                    float[] perlinNoise = PerlinNoiseUtilities.CalculatePerlinNoise(x, y, m_Frequency, m_Width, m_Height, m_OffsetX, m_OffsetY, m_Octaves, m_Lacunarity, m_Persistence, m_Carve, m_Verbose, true);
                    colors[0][x + y * m_Width] = m_Gradient.Evaluate(perlinNoise[0]);

                    //Omplim les textures de les octaves
                    for (int octave = 1; octave <= m_Octaves; octave++)
                    {
                        colors[octave * 2][x + y * m_Width] = m_Gradient.Evaluate(perlinNoise[octave*2]);
                        colors[octave * 2 + 1][x + y * m_Width] = m_Gradient.Evaluate(perlinNoise[octave * 2 + 1]);
                    }

                    //i utilitzem el soroll com a factor per a determinar l'al�ada final del terreny
                    colors[1][x + y * m_Width] = m_Gradient.Evaluate(perlinNoise[1]);
                    for(int i = 0; i < (m_Octaves + 1) * 2; i++)
                        m_Heights[i][y, x] = perlinNoise[i];
                        
                }
            }

            
            elapsedTime = Time.realtimeSinceStartup;
            for (int i = 0; i < (MAX_OCTAVES + 1) * 2; i++)
            {
                m_Texture[i].SetPixels(colors[i]);
                m_Texture[i].Apply();
            }

            ShowResult(0, 1);

            Debug.Log($"Temps emprat per a la generaci� del terreny: {(Time.realtimeSinceStartup - elapsedTime)}");
            Debug.Log("Recreaci� finalitzada");
        }

        private void GenerateMeshAndTextures()
        {
            for (int i = 0; i < (MAX_OCTAVES + 1) * 2; i++)
                m_Texture[i].Reinitialize(m_Width, m_Height);

            m_MeshTexture.Reinitialize(m_Width, m_Height);

            CreateMesh();
        }

        private void ShowResult(int octave, int mode)
        {
            int index = octave * 2 + mode;
            m_TextureOctave = octave;
            m_TextureMode = mode;
            m_GUI.GetComponent<Renderer>().material.mainTexture = m_Texture[index];
            RecalculateMesh(m_Heights[index]);
            m_MeshTexture.SetPixels(m_Texture[index].GetPixels());
            m_MeshTexture.Apply();
            Debug.Log($"Mostrant la textura {(m_TextureMode == 0 ? "base" : "combinada")} de l'octava {m_TextureOctave}");
        }

        //for future use
        private void CreateMesh()
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            mesh.Clear();

            Vector3[] vertices = new Vector3[m_Height * m_Width];
            Vector2[] uv = new Vector2[m_Height * m_Width];
            for (int x = 0; x < m_Height; x++)
                for (int z = 0; z < m_Width; z++)
                {
                    vertices[x * m_Width + z] = new Vector3(x, 0, z);
                    uv[x * m_Width + z] = new Vector2(z / ((float)m_Width-1), x / ((float)m_Height-1));
                }

            mesh.vertices = vertices;
            mesh.uv = uv;

            int[] triangles = new int[(m_Height-1) * (m_Width-1) * 2 * 3];
            for (int x = 0; x < (m_Height-1); x++)
                for (int z = 0; z < (m_Width-1); z++)
                {
                    triangles[x * ((m_Width - 1) * 6) + z * 6] = x * m_Width + z;
                    triangles[x * ((m_Width - 1) * 6) + z * 6 + 1] = (x + 1) * m_Width + z + 1;
                    triangles[x * ((m_Width - 1) * 6) + z * 6 + 2] = (x + 1) * m_Width + z;

                    triangles[x * (m_Width - 1) * 6 + z * 6 + 3] = x * m_Width + z;
                    triangles[x * (m_Width - 1) * 6 + z * 6 + 4] = x * m_Width + z + 1;
                    triangles[x * (m_Width - 1) * 6 + z * 6 + 5] = (x + 1) * m_Width + z + 1;
                }

            mesh.triangles = triangles;
        }

        private void RecalculateMesh(float[,] heights)
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;

            Vector3[] vertices = mesh.vertices;
            for (int x = 0; x < m_Height; x++)
                for (int z = 0; z < m_Width; z++)
                    vertices[x * m_Width + z] = new Vector3(x, heights[x, z] * m_Amplitude, z);

            mesh.vertices = vertices;
            mesh.RecalculateNormals();
        }

        void Update()
        {
            //regenerar perlins
            if (Input.GetKeyDown(KeyCode.Space))
                GeneratePerlinMap();

            //canviar textura entre base i combinada
            if (Input.GetKeyDown(KeyCode.C))
                ShowResult(m_TextureOctave, (m_TextureMode + 1) % 2);

            //canviar l'octava de textura a mostrar
            if (Input.GetKeyDown(KeyCode.Alpha0))
                ShowResult(0, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha1))
                ShowResult(1, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha2))
                ShowResult(2, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha3))
                ShowResult(3, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha4))
                ShowResult(4, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha5))
                ShowResult(5, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha6))
                ShowResult(6, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha7))
                ShowResult(7, m_TextureMode);
            if (Input.GetKeyDown(KeyCode.Alpha8))
                ShowResult(8, m_TextureMode);

        }
    }
}

