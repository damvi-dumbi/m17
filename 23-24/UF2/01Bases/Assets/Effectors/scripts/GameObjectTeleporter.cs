using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.Tilemaps;

namespace m17
{
    public class GameObjectTeleporter : MonoBehaviour
    {
        [SerializeField]
        private InputActionAsset m_InputAsset;
        private InputActionAsset m_Input;
        private InputAction m_PointerPosition;
        [SerializeField]
        [Tooltip("The teleportable object's layer")]
        private LayerMask m_LayerMask;

        private GameObject m_SelectedGameObject;

        private void Awake()
        {
            Assert.IsNotNull(m_InputAsset);
            m_Input = Instantiate(m_InputAsset);

            m_PointerPosition = m_Input.FindActionMap("Default").FindAction("PointerPosition");
            m_Input.FindActionMap("Default").FindAction("PointerClick").performed += OnTargetGameObject;
            m_Input.FindActionMap("Default").FindAction("Teleport").performed += OnTeleportGameObject;
            m_Input.FindActionMap("Default").Enable();
        }

        private void OnDestroy()
        {
            m_Input.FindActionMap("Default").FindAction("PointerClick").performed -= OnTargetGameObject;
            m_Input.FindActionMap("Default").FindAction("Teleport").performed -= OnTeleportGameObject;
            m_Input.FindActionMap("Default").Disable();
        }

        private void OnTargetGameObject(InputAction.CallbackContext context)
        {
            //coordenades de mouse
            Vector2 pointerPosition = m_PointerPosition.ReadValue<Vector2>();

            //transformem de coordenades pantalla a raig per poder fer Raycast
            Ray worldPosition = Camera.main.ScreenPointToRay(pointerPosition);

            //comprovem si hem clicat sobre un objecte v�lid
            RaycastHit2D hit = Physics2D.Raycast(worldPosition.origin, worldPosition.direction, 20f, m_LayerMask);
            if(hit.rigidbody != null)
            {
                m_SelectedGameObject = hit.rigidbody.gameObject;
            }
        }

        private void OnTeleportGameObject(InputAction.CallbackContext context)
        {
            if(m_SelectedGameObject != null)
            {
                //coordenades de mouse
                Vector2 pointerPosition = m_PointerPosition.ReadValue<Vector2>();

                //transformem de coordenades pantalla a posici� m�n per a teletransportar
                Vector3 worldPosition = Camera.main.ScreenToWorldPoint(pointerPosition);

                m_SelectedGameObject.TryGetComponent<Rigidbody2D>(out Rigidbody2D rigidbody);
                if(rigidbody != null)
                {
                    rigidbody.position = worldPosition; 
                    rigidbody.velocity = Vector2.zero;
                    rigidbody.angularVelocity = 0;
                    rigidbody.rotation = 0f;
                }
            }
            else
            {
                Debug.LogError("GameObjectTeleporter: You have no target to teleport.");
            }
        }
    }
}
