using m17;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Prefab;

    [SerializeField]
    private float m_SpawnFrequency = 1.0f;

    public void FromSaveData(SaveData data)
    {
        foreach(SaveData.EnemyData circleData in data.m_Enemies)
        {
            GameObject go = Instantiate(m_Prefab);
            go.GetComponent<ISaveableObject>().Load(circleData);
        }
    }

    private void Start()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while(true)
        {
            GameObject go = Instantiate(m_Prefab);
            go.GetComponent<CircleController>().Init();
            yield return new WaitForSeconds(m_SpawnFrequency);
        }
    }
}
