using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static m17.SaveData;

namespace m17
{
    public class CircleController : MonoBehaviour, ISaveableObject
    {
        public void Init()
        {
            transform.position = new Vector2(Random.Range(-10f, 10f), Random.Range(-5f, 5f));
            GetComponent<SpriteRenderer>().color = new Color(Random.Range(0f,1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        }

        public void Load(SaveData.EnemyData _enemyData)
        {
            transform.position = _enemyData.position;
            GetComponent<SpriteRenderer>().color = _enemyData.color;
        }

        public SaveData.EnemyData Save()
        {
            return new SaveData.EnemyData(GetComponent<SpriteRenderer>().color, transform.position);
        }
    }
}