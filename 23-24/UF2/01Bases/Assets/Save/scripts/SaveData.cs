using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using static m17.SaveData;

namespace m17
{
    [Serializable]
    public class SaveData
    {
        [Serializable]
        public struct EnemyData
        {
            public Color color;
            public Vector3 position;

            public EnemyData(Color _color, Vector3 _position)
            {
                color = _color;
                position = _position;
            }
        }

        public EnemyData[] m_Enemies;

        public void PopulateData(ISaveableObject[] _enemiesData)
        {
            m_Enemies = new EnemyData[_enemiesData.Length];
            for(int i = 0; i <  _enemiesData.Length; i++)
                m_Enemies[i] = _enemiesData[i].Save();
        }
    }

    public interface ISaveableObject
    {
        public EnemyData Save();
        public void Load(EnemyData _enemyData);
    }
}