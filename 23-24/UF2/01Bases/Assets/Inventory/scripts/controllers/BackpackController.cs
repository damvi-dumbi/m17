using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    public class BackpackController : MonoBehaviour
    {
        [SerializeField]
        private GameEvent m_GUIEvent;

        [SerializeField]
        private Backpack m_Backpack;

        public void ConsumeItem(Item item)
        {
            if (!item.UsedBy(gameObject))
                return;

            m_Backpack.RemoveItem(item);
            m_GUIEvent.Raise();
        }

        public void AddItem(Item item)
        {
            m_Backpack.AddItem(item);
            m_GUIEvent.Raise();
        }
    }
}