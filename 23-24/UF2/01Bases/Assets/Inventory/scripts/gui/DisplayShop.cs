using m17;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    public class DisplayShop : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_DisplayShopParent;

        [SerializeField]
        private GameObject m_DisplayItemPrefab;

        [SerializeField]
        private Item[] m_ItemsToDisplay;

        private void Start()
        {
            foreach (Item item in m_ItemsToDisplay)
            {
                GameObject displayedItem = Instantiate(m_DisplayItemPrefab, m_DisplayShopParent.transform);
                displayedItem.GetComponent<DisplayItem>().Load(item);
            }
        }
    }
}