using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    [CreateAssetMenu(fileName = "Healing Item", menuName = "Inventory/Items/Healing")]
    public class HealingItem : Item
    {
        [Header("Healing Item values")]
        [SerializeField]
        [Min(0f)]
        private float m_Healing;

        public override bool UsedBy(GameObject go)
        {
            if (!go.TryGetComponent(out IHealable healable))
                return false;

            healable.Heal(m_Healing);
            return true;
        }
    }
}