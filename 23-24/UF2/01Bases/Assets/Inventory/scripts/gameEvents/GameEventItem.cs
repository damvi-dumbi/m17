using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace m17
{
    [CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - IItem")]
    public class GameEventItem : GameEvent<Item> { }
}