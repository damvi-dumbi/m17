using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMovement : MonoBehaviour
{
    [SerializeField]
    private float m_MovementSpeed = 2.0f;

    void Update()
    {
        Vector3 moveSpeed = Vector3.zero;
        if(Input.anyKey)
        {
            if(Input.GetKey(KeyCode.A))
            {
                moveSpeed += Vector3.left;
            }
            if (Input.GetKey(KeyCode.D))
            {
                moveSpeed += Vector3.right;
            }
            if (Input.GetKey(KeyCode.W))
            {
                moveSpeed += Vector3.up;
            }
            if (Input.GetKey(KeyCode.S))
            {
                moveSpeed += Vector3.down;
            }
            moveSpeed.Normalize();
            moveSpeed *= m_MovementSpeed;
        }
        GetComponent<Rigidbody2D>().velocity = new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y) + moveSpeed;
    }

    public void SetSpeed(float speed)
    {
        m_MovementSpeed = speed;
    }
}
