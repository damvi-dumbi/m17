using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLerp : MonoBehaviour
{
    public float interpolationTimeCount = 10.0f; // Number of frames to completely interpolate between the 2 positions
    float elapsedTime = 0f;

    void Update()
    {
        float interpolationRatio = (float)elapsedTime / interpolationTimeCount;

        Vector3 interpolatedPosition = Vector3.Lerp(Vector3.up, Vector3.right, interpolationRatio);

        elapsedTime = (elapsedTime + Time.deltaTime) % (interpolationTimeCount + 1);  // reset elapsedTime to zero after it reached (interpolationTimeCount + 1)

        Debug.DrawLine(Vector3.zero, Vector3.up, Color.green);
        Debug.DrawLine(Vector3.zero, Vector3.right, Color.blue);
        Debug.DrawLine(Vector3.zero, interpolatedPosition, Color.yellow);
    }
}
