using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public GameObject Target;

    private Vector3 m_Offset;
    private Vector3 m_FollowPosition;
    private Vector3 m_InitialPosition;

    [SerializeField]
    private float m_FollowSpeed = 0.5f;
    private float m_FollowTime = 0.0f;

    private void Start()
    {
        if (Target)
        {
            m_FollowPosition = Target.transform.position;
            m_InitialPosition = transform.position;
            m_Offset = Target.transform.position - transform.position;
            m_FollowTime = m_FollowSpeed;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Target)
        {
            if(Target.transform.position != m_FollowPosition)
            {
                m_InitialPosition = transform.position;
                m_FollowPosition = Target.transform.position;
                m_FollowTime = 0.0f;
            }
            
            if(m_FollowTime < m_FollowSpeed)
            {
                m_FollowTime += Time.deltaTime;
                transform.position = Vector3.Lerp(m_InitialPosition, m_FollowPosition - m_Offset, (m_FollowTime / m_FollowSpeed));
                Debug.DrawLine(transform.position, m_FollowPosition - m_Offset, Color.red);
            }
        }
    }
}
