using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkellyBehaviour : MonoBehaviour
{
    [SerializeField]
    GameObject m_energy;
    // Start is called before the first frame update
    public void Shoot()
    {
        GameObject energy = Instantiate(m_energy);
        energy.transform.position = transform.position + transform.right * 1.5f + new Vector3(0,0,-1);
        energy.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 0);
    }
}
