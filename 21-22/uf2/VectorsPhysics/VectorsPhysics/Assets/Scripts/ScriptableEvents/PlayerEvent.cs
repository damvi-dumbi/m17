using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEvent : MonoBehaviour
{
    [SerializeField]
    private ScriptableFloat m_hp;
    [SerializeField]
    private GameEvent m_OnPlayerHP;

    private void Awake()
    {
        m_hp.value = 10;
    }

    private void OnMouseDown()
    {
        m_hp.value--;
        m_OnPlayerHP.Raise();
    }
}
