using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyForce : MonoBehaviour
{
    public float force = 3;
    public ForceMode2D forceMode;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.X))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * force, forceMode);
        }

        if (Input.GetKey(KeyCode.C))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * force, forceMode);
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            GetComponent<Rigidbody2D>().AddTorque(force, forceMode);
        }

        if (Input.GetKey(KeyCode.B))
        {
            GetComponent<Rigidbody2D>().AddTorque(force, forceMode); ;
        }
    }
}
