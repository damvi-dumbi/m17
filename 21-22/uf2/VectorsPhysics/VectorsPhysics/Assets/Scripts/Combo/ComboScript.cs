using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboScript : MonoBehaviour
{
    private enum Combo { IDLE, COMBO11, COMBO12, COMBO21, COMBO22 };
    private Combo m_state;

    //combo timers
    private bool m_inputReady = true;
    private float m_minTimeInput = 0.5f;
    private float m_maxTimeInput = 1.5f;
    private Coroutine MaxTimeCoroutine;
    // Start is called before the first frame update
    void Start()
    {
        SwitchState(Combo.IDLE);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.anyKeyDown)
        {
            ComboUpdate();
        }
    }

    private void ComboUpdate()
    {
        if (!m_inputReady)
            return;

        switch(m_state)
        {
            case Combo.IDLE:
                if(Input.GetKeyDown(KeyCode.Q))
                {
                    SwitchState(Combo.COMBO11);
                }else if(Input.GetKeyDown(KeyCode.W))
                {
                    SwitchState(Combo.COMBO21);
                }
                break;
            case Combo.COMBO11:
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    SwitchState(Combo.COMBO12);
                }
                break;
            case Combo.COMBO12:
                
                break;
            case Combo.COMBO21:
                if (Input.GetKeyDown(KeyCode.W))
                {
                    SwitchState(Combo.COMBO22);
                }
                break;
            case Combo.COMBO22:
                break;
            default:
                break;
        }
        StartCoroutine(WaitForComboInputMin());
        if(MaxTimeCoroutine != null)
            StopCoroutine(MaxTimeCoroutine);
        MaxTimeCoroutine = StartCoroutine(WaitForComboInputMax());
    }

    private IEnumerator WaitForComboInputMin()
    {
        m_inputReady = false;
        yield return new WaitForSeconds(m_minTimeInput);
        m_inputReady = true;
    }

    private IEnumerator WaitForComboInputMax()
    {
        yield return new WaitForSeconds(m_maxTimeInput);
        SwitchState(Combo.IDLE);
    }

    private IEnumerator SwitchStateCoroutine(float delay,Combo newState)
    {
        yield return new WaitForSeconds(delay);
        SwitchState(newState);
    }

    private void SwitchState(Combo newState)
    {
        switch(newState)
        {
            case Combo.IDLE:
                print("estat idle");
                break;
            case Combo.COMBO11:
                print("estat combo1");
                break;
            case Combo.COMBO12:
                print("estat combo12");
                StartCoroutine(SwitchStateCoroutine(1.0f, Combo.IDLE));
                break;
            case Combo.COMBO21:
                print("estat combo2");
                break;
            case Combo.COMBO22:
                print("estat combo22");
                StartCoroutine(SwitchStateCoroutine(1.0f, Combo.IDLE));
                break;
            default:
                break;
        }
        m_state = newState;
    }
}
