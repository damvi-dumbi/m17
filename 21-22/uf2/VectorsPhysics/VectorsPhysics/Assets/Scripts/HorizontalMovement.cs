using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalMovement : MonoBehaviour
{
    [SerializeField]
    private float m_MovementSpeed = 2.0f;

    void Update()
    {
        float horizontalSpeed = 0f;
        if(Input.anyKey)
        {
            if(Input.GetKey(KeyCode.A))
            {
                horizontalSpeed -= 1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                horizontalSpeed += 1;
            }

            horizontalSpeed *= m_MovementSpeed;
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(horizontalSpeed, GetComponent<Rigidbody2D>().velocity.y);
    }
}
