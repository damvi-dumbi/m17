using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTest : MonoBehaviour
{
    [SerializeField]
    private LayerMask m_collisionMask;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        List<Collider2D> colliders = new List<Collider2D>();
        ContactFilter2D filter = new ContactFilter2D();
        filter.useTriggers = true;
        filter.layerMask = m_collisionMask;
        int amount = collision.OverlapCollider(filter, colliders);
        for (int i = 0; i < amount; i++)
            print(colliders[i]);
    }
}
