using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIHpController : MonoBehaviour
{
    [SerializeField]
    private ScriptableFloat m_hp;
    [SerializeField]
    private Image m_hpBar;

    private void Start()
    {
        m_hpBar.type = Image.Type.Filled;
        m_hpBar.fillMethod = Image.FillMethod.Horizontal;
    }

    public void FillHP()
    {
        m_hpBar.fillAmount = m_hp.value / 10.0f;
    }
}
