using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightShooter : MonoBehaviour
{
    [SerializeField]
    private GameObject m_light;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnLights());
    }

    private IEnumerator SpawnLights()
    {
        while(true)
        {
            GameObject light = Instantiate(m_light);
            m_light.transform.position = new Vector2(Random.Range(-10f,10f), Random.Range(-10f, 10f));
            light.GetComponent<Rigidbody2D>().velocity = -1*light.transform.position.normalized*Random.Range(3f, 10f);
            Destroy(light, 5);
            yield return new WaitForSeconds(Random.Range(1, 3));
        }
    }
}
