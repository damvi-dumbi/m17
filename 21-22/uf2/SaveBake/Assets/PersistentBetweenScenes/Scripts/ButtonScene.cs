using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScene : MonoBehaviour
{
    public void ChangeScene(string scene)
    {
        PersistentGameManager.Instance.ChangeScene(scene);
    }
}
