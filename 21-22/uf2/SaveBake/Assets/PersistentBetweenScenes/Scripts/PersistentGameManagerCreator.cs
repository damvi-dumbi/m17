using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentGameManagerCreator : MonoBehaviour
{
    [SerializeField]
    private GameObject m_GameManager;
    private void Awake()
    {
        PersistentGameManager gameManager = FindObjectOfType<PersistentGameManager>();
        if (!gameManager)
        {
            Instantiate(m_GameManager);
        }
    }
}
