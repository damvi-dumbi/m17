using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PersistentGameManager : MonoBehaviour
{
    private static PersistentGameManager m_instance = null;
    public static PersistentGameManager Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = FindObjectOfType<PersistentGameManager>();

            return m_instance;
        }
    }

    [SerializeField]
    private GameEvent m_OnSceneChanged;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        m_OnSceneChanged.Raise();
    }

    public void ChangeScene(string scene)
    {
        SceneManager.LoadSceneAsync(scene);
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
