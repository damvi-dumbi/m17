using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TextScene : MonoBehaviour
{
    public void ChangeTextToScene()
    {
        GetComponent<Text>().text = SceneManager.GetActiveScene().name;
    }
}
