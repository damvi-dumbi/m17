using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SimpleSave : MonoBehaviour
{
    [SerializeField]
    private GameObject m_saveLoadObject;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveInfo info = new SaveInfo(m_saveLoadObject.transform);
            string jsonStr = JsonUtility.ToJson(info);
            print(jsonStr);
            File.WriteAllText("savegame.json", jsonStr);
            print("Game Saved");
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            
            string jsonStr = File.ReadAllText("savegame.json");
            SaveInfo info = JsonUtility.FromJson<SaveInfo>(jsonStr);

            m_saveLoadObject.GetComponent<Behaviour>().Load(info);
            print("Game Loaded");
        }
    }
}
