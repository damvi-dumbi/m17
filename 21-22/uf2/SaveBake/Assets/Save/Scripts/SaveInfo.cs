using System;
using UnityEngine;

[Serializable]
public class SaveInfo
{
    public Vector3 position;
    public Vector3 rotation;
    public Vector3 scale;

    public SaveInfo(Transform transform)
    {
        position = transform.localPosition;
        rotation = transform.localEulerAngles;
        scale = transform.localScale;
    }
}
