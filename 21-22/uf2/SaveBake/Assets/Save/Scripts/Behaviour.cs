using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Behaviour : MonoBehaviour
{
    void Awake()
    {
        transform.localScale = new Vector3(Random.Range(-3.0f, 3.0f), Random.Range(-3.0f, 3.0f), 0);
        transform.localEulerAngles = new Vector3(0, 0, Random.Range(-180.0f, 180.0f));
        transform.localPosition = new Vector3(Random.Range(-5.0f, 5.0f), Random.Range(-5.0f, 5.0f), 0);
    }

    public void Load(SaveInfo info)
    {
        transform.localPosition = info.position;
        transform.localEulerAngles = info.rotation;
        transform.localScale = info.scale;
    }
}