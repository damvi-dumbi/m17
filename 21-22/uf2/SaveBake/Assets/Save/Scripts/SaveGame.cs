using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveGame : MonoBehaviour
{
    [SerializeField]
    private GameObject m_prefab;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            SaveInfo[] info = new SaveInfo[transform.childCount];
            for(int i = 0; i < transform.childCount; i++)
                info[i] = new SaveInfo(transform.GetChild(i));

            string jsonStr = JsonHelper.ToJson<SaveInfo>(info);
            print(jsonStr);
            File.WriteAllText("savegame.json", jsonStr);
            print("Game Saved");
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            for (int i = 0; i < transform.childCount; i++)
                Destroy(transform.GetChild(i).gameObject);

            string jsonStr = File.ReadAllText("savegame.json");
            SaveInfo[] info = JsonHelper.FromJson<SaveInfo>(jsonStr);

            for (int i = 0; i < info.Length; i++)
            {
                GameObject go = Instantiate(m_prefab, transform);
                go.GetComponent<Behaviour>().Load(info[i]);
            }

            print("Game Loaded");
        }
    }
}