using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelegateTest : MonoBehaviour
{
    public GUIController m_GUI;
    public GameObject m_cosa;
    /*
     * Definicio del delegat i el seu event corresponent
     */
    public delegate void TextDelegate();
    public event TextDelegate OnTextDelegate;

    // Start is called before the first frame update
    void Start()
    {
        //Subscripci� al delegat
        OnTextDelegate += PrintTextDelegate;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            m_GUI.SetText("Text modificat");
            GameObject cosa = Instantiate(m_cosa);
            //li passem per refer�ncia perqu� s'ha de subscriure
            cosa.GetComponent<CosaController>().m_delegateTest = this;
        }
        if (Input.GetKeyDown(KeyCode.S))
            OnTextDelegate.Invoke();
    }

    //funci� que es cridar� quan es produeixi l'event del delegat
    private void PrintTextDelegate()
    {
        m_GUI.SetText("Via delegat");
    }
}
