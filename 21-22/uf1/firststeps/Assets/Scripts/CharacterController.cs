using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float m_jumpForce = 5f;
    public float m_sideForce = 5f;
    private bool m_isGrounded = true;

    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    private void Reset()
    {
        m_isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();
            rigidBody.velocity = new Vector2(-m_sideForce, rigidBody.velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();
            rigidBody.velocity = new Vector2(m_sideForce, rigidBody.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.W) && m_isGrounded)
        {
            Rigidbody2D rigidBody = GetComponent<Rigidbody2D>();
            rigidBody.AddForce(new Vector2(0f, m_jumpForce), ForceMode2D.Impulse);
            //rigidBody.velocity = new Vector2(rigidBody.velocity.y, m_jumpForce);
            m_isGrounded = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!m_isGrounded && collision.gameObject.tag == "Ground")
            m_isGrounded = true;
    }
}
