using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooSpawner : MonoBehaviour
{
    public float m_spawnFrequency = 1.0f;
    public GameObject m_poo;
    // Start is called before the first frame update
    void Start()
    {
        //cridem infinitament la seg�ent funci�
        InvokeRepeating("SpawnPoo", 0, m_spawnFrequency);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnPoo()
    {
        GameObject poo = Instantiate(m_poo);
        Vector2 position = new Vector2(Random.Range(-10, 10), 7);
        poo.transform.localEulerAngles = new Vector3(0,0,20);
        poo.transform.localScale = new Vector3(Random.Range(1,3), Random.Range(1, 3));
        poo.transform.position = position;
    }
}
