using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooController : MonoBehaviour
{
    public float m_fallSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = -transform.up*m_fallSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0,0,45*Time.deltaTime));
        if (transform.position.y <= -5)
            Destroy(gameObject);
    }
}
