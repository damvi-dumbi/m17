using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosaController : MonoBehaviour
{
    public DelegateTest m_delegateTest;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector2(Random.Range(-8, 8), Random.Range(-4, 4));
        m_delegateTest.OnTextDelegate += TimeToDie;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDisable()
    {
        m_delegateTest.OnTextDelegate -= TimeToDie;
    }

    private void TimeToDie()
    {
        Destroy(gameObject);
    }
}
