using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Tilemap m_tileMap;

    public delegate void UserClicked(Vector3 position);
    public event UserClicked OnUserClicked;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3Int position = m_tileMap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            //aqu� podriem agafar el tile per exemple
            Tile tile = m_tileMap.GetTile<Tile>(position);

            //tamb� podem esborrar o posar un altre
            m_tileMap.SetTile(position, null);
            GUIController.Instance.SetText(position.ToString());

            OnUserClicked.Invoke(position);
        }
    }
}
