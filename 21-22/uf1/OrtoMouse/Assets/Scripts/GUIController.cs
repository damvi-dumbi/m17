using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    [SerializeField]
    private TMPro.TextMeshProUGUI m_text;

    public static GUIController Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = FindObjectOfType<GUIController>();

            return m_instance;
        }
    }
    private static GUIController m_instance = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetText(string text)
    {
        m_text.text = text;
    }
}
