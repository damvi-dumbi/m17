using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChonkusController : MonoBehaviour
{
    public float m_movementSpeed = 1f;

    [SerializeField]
    private GameController m_gameController;
    private UserInputController m_input;
    private Rigidbody2D m_rigidbody;

    private void Awake()
    {
        m_input = UserInputController.Instance;
    }

    void Start()
    {
        m_gameController.OnUserClicked += OnClicked;
        m_rigidbody = GetComponent<Rigidbody2D>();
    }

    
    void Update()
    {
        m_rigidbody.velocity = m_input.InputAxis;
        m_rigidbody.velocity.Normalize();
        m_rigidbody.velocity *= m_movementSpeed;
    }

    private void OnDisable()
    {
        m_gameController.OnUserClicked -= OnClicked;
    }

    private void OnClicked(Vector3 position)
    {
        //comprova col�lisions pel cam�
        m_rigidbody.MovePosition(position + new Vector3(.5f,.5f));
        //teletransporta ignorant les f�siques
        //transform.position = position + new Vector3(.5f, .5f);
    }
}
