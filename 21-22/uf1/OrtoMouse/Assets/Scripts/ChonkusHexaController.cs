using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ChonkusHexaController : MonoBehaviour
{
    [SerializeField]
    private float m_movementSpeed;

    [SerializeField]
    private Tilemap m_tilemap;

    private Rigidbody2D m_rigidbody;

    private Vector3 m_destination;
    private bool m_arrived = false;

    public delegate void DestinationSet(Vector3 destination);
    public event DestinationSet OnDestinationSet;

    public delegate void DestinationArrived();
    public event DestinationArrived OnDestinationArrived;

    // Start is called before the first frame update
    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_arrived = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            m_arrived = false;
            Vector3Int cellCoordinates = m_tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            /*
             * les coordenades de posici� d'una casella estan en z = 0
             */
            cellCoordinates.z = 0;
            /*
             * agafem la posici� del centre de la casella en coordenades del m�n
             */
            m_destination = m_tilemap.GetCellCenterWorld(cellCoordinates);
            OnDestinationSet.Invoke(m_destination);
        }

        //moure quan estiguis a una distancia, sino no movem
        if (Vector3.Distance(transform.position, m_destination) > 0.2f)
        {
            /*
             * O es fa servir el move Towards
             */
            //Vector2 direction = Vector2.MoveTowards(transform.position, m_destination, m_movementSpeed * Time.deltaTime);
            //m_rigidbody.MovePosition(direction);

            /* O es fan els mateixos c�lculs a m�.
             *  1. Calcular la direcci�
             *  2. Convertir el vector a m�dul 1 (longitud)
             *  3. estipular la nova posici� com a posici� actual + l'increment de posici� que ens hem de moure
             */
            Vector2 direction = m_destination - transform.position;
            direction.Normalize();
            m_rigidbody.MovePosition(new Vector2(transform.position.x, transform.position.y) + direction*m_movementSpeed*Time.deltaTime);
        }
        else
        {
            if(!m_arrived)
            {
                OnDestinationArrived.Invoke();
                m_arrived = true;
            }
        }
    }
}
