using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    [SerializeField]
    private Turret m_turretInfo;

    void Start()
    {
        LoadInfo();
        StartCoroutine("Shoot");
    }

    private IEnumerator Shoot()
    {
        while(true)
        {
            print("turret " + gameObject.name + " with range: " + m_turretInfo.Range
                + " and attackPattern " + m_turretInfo.AttackPattern + " shooting.");
            for(int i = 0; i < m_turretInfo.Burst; i++)
            {
                Instantiate(m_turretInfo.ProjectilePrefab, transform.position, Quaternion.identity);
                yield return new WaitForSeconds(m_turretInfo.BurstCadence);
            }
            yield return new WaitForSeconds(m_turretInfo.Cadence);
        }
    }

    public void SetInfo(Turret turretInfo)
    {
        m_turretInfo = turretInfo;
        LoadInfo();
    }

    private void LoadInfo()
    {
        GetComponent<SpriteRenderer>().sprite = m_turretInfo.Sprite;
    }
}