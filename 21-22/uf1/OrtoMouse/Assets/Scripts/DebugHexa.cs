using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DebugHexa : MonoBehaviour
{
    [SerializeField]
    private Tilemap m_tilemap;

    [SerializeField]
    private ChonkusHexaController m_chonkus;

    [SerializeField]
    private Color m_color;

    private bool m_destinationSet = false;
    private bool m_arrived = false;
    private Vector3Int m_destinationCell;
    private Vector3 m_destinationPosition;


    void Start()
    {
        m_chonkus.OnDestinationArrived += OnArrived;
        m_chonkus.OnDestinationSet += OnDestination;
    }

    private void OnDisable()
    {
        m_chonkus.OnDestinationArrived -= OnArrived;
        m_chonkus.OnDestinationSet -= OnDestination;
    }

    private void OnDestination(Vector3 destination)
    {
        if (!m_arrived && m_destinationSet)
        {
            SetTileColor(Color.white);
            StopCoroutine("DrawLine");
        }

        m_destinationPosition = destination;
        m_destinationCell = m_tilemap.WorldToCell(destination);
        
        m_arrived = false;
        m_destinationSet = true;
        SetTileColor(m_color);
        StartCoroutine("DrawLine");
    }

    private void OnArrived()
    {
        m_arrived = true;
        m_destinationSet = false;
        SetTileColor(Color.white);
    }

    private void SetTileColor(Color color)
    {
        /*
         * El flag ens permet modificar nom�s aquest Tile i no tots, ja que per
         * defecte estan tots connectats entre ells i pintariem tots els del
         * mateix tipus.
         */        
        m_tilemap.SetTileFlags(m_destinationCell, TileFlags.None);
        m_tilemap.SetColor(m_destinationCell, color);
    }

    private IEnumerator DrawLine()
    {
        /*
         * Pintem una l�nia per a veure quina ser� la traject�ria
         */
        float update = 0.5f;
        while(!m_arrived)
        {
            Debug.DrawLine(m_chonkus.transform.position, m_destinationPosition, Color.red, update, false);
            yield return new WaitForSeconds(update);
        }
    }
}
