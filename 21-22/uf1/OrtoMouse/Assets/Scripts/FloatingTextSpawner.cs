using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_floatingTextPrefab;
    private GameController m_gameController;

    private void Awake()
    {
        m_gameController = GameObject.FindObjectOfType<GameController>();
    }

    void Start()
    {
        m_gameController.OnUserClicked += SpawnEnemy;
    }

    void Update()
    {
        
    }

    private void OnDisable()
    {
        m_gameController.OnUserClicked -= SpawnEnemy;
    }

    private void SpawnEnemy(Vector3 position)
    {
        GameObject floatingText = Instantiate(m_floatingTextPrefab);
        floatingText.transform.position += position+new Vector3(0.5f,0.5f);
    }
}
