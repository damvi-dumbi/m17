using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInputController : MonoBehaviour
{
    private static UserInputController m_instance;

    public static UserInputController Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = FindObjectOfType<UserInputController>();
            return m_instance;
        }
    }

    public Vector2Int InputAxis => m_inputAxis;
    [SerializeField]
    private Vector2Int m_inputAxis = new Vector2Int(0, 0);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(m_inputAxis.x != 0)
        {
            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                m_inputAxis.x = 0;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.A))
                m_inputAxis.x = -1;
            else if (Input.GetKeyDown(KeyCode.D))
                m_inputAxis.x = 1;
        }

        if (m_inputAxis.y != 0)
        {
            if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S))
                m_inputAxis.y = 0;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
                m_inputAxis.y = -1;
            else if (Input.GetKeyDown(KeyCode.W))
                m_inputAxis.y =  1;
        }
    }
}
