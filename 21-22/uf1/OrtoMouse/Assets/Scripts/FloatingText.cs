using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    [SerializeField]
    private float m_spintVelocity = 180f;
    private TMPro.TextMeshPro m_text;
    // Start is called before the first frame update
    private void Awake()
    {
        m_text = GetComponent<TMPro.TextMeshPro>();
    }

    void Start()
    {
        StartCoroutine("TimeToLive");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, m_spintVelocity*Time.deltaTime, 0);
    }

    public void SetText(string text)
    {
        m_text.text = text;
    }

    IEnumerator TimeToLive()
    {
        SetText("4");
        yield return new WaitForSeconds(1);
        SetText("3");
        yield return new WaitForSeconds(1);
        SetText("2");
        yield return new WaitForSeconds(1);
        SetText("1");
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
