using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    [SerializeField]
    private Projectile m_projectileInfo;

    void Start()
    {
        print("S�c un projectil tipus " + m_projectileInfo.Type
            + " amb damage " + m_projectileInfo.Damage);

        GetComponent<Rigidbody2D>().velocity = new Vector2(3, 0);
        Destroy(gameObject, 3.0f);
    }
}
