using UnityEngine;

public enum TurretAttackPattern { STRAIGHT, HORIZONTAL, CROSS, ROUND };

[CreateAssetMenu(fileName="Turret", menuName="ScriptableObjects/Enemies/Turret")]
public class Turret : ScriptableObject
{
    public Sprite Sprite;
    public TurretAttackPattern AttackPattern;
    public int Range;
    public float Cadence;
    public int Burst;
    public float BurstCadence;
    public GameObject ProjectilePrefab;
}
