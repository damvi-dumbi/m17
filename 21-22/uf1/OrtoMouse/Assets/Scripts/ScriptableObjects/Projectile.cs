using UnityEngine;

public enum ProjectileType { SINGLE, AREA };

[CreateAssetMenu(fileName="Projectile", menuName="ScriptableObjects/Projectile")]
public class Projectile : ScriptableObject
{
    public ProjectileType Type;
    public int Damage;
}
