using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_TurretPrefab;
    public void SpawnTurret(Turret turretInfo)
    {
        Vector3 position = new Vector3(Random.Range(-10,10), Random.Range(-5, 5), 0);
        GameObject turret = Instantiate(m_TurretPrefab, position, Quaternion.identity);
        turret.GetComponent<TurretController>().SetInfo(turretInfo);
    }
}
