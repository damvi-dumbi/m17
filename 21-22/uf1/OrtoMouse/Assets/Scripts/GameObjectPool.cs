using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool : MonoBehaviour
{
    [SerializeField]
    private int m_poolSize = 10;
    [SerializeField]
    private GameObject m_prefabs;

    private List<GameObject> m_list;
    private List<GameObject> m_listAvailable;

    // Start is called before the first frame update
    void Start()
    {
        m_list = new List<GameObject>();
        m_listAvailable = new List<GameObject>();
        for (int i = 0; i < m_poolSize; i++)
        {
            GameObject poolElement = Instantiate(m_prefabs);
            poolElement.SetActive(false);
            m_list.Add(poolElement);
            m_listAvailable.Add(poolElement);
        }
    }

    public GameObject GetElement()
    {
        GameObject element = null;
        if (m_listAvailable.Count > 0)
        {
            element = m_listAvailable[0];
            m_listAvailable.RemoveAt(0);
            element.SetActive(true);
        }

        return element;
    }

    public void ReturnElement(GameObject go)
    {
        if(m_list.Contains(go) && !m_listAvailable.Contains(go))
        {
            go.SetActive(false);
            m_listAvailable.Add(go);
        }
    }
}
