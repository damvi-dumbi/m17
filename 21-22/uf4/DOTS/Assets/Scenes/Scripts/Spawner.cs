using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Prefab;

    [SerializeField]
    private float m_SpawnTime;

    [SerializeField]
    private float m_SpawnAmount;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        while(true)
        {
            for (int i = 0; i < m_SpawnAmount; i++)
                Instantiate(m_Prefab);

            yield return new WaitForSeconds(m_SpawnTime);
        }
    }
}
