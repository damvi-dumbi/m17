using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class PlayerCollisionSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        float3 playerPosition = PlayerController.Instance.transform.position;
        float deltaTime = Time.DeltaTime;

        Entities.WithAll<EnemyTag>().
            ForEach((Entity entity,
                    ref Translation translation,
                    ref HP hp) =>
        {
            if (math.distance(translation.Value, playerPosition) < 3f)
            {
                hp.Value -= deltaTime * 20;
                if(hp.Value <= 0)
                    PostUpdateCommands.DestroyEntity(entity);
            }
                
        });
    }
}
