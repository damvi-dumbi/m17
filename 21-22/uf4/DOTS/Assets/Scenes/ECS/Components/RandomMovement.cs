using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct RandomMovement : IComponentData
{
    public float2 direction;
    public float speed;
}
