using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[DisallowMultipleComponent]
public class CircleInitialization : MonoBehaviour, IConvertGameObjectToEntity
{
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        float3 initialPosition = new float3(UnityEngine.Random.Range(-10f,10f), UnityEngine.Random.Range(-10f, 10f), 0);
        dstManager.SetComponentData(entity, new Unity.Transforms.Translation { Value =  initialPosition });

        Vector2 randomDirection = new Vector2(UnityEngine.Random.Range(-10, 10), UnityEngine.Random.Range(-10, 10));
        dstManager.SetComponentData(entity, new RandomMovement { direction = randomDirection.normalized, speed = 2 });
    }
}
