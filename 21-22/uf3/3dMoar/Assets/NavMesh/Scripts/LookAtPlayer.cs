using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject m_target;
 
    void Update()
    {
        transform.LookAt(m_target.transform.position);
    }
}
