using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBehaviour : MonoBehaviour
{
    [SerializeField]
    private float m_speed = 4f;

    private Vector3 m_direction;
    void Start()
    {
        m_direction = new Vector3(Random.Range(0f, 1f), 0, Random.Range(0f, 1f));
        m_direction.Normalize();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += m_direction * m_speed * Time.deltaTime;
    }
}
