using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField]
    private float m_spawnTime = 3f;
    [SerializeField]
    private int m_spawnRate = 3;
    [SerializeField]
    private Vector2 m_agentDestinationRangeX;
    [SerializeField]
    private Vector2 m_agentDestinationRangeZ;
    [SerializeField]
    private GameObject[] m_obstacles;

    void Start()
    {
        StartCoroutine(SpawnObstacle());
    }

    private IEnumerator SpawnObstacle()
    {
        while(true)
        {
            for(int i = 0; i < m_spawnRate; i++)
            {
                GameObject obstacle = Instantiate(m_obstacles[Random.Range(0, m_obstacles.Length)]);
                Vector3 spawnPoint = new Vector3(Random.Range(m_agentDestinationRangeX.x, m_agentDestinationRangeX.y), 0, Random.Range(m_agentDestinationRangeZ.x, m_agentDestinationRangeZ.y));
                obstacle.transform.position = spawnPoint;
                Vector3 direction = new Vector3(Random.Range(0f, 1f), 0, Random.Range(0f, 1f));
                direction.Normalize();
                print(direction);
                obstacle.GetComponent<Rigidbody>().velocity = direction * Random.Range(2f, 10f);
                Destroy(obstacle, 10f);
            }
            yield return new WaitForSeconds(m_spawnTime);
        }
    }
}
