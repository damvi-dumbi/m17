using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomMovementRigid : MonoBehaviour
{
    [SerializeField]
    private GameObject m_destinationMarkerPrefab;
    private List<GameObject> m_destinationMarkers = new List<GameObject>();

    List<Vector3> m_waypoints = new List<Vector3>();

    [SerializeField]
    float m_stopDistance = 0.1f;

    [SerializeField]
    float m_movementSpeed = 1f;

    [SerializeField]
    float m_rotationSpeed = 45f;


    private void FixedUpdate()
    {
        if (m_waypoints.Count == 0)
        {
            SetNewDestination();
            return;
        }

        if (Vector3.Distance(transform.position, m_waypoints[0]) >= m_stopDistance)
        {
            GetComponent<Rigidbody>().velocity = (m_waypoints[0] - transform.position).normalized * m_movementSpeed;

            float m_rotationY = Vector3.SignedAngle(transform.forward, m_waypoints[0]- transform.position, Vector3.up);
            if(m_rotationY > 5 || m_rotationY < -5)
                transform.Rotate(Vector3.up * (m_rotationY>0?1:-1) *m_rotationSpeed * Time.fixedDeltaTime);
        }
        else
        {
            m_waypoints.RemoveAt(0);
        }
    }

    private void SetNewDestination()
    {
        Vector3 newDestination = new Vector3(Random.Range(-6f,6f),0, Random.Range(-6f, 6f));

        NavMeshPath navigationPath = new NavMeshPath();
        if (NavMesh.CalculatePath(transform.position, newDestination, NavMesh.AllAreas, navigationPath))
        {
            m_destinationMarkers.ForEach(Destroy);
            m_destinationMarkers.Clear();
            for (int i = 0; i < navigationPath.corners.Length; i++)
            {
                GameObject destinationMarker = Instantiate(m_destinationMarkerPrefab);
                destinationMarker.transform.position = navigationPath.corners[i] + Vector3.up * 3;
                m_destinationMarkers.Add(destinationMarker);
            }
            m_waypoints.AddRange(navigationPath.corners);
        }
    }
}
