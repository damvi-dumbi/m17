using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RandomMovement : MonoBehaviour
{
    [SerializeField]
    private GameObject m_destinationMarkerPrefab;
    private GameObject m_destinationMarker;
    private NavMeshAgent m_agent;

    [SerializeField]
    private Vector2 m_agentDestinationRangeX;
    [SerializeField]
    private Vector2 m_agentDestinationRangeZ;

    void Awake()
    {
        m_agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if ((m_agent.remainingDistance != Mathf.Infinity || m_agent.remainingDistance <= m_agent.stoppingDistance)
            && !m_agent.hasPath
            && !m_agent.pathPending)
            SetNewDestination();
    }

    private void SetNewDestination()
    {
        Vector3 newDestination = new Vector3(Random.Range(m_agentDestinationRangeX.x, m_agentDestinationRangeX.y), 0, Random.Range(m_agentDestinationRangeZ.x, m_agentDestinationRangeZ.y));

        m_agent.SetDestination(newDestination);
        print("Setting new destination as: " + newDestination);

        if(!m_destinationMarker)
            m_destinationMarker = Instantiate(m_destinationMarkerPrefab);
        m_destinationMarker.transform.position = newDestination + Vector3.up * 3;
    }
}
