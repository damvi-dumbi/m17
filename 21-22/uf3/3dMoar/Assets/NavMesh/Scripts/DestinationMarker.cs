using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationMarker : MonoBehaviour
{
    float m_angle;

    private void Update()
    {
        m_angle += Time.deltaTime*Mathf.PI/2;

        if (m_angle >= 360f)
            m_angle -= 360f;

        transform.position = new Vector3(transform.position.x, 3+Mathf.Sin(m_angle), transform.position.z);
    }
}
