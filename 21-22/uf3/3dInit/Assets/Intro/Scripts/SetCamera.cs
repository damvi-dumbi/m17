using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCamera : MonoBehaviour
{
    public Camera m_standard;
    public Camera m_topside;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            m_standard.enabled = !m_standard.enabled;
            m_topside.enabled = !m_topside.enabled;
        }
    }
}
