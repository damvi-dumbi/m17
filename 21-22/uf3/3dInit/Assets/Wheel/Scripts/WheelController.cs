using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelController : MonoBehaviour
{
    [SerializeField]
    private float m_motorTorque = 20f;
    [SerializeField]
    private float m_motorBrake = 5000f;
    [SerializeField]
    private float m_rotateSpeed = 30f;

    private bool m_accelerate = false;
    private bool m_brake = false;
    private float m_rotate = 0;

    [SerializeField]
    AxleInfo[] m_axles;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
            m_accelerate = true;
        if (Input.GetKeyUp(KeyCode.W))
            m_accelerate = false;

        if (Input.GetKeyDown(KeyCode.S))
            m_brake = true;
        if (Input.GetKeyUp(KeyCode.S))
            m_brake = false;

        m_rotate = Input.GetAxis("Horizontal");
    }
    private void FixedUpdate()
    {
        if(m_accelerate)
            Accelerate();

        if(m_brake)
            Brake();

        Steer();
    }

    private void Accelerate()
    {
        foreach (AxleInfo axle in m_axles)
        {
            if (axle.motor)
            {
                axle.leftWheel.motorTorque = m_motorTorque;
                axle.leftWheel.brakeTorque = 0;

                axle.rightWheel.motorTorque = m_motorTorque;
                axle.rightWheel.brakeTorque = 0;
            }
        }
    }

    private void Brake()
    {
        foreach (AxleInfo axle in m_axles)
        {
            if (axle.brake)
            {
                axle.leftWheel.motorTorque = 0;
                axle.leftWheel.brakeTorque = m_motorBrake;

                axle.rightWheel.motorTorque = 0;
                axle.rightWheel.brakeTorque = m_motorBrake;
            }
        }
    }

    private void Steer()
    {
        foreach (AxleInfo axle in m_axles)
        {
            if (axle.steering)
            {
                axle.leftWheel.steerAngle = m_rotate * m_rotateSpeed;
                axle.rightWheel.steerAngle = m_rotate * m_rotateSpeed;
            }
            else
            {
                axle.leftWheel.steerAngle = 0;
                axle.rightWheel.steerAngle = 0;
            }
        }
    }
}

[System.Serializable]
public class AxleInfo
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor; // is this wheel attached to motor?
    public bool steering; // does this wheel apply steer angle?
    public bool brake;
}