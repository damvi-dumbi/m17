using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SynchWheelMesh : MonoBehaviour
{
    Transform m_wheelMesh;
    WheelCollider m_wheelCollider;
    private void Awake()
    {
        m_wheelMesh = transform.GetChild(0);
        m_wheelCollider = GetComponent<WheelCollider>();
    }

    private void Update()
    {
        Vector3 position;
        Quaternion rotation;
        m_wheelCollider.GetWorldPose(out position, out rotation);
        rotation *= Quaternion.Euler(0, 0, 90f);
        m_wheelMesh.position = position;
        m_wheelMesh.localRotation = rotation;
    }
}
