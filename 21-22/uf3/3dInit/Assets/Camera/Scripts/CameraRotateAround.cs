using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotateAround : MonoBehaviour
{
    [SerializeField]
    private GameObject m_target;

    private void Awake()
    {
        transform.LookAt(m_target.transform.position);
    }
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            transform.RotateAround(m_target.transform.position, m_target.transform.up, Input.GetAxis("Mouse X") * 60f * Time.deltaTime);
            transform.LookAt(m_target.transform.position);
        }
    }
}
