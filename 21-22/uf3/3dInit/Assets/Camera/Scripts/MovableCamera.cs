using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableCamera : MonoBehaviour
{

    [SerializeField]
    private float m_rotationSpeed = 1.0f;

    [SerializeField]
    private float m_zoomSpeed = 1.0f;

    [SerializeField]
    private float m_panSpeed = 1.0f;

    public bool InvertYAxis = false;

    void Update()
    {
        //leftClick
        if (Input.GetMouseButtonDown(0))
        {
            LockMouse();
        }
        if(Input.GetMouseButton(0))
        {
            RotateCamera(new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")));
        }
        if (Input.GetMouseButtonUp(0))
        {
            UnlockMouse();
        }

        //mouseWheel
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            MoveCameraForward((Input.GetAxis("Mouse ScrollWheel") > 0 ? 1 : -1));
        }

        //rightClick
        if (Input.GetMouseButtonDown(1))
        {
            LockMouse();
        }
        if (Input.GetMouseButton(1))
        {
            PanCamera(new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")));
        }
        if (Input.GetMouseButtonUp(1))
        {
            UnlockMouse();
        }

    }

    private void LockMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void UnlockMouse()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void RotateCamera(Vector2 rotation)
    {
        rotation *= m_rotationSpeed;
        transform.Rotate((InvertYAxis ? 1 : -1) * rotation.y, rotation.x, 0);
    }

    private void MoveCameraForward(int direction)
    {
        transform.position += transform.forward * direction * m_zoomSpeed;
    }

    private void PanCamera(Vector2 movement)
    {
        transform.position += transform.up * movement.y * m_panSpeed;
        transform.position += transform.right * movement.x * m_panSpeed;
    }
}
