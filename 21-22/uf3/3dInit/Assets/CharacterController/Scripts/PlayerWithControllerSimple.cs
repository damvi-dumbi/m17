using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWithControllerSimple : MonoBehaviour
{
    [SerializeField]
    private float m_speed = 3.0f;

    [SerializeField]
    private float m_rotateSpeed = 1.0f;

    private CharacterController m_characterController;
    private void Awake()
    {
        m_characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        Vector3 movement = Vector3.zero;
        if(Input.GetAxis("Vertical") != 0f)
            movement += transform.forward * Input.GetAxis("Vertical") * m_speed;

        if(Input.GetAxis("Horizontal") != 0f)
        {
            if(Input.GetMouseButton(1))
                movement += transform.right * Input.GetAxis("Horizontal") * m_speed;
            else
                transform.Rotate(Vector3.up * Input.GetAxis("Horizontal") * m_rotateSpeed);
        }

        m_characterController.SimpleMove(movement);
    }
}
