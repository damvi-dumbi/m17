using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using OdinSerializer;
using System.IO;

public class GameController : MonoBehaviour
{
    private SaveGameData m_CurrentSavedData;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void OnChangeLevel()
    {
        if (SceneManager.GetActiveScene().name == "Scene01")
            SceneManager.LoadScene("Scene02");
        else SceneManager.LoadScene("Scene01");
    }

    public void SaveGame()
    {
        //Al main menu no guardem
        if(SceneManager.GetActiveScene().name != "SaveGameINIT")
        {
            SaveGameData saveGameData = new SaveGameData();
            saveGameData.level = SceneManager.GetActiveScene().name;
            PlayerController player = FindObjectOfType<PlayerController>();
            saveGameData.player.position = player.transform.position;
            saveGameData.player.rotation = player.transform.rotation;

            byte[] serializedData = SerializationUtility.SerializeValue<SaveGameData>(saveGameData, DataFormat.JSON);
            string base64 = System.Convert.ToBase64String(serializedData);
            
            File.WriteAllText("savegame.json", base64);
            Debug.Log("Desant el fitxer savegame.json");
            Debug.Log(saveGameData);
        }
    }

    public void LoadGame()
    {
        try
        {
            Debug.Log("Carregant el fitxer: savegame.json");
            string newBase64 = File.ReadAllText("savegame.json");
            byte[] serializedData = System.Convert.FromBase64String(newBase64);

            m_CurrentSavedData = SerializationUtility.DeserializeValue<SaveGameData>(serializedData, DataFormat.JSON);
            Debug.Log(m_CurrentSavedData);
            //subscribe to the scene loaded
            SceneManager.sceneLoaded += LoadGameSceneLoaded;
            //change scene to the specified one
            Debug.Log("Canviant a escena: " + m_CurrentSavedData.level);
            SceneManager.LoadScene(m_CurrentSavedData.level);
        }
        catch(System.Exception e)
        {
            Debug.Log(e.Message);
        }
        
    }

    private void LoadGameSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Escena carregada en mode LOAD : " + scene.name);
        //we are called because we are loading and the scene has been loaded
        //load values to the player
        FindObjectOfType<PlayerController>().Load(m_CurrentSavedData);
        //unsubscribe from the onloadscene
        SceneManager.sceneLoaded -= LoadGameSceneLoaded;
    }
}
