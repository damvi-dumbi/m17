using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    float m_MoveSpeed = 3;

    [SerializeField]
    float m_RotationSpeed = 180;

    private void Awake()
    {
        Debug.Log("AWAKE");
    }

    private void Start()
    {
        Debug.Log("START");
    }

    void Update()
    {
        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement += transform.up;

        if (Input.GetKey(KeyCode.D))
            movement += transform.right;

        if (Input.GetKey(KeyCode.S))
            movement -= transform.up;

        if (Input.GetKey(KeyCode.A))
            movement -= transform.right;

        transform.position += movement.normalized * m_MoveSpeed * Time.deltaTime;

        if (Input.GetKey(KeyCode.E))
            transform.Rotate(-Vector3.forward * m_RotationSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.Q))
            transform.Rotate(Vector3.forward * m_RotationSpeed * Time.deltaTime);
    }

    public void Load(SaveGameData data)
    {
        Debug.Log("Player carregant informació.");
        transform.position = data.player.position;
        transform.rotation = data.player.rotation;
    }
}
