using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class OnButtonClickEvent : MonoBehaviour
{
    [SerializeField]
    GameEvent m_Event;
    
    public void RaiseEvent()
    {
        m_Event?.Raise();
    }
}