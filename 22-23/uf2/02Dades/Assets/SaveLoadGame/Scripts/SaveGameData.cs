using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct SaveGameData
{
    public string level;
    [Serializable]
    public struct SaveObjectPositionRotation
    {
        public Vector3 position;
        public Quaternion rotation;
    }
    public SaveObjectPositionRotation player;
}
