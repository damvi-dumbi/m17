using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OdinSerializer;
using System.IO;
using System;

public class ControlInventariPlayer : MonoBehaviour
{
    [SerializeField]
    private Backpack m_Backpack;

    [SerializeField]
    private GameEvent m_BackpackCanviat;

    public void Utilitzar(Objecte objecte)
    {
        if (m_Backpack.Utilitzar(objecte))
        {
            Debug.Log("Hauria de tornar a pintar el backpack");
            m_BackpackCanviat.Raise();
        }
            
    }

    //Exemple save ScriptableObject
    [SerializeField]
    private List<Backpack.ItemSlot> m_Slots;
    [SerializeField]
    private List<Backpack.ItemSlot> m_LoadedSlots;
    [SerializeField]
    private BaseDadesObjectes m_BaseDadesObjectes;
    [SerializeField]
    private Objecte m_ObjecteAfegir;

    private void Start()
    {
        m_Slots = m_Backpack.slots;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            byte[] serializedData = SerializationUtility.SerializeValue<List<Backpack.ItemSlot>>(m_Slots, DataFormat.JSON);
            string base64 = System.Convert.ToBase64String(serializedData);

            File.WriteAllText("savebackpack.json", base64);
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            try
            {
                string newBase64 = File.ReadAllText("savebackpack.json");
                byte[] serializedData = System.Convert.FromBase64String(newBase64);

                m_LoadedSlots = SerializationUtility.DeserializeValue<List<Backpack.ItemSlot>>(serializedData, DataFormat.JSON);
                //Es trenquen les refer�ncies als SO. Per tant les tornem a fer.
                //tenim una esp�cie de base de dades d'objectes i amb la clau (nom)
                //accedim a aquesta i les anem posant.
                //Aquest m�tode el podria tenir el propi ItemSlot perqu� la BD pot ser
                //perfectament Singleton ja que nom�s hi ha una i no cal la refer�ncia
                //que fem aqu� amb m_BaseDadesObjectes.
                foreach (Backpack.ItemSlot slot in m_LoadedSlots)
                {
                    slot.objecte = m_BaseDadesObjectes.Get(slot.nom);
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            m_Backpack.slots = m_LoadedSlots;
            m_BackpackCanviat.Raise();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            m_Backpack.Afegir(m_ObjecteAfegir);
            m_BackpackCanviat.Raise();
        }
    }
}