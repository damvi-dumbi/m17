using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIItemClick : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_Text;
    [SerializeField]
    private EventObjecte m_Event;

    public void Load(Objecte item)
    {
        m_Text.text = item.nom;
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(() => RaiseEvent(item));
    }

    private void RaiseEvent(Objecte item)
    {
        m_Event.Raise(item);
    }
}
