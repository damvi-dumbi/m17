using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIBackpack : MonoBehaviour
{
    [SerializeField]
    private Backpack m_Backpack;
    [SerializeField]
    private GameObject m_BackpackItem;

    private void Start()
    {
        ReloadBackpack();
    }

    public void ReloadBackpack()
    {
        Debug.Log("netejant");
        //netegem els previs botons
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        Debug.Log("creant botons");
        //creem els nous botons com a fills
        foreach (Backpack.ItemSlot slot in m_Backpack.slots)
        {
            Debug.Log(string.Format("creant bot� per a {0}",slot.objecte.nom));
            GameObject button = Instantiate(m_BackpackItem, transform);
            button.GetComponent<GUIItemClick>().Load(slot.objecte);
        }
    }
}
