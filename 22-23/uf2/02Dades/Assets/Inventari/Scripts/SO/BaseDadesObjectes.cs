using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "BaseDadesObjectes", menuName = "Objectes/BaseDadesObjectes")]
public class BaseDadesObjectes : ScriptableObject
{
    public List<Objecte> objectes;

    public Objecte Get(string nom)
    {
        return objectes.FirstOrDefault(objecte => objecte.nom == nom);
    }
}
