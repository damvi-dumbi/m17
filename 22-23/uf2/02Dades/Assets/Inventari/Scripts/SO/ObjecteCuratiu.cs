using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.FullSerializer;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjecteCuratiu", menuName = "Objectes/Objecte Curatiu")]
public class ObjecteCuratiu : Objecte
{
    public int recovery;
    public override bool Use()
    {
        Debug.Log(string.Format("Utilitzant {0}, curant {1} punts de vida", nom, recovery));
        return true;
    }
}
