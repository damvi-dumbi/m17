using UnityEngine;

[CreateAssetMenu(fileName = "EventObjecte", menuName = "GameEvent/GameEvent - Objecte")]
public class EventObjecte : GameEvent<Objecte> { }
