using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjecteDeMal", menuName = "Objectes/Objecte De Mal")]
public class ObjecteDeMal : Objecte
{
    public int damage;
    public override bool Use()
    {
        Debug.Log(string.Format("Utilitzant {0}, fent {1} punts de mal", nom, damage));
        return true;
    }
}
