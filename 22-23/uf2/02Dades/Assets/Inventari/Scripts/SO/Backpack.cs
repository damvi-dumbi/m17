using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using Unity.VisualScripting.ReorderableList.Element_Adder_Menu;
using UnityEngine;
using static UnityEditor.Progress;

[CreateAssetMenu(fileName = "Backpack", menuName = "Objectes/Backpack")]
public class Backpack : ScriptableObject
{
    [Serializable]
    public class ItemSlot
    {
        [SerializeField] public Objecte objecte;
        [SerializeField] public string nom;
        [SerializeField] public int quantitat;

        public ItemSlot(Objecte obj)
        {
            objecte = obj;
            nom = obj.nom;
            quantitat = 1;
        }
    }

    [SerializeField] public List<ItemSlot> slots;

    /*
     * return true si s'han eliminat objectes.
     * return false si segueix igual.
     */
    public bool Utilitzar(Objecte objecte)
    {
        ItemSlot item = slots.FirstOrDefault(slot => slot.objecte == objecte);

        if (item != null)
        {
            if (item.objecte.Use())
            {
                item.quantitat--;
                Debug.Log(string.Format("Queden {0} unitats de {1}", item.quantitat, item.objecte.nom));
                if (item.quantitat == 0)
                {
                    Debug.Log(string.Format("Esborrant {0} de l'inventari", item.objecte.nom));
                    return slots.Remove(item);
                }
            }
        }
        return false;
        /*
        for (int i = 0; i < slots.Length; ++i)
        {
            if (slots[i].objecte == objecte)
            {
                if (slots[i].objecte.Use())
                {
                    slots[i].quantitat--;
                    Debug.Log(string.Format("Queden {0} unitats de {1}", slots[i].quantitat, slots[i].objecte.nom));
                    if (slots[i].quantitat == 0)
                    {
                        Debug.Log(string.Format("Esborrant {0} de l'inventari", slots[i].objecte.nom));
                        slots = slots.Where(item => item.objecte != objecte).ToArray();
                    }
                }
                return false;
            }
        }
        return true;
        */
    }
    public void Afegir(Objecte objecte)
    {
        Debug.Log(string.Format("Afegint {0}", objecte.nom));
        ItemSlot item = slots.FirstOrDefault(slot => slot.objecte == objecte);
        if (item != null)
        {
            item.quantitat++;
        }
        else
        {
            slots.Add(new ItemSlot(objecte));
        }
    }
}
