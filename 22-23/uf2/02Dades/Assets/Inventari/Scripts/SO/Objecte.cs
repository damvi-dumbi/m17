using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Objecte : ScriptableObject
{
    public string nom;
    public abstract bool Use();
}
