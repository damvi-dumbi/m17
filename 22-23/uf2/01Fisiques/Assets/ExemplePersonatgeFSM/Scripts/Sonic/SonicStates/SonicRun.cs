using UnityEngine;
using FiniteStateMachine;

public class SonicRun : SonicMovement
{
    public SonicRun(FSM fSM, string movementAnimation, float movementSpeed)
        : base(fSM, movementAnimation, movementSpeed)
    {
    }
}
