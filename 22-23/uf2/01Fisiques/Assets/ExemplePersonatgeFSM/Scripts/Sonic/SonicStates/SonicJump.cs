using UnityEngine;
using FiniteStateMachine;

public class SonicJump : SonicMovement
{
    float m_JumpForce;
    float m_MinimumSpeed;

    public SonicJump(FSM fSM, string movementAnimation, float movementSpeed, float jumpForce)
        : base(fSM, movementAnimation, movementSpeed)
    {
        m_JumpForce = jumpForce;
        m_MinimumSpeed = movementSpeed;
    }

    public override void Init()
    {
        base.Init();

        if(Mathf.Abs(m_RigidBody.velocity.x) > m_MovementSpeed)
            m_MovementSpeed = Mathf.Abs(m_RigidBody.velocity.x);
        else
            m_MovementSpeed = m_MinimumSpeed;

        if (m_RigidBody.velocity.y == 0)
            m_RigidBody.AddForce(Vector2.up*m_JumpForce, ForceMode2D.Impulse);
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_RigidBody.velocity.y < 0 && m_SonicBehaviour.CheckGround().collider != null)
        {
            if (m_SonicBehaviour.Actions.Land.Movement.ReadValue<float>() != 0)
                m_FSM.ChangeState<SonicWalk>();
            else
                m_FSM.ChangeState<SonicIdle>();
        }
    }
}
