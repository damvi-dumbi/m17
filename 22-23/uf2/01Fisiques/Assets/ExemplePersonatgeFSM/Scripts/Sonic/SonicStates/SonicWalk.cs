using UnityEngine;
using FiniteStateMachine;
using System.Collections;

public class SonicWalk : SonicMovement
{
    float m_MaxWalkTime;
    Coroutine m_WalkCoroutine;

    public SonicWalk(FSM fSM, string movementAnimation, float movementSpeed, float maxWalkTime)
        : base(fSM, movementAnimation, movementSpeed)
    {
        m_MaxWalkTime = maxWalkTime;
    }

    public override void Init()
    {
        base.Init();

        if (Mathf.Abs(m_RigidBody.velocity.x) > m_MovementSpeed)
        {
            m_FSM.ChangeState<SonicRun>();
            return;
        }

        m_WalkCoroutine = m_SonicBehaviour.StartCoroutine(StartRunning());
    }

    public override void Exit()
    {
        base.Exit();

        m_SonicBehaviour.StopCoroutine(m_WalkCoroutine);
    }

    private IEnumerator StartRunning()
    {
        yield return new WaitForSeconds(m_MaxWalkTime);
        m_FSM.ChangeState<SonicRun>();
    }
}
