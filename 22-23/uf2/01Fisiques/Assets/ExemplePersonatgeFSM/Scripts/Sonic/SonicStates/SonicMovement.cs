using UnityEngine;
using FiniteStateMachine;
using UnityEngine.InputSystem;

public abstract class SonicMovement : State
{
    protected SonicBehaviour m_SonicBehaviour;
    protected Rigidbody2D m_RigidBody;
    protected SpriteRenderer m_SpriteRenderer;
    protected string m_MovementAnimation;
    protected float m_MovementSpeed;

    public SonicMovement(FSM fSM, string movementAnimation, float movementSpeed)
        : base(fSM)
    {
        m_SonicBehaviour = fSM.Owner.GetComponent<SonicBehaviour>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody2D>();
        m_SpriteRenderer = fSM.Owner.GetComponent<SpriteRenderer>();
        m_MovementAnimation = movementAnimation;
        m_MovementSpeed = movementSpeed;
    }

    public override void Init()
    {
        base.Init();
        m_SonicBehaviour.Actions.Land.Movement.canceled += StopMovement;
        m_SonicBehaviour.Actions.Land.Jump.performed += StartedJumping;

        m_FSM.Owner.GetComponent<Animator>().Play(m_MovementAnimation);
    }

    public override void Exit()
    {
        base.Exit();
        m_SonicBehaviour.Actions.Land.Movement.canceled -= StopMovement;
        m_SonicBehaviour.Actions.Land.Jump.performed -= StartedJumping;
    }

    private void StopMovement(InputAction.CallbackContext context)
    {
        m_RigidBody.velocity = new Vector2(0, m_RigidBody.velocity.y);
        if (m_RigidBody.velocity.y == 0)
            m_FSM.ChangeState<SonicIdle>();
        else
            m_FSM.ChangeState<SonicJump>();
    }

    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<SonicJump>();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        float horizontalAxis = m_SonicBehaviour.Actions.Land.Movement.ReadValue<float>();
        if (horizontalAxis != 0f)
        {
            if(horizontalAxis > 0)
            {
                m_RigidBody.velocity = new Vector2(m_MovementSpeed, m_RigidBody.velocity.y);
                m_SpriteRenderer.flipX = false;
            }
            else
            {
                m_RigidBody.velocity = new Vector2(-m_MovementSpeed, m_RigidBody.velocity.y);
                m_SpriteRenderer.flipX = true;
            }
        }
    }
}
