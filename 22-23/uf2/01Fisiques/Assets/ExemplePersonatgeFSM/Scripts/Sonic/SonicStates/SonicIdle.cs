using UnityEngine;
using FiniteStateMachine;
using Unity.VisualScripting.ReorderableList;
using UnityEngine.InputSystem;
using UnityEngine.XR;

public class SonicIdle : State
{
    SonicBehaviour m_SonicBehaviour;

    string[] m_IdleAnimations;

    public SonicIdle(FSM fSM, string[] idleAnimations)
        : base(fSM)
    {
        m_SonicBehaviour = fSM.Owner.GetComponent<SonicBehaviour>();
        m_IdleAnimations = idleAnimations;
    }

    public override void Init()
    {
        base.Init();

        m_SonicBehaviour.Actions.Land.Jump.performed += StartedJumping;

        m_FSM.Owner.GetComponent<Animator>().Play(m_IdleAnimations[Random.Range(0, m_IdleAnimations.Length)]);
    }

    public override void Exit()
    {
        base.Exit();
        m_SonicBehaviour.Actions.Land.Jump.performed -= StartedJumping;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_SonicBehaviour.Actions.Land.Movement.ReadValue<float>() != 0)
            m_FSM.ChangeState<SonicWalk>();
    }

    private void StartedJumping(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<SonicJump>();
    }
}
