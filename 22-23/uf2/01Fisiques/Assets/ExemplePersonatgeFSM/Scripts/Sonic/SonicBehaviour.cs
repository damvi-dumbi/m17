using UnityEngine;
using FiniteStateMachine;

public class SonicBehaviour : MonoBehaviour
{
    FSM m_FSM;
    SonicActions m_Actions;
    public SonicActions Actions => m_Actions;

    [SerializeField]
    private LayerMask m_LayerMask;

    [SerializeField]
    private float m_WalkSpeed = 1f;
    [SerializeField]
    private float m_TimeToRun = 1f;
    [SerializeField]
    private float m_RunSpeed = 2f;
    [SerializeField]
    private float m_JumpForce = 3f;

    void Awake()
    {
        m_Actions = new SonicActions();
        m_Actions.Land.Enable();

        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new SonicIdle(m_FSM, new string[] {"Idle", "IdleTap"} ));
        m_FSM.AddState(new SonicWalk(m_FSM, "Walk", m_WalkSpeed, m_TimeToRun));
        m_FSM.AddState(new SonicRun(m_FSM, "Run", m_RunSpeed));
        m_FSM.AddState(new SonicJump(m_FSM, "Jump", m_WalkSpeed, m_JumpForce));

        m_FSM.ChangeState<SonicIdle>();
    }

    public RaycastHit2D CheckGround()
    {
        return Physics2D.Raycast(transform.position, Vector2.down, GetComponent<CircleCollider2D>().radius * 1.1f, m_LayerMask);
    }

    void Update()
    {
        m_FSM.Update();
    }

    void FixedUpdate()
    {
        m_FSM.FixedUpdate();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        m_FSM.OnCollisionEnter2D(collision);
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        m_FSM.OnCollisionStay2D(collision);
    }
    void OnCollisionExit2D(Collision2D collision)
    {
        m_FSM.OnCollisionExit2D(collision);
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        m_FSM.OnTriggerEnter2D(collider);
    }
    void OnTriggerStay2D(Collider2D collider)
    {
        m_FSM.OnTriggerStay2D(collider);
    }
    void OnTriggerExit2D(Collider2D collider)
    {
        m_FSM.OnTriggerExit2D(collider);
    }
}
