using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using Unity.Burst.Intrinsics;

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    private enum States { IDLE, PUNCH, SHOOT }
    private States m_CurrentState;

    private Animator m_Animator;

    [SerializeField]
    private GameObject m_EnergyBolt;

    bool combo = false;

    private FSM m_FSM;

    void Awake()
    {
        m_Animator = GetComponent<Animator>();

        //m�quina d'estats externa
        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new IdleState(m_FSM, "idle"));
        m_FSM.AddState(new CoroutineExampleState(m_FSM, "shoot", 1f));
        m_FSM.AddState(new Punch01State(m_FSM, "punch", 0.8f, 1.1f));
        m_FSM.AddState(new Punch02State(m_FSM, "shoot", 0.4f));

        m_FSM.ChangeState<IdleState>();
        
    }

    void Update()
    {
        //cas de m�quina d'estats externa
        m_FSM.Update();

        /*
         * Cas de m�quina d'estats amb switch
         * 
        */
        /*
        UpdateState(m_CurrentState);
        */

        /*
         * Com fer-ho a saco, es complica la cosa i la l�gica i acabes amb un guirigall
         * de par�metres i booleans i corutines i coses.
         * 
        if (Input.GetKeyDown(KeyCode.A))
        {
            m_Animator.Play("punch");
            transform.GetChild(0).GetComponent<HitboxHandler>().Damage = Time.frameCount;
            StartCoroutine(CorutineCombo(0.65f, 1.1f));
        }

        if (combo && Input.GetKeyDown(KeyCode.Space))
        {
            m_Animator.Play("shoot");
        }
        */
    }

    private void FixedUpdate()
    {
        //cas de m�quina d'estats externa
        m_FSM.FixedUpdate();
    }

    public void Shoot()
    {
        GameObject dispar = Instantiate(m_EnergyBolt);
        dispar.transform.position = transform.position;
        dispar.GetComponent<Rigidbody2D>().velocity = transform.right * 5f;
    }

    private IEnumerator CorutineCombo(float comboWindowStart, float comboWindowEnd)
    {
        combo = false;
        yield return new WaitForSeconds(comboWindowStart);
        combo = true;
        yield return new WaitForSeconds(comboWindowEnd);
        combo = false;
    }

    private float m_StateDeltaTime;
    private Coroutine m_ComboTimeCoroutine;

    private void ChangeState(States newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState(m_CurrentState);
        InitState(newState);
    }

    private void InitState(States initState)
    {
        m_CurrentState = initState;
        m_StateDeltaTime = 0;

        switch (m_CurrentState)
        {
            case States.IDLE:
                m_Animator.Play("idle");
                break;
            case States.PUNCH:
                m_Animator.Play("punch");
                m_ComboTimeCoroutine = StartCoroutine(CorutineCombo(0.65f, 1.1f));
                break;
            case States.SHOOT:
                m_Animator.Play("shoot");
                break;
            default:
                break;
        }
    }

    
    private void UpdateState(States updateState)
    {
        m_StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {
            case States.IDLE:
                if (Input.GetKeyDown(KeyCode.A))
                    ChangeState(States.PUNCH);
                
                break;
            case States.PUNCH:
                if (Input.GetKeyDown(KeyCode.Space) && combo)
                    ChangeState(States.SHOOT);

                else if (m_StateDeltaTime >= 1.2f)
                    ChangeState(States.IDLE);

                break;
            case States.SHOOT:
                if (m_StateDeltaTime >= 0.4f)
                    ChangeState(States.IDLE);

                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (exitState)
        {
            case States.IDLE:
                break;
            case States.PUNCH:
                StopCoroutine(m_ComboTimeCoroutine);
                break;
            case States.SHOOT:
                break;
            default:
                break;
        }
    }
    
}
