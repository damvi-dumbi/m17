using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;

public class Punch01State : State
{
    string m_Animation;
    float m_ComboTime;
    float m_StateDelta;
    float m_StateLength;

    public Punch01State(FSM fsm, string animation, float comboTime, float stateLength)
        : base(fsm)
    {
        m_Animation = animation;
        m_ComboTime = comboTime;
        m_StateLength = stateLength;
    }

    public override void Init()
    {
        base.Init();
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_StateDelta = 0;
    }

    public override void Update()
    {
        base.Update();
        m_StateDelta += Time.deltaTime;

        if(m_StateDelta >= m_ComboTime)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                m_FSM.ChangeState<Punch02State>();
                return;
            }
        }

        if(m_StateDelta >= m_StateLength)
        {
            m_FSM.ChangeState<IdleState>();
            return;
        }
    }
}
