using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.UIElements;

public class CoroutineExampleState : State
{
    string m_Animation;
    float m_StateLength;
    Coroutine m_Coroutine;

    public CoroutineExampleState(FSM fsm, string animation, float stateLength)
        : base(fsm)
    {
        m_Animation = animation;
        m_StateLength = stateLength;
    }

    public override void Init()
    {
        m_FSM.Owner.GetComponent<Animator>().Play(m_Animation);
        m_Coroutine = m_FSM.Owner.GetComponent<PlayerController>().StartCoroutine(ChangeState(m_StateLength));
    }

    public override void Exit()
    {
        base.Exit();
        m_FSM.Owner.GetComponent<PlayerController>().StopCoroutine(m_Coroutine);
    }

    private IEnumerator ChangeState(float time)
    {
        yield return new WaitForSeconds(time);
        m_FSM.ChangeState<IdleState>();
    }
}
