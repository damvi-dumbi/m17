using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObjectBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Target;

    [Tooltip("Time it takes to reach the target object")]
    [SerializeField]
    private float m_FollowDelay = 1f;
    private Vector3 m_VectorSpeed = Vector3.zero;

    [SerializeField]
    private Vector3 m_Offset;

    void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, m_Target.transform.position + m_Offset, ref m_VectorSpeed, m_FollowDelay);
    }
}
