using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class CircleBehaviour : MonoBehaviour
{
    private enum PlayerSelect { Player01, Player02 };
    [SerializeField]
    private float m_Speed = 3;
    [SerializeField]
    private float m_Jump = 5;

    private Rigidbody2D m_Rigidbody2D;
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputActionMap m_CurrentActionMap;
    private InputAction m_MovementMapping;

    [SerializeField]
    private PlayerSelect m_PlayerSelect;

    void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

        /*
         * Input System
         */ 
        //Es crea una instancia del input local,
        //ja que cada jugador usar� el seu propi input.
        m_Input = Instantiate(m_InputAsset);

        //S'agafa l'action map que usarem, en aquest cas nom�s tindrem el Land
        m_CurrentActionMap = m_Input.FindActionMap("Land");

        //Aquest cas �s exclussiu d'aquest exemple.
        //Ja que tenim dos jugadors, cal setejar quina m�scara ha d'aplicar
        //el mapa a l'hora d'agafar els inputs.
        //Recordem que tenim les mateixes entrades amb l'schema Player1 i Player2.
        //Per tant hem d'estar segurs que els controls de cada un fan refer�ncia
        //a la configuraci� de tecles pertinent.
        switch (m_PlayerSelect)
        {
            case PlayerSelect.Player01:
                m_CurrentActionMap.bindingMask = InputBinding.MaskByGroup("Player1");
                break;
            case PlayerSelect.Player02:
                m_CurrentActionMap.bindingMask = InputBinding.MaskByGroup("Player2");
                break;
        }

        //Ara simplement cal subscriure's a les diferents accions
        m_CurrentActionMap.FindAction("Jump").performed += Jump;

        //D'aquest mapping ens guardem una refer�ncia, perqu� anirem
        //accedint al seu valor per tal de gestionar el moviment.
        m_MovementMapping = m_CurrentActionMap.FindAction("Movement");
        m_MovementMapping.canceled += StopMovement;

        //Finalment, si no activem el mapping, no funcionar� res
        m_CurrentActionMap.Enable();
    }

    /*
     * Old Input System
     *
    void Update()
    {
         
        if(Input.GetKey(KeyCode.D))
        {
            m_Rigidbody2D.velocity = Vector2.right * m_Speed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            m_Rigidbody2D.velocity = Vector2.left * m_Speed;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_Rigidbody2D.velocity += Vector2.up * m_Jump;
        }
    }
    */

    
    void FixedUpdate()
    {
        Vector2 movement = m_MovementMapping.ReadValue<Vector2>();
        if (movement.x > 0)
            m_Rigidbody2D.velocity = new Vector2(m_Speed, m_Rigidbody2D.velocity.y);
        else if (movement.x < 0)
            m_Rigidbody2D.velocity = new Vector2(-m_Speed, m_Rigidbody2D.velocity.y);
    }

    
    private void StopMovement(InputAction.CallbackContext context)
    {
        Debug.Log(context);
        if(context.phase == InputActionPhase.Canceled)
        {
            m_Rigidbody2D.velocity = new Vector2(0, m_Rigidbody2D.velocity.y);
        }
    }

    [SerializeField]
    LayerMask m_LayerMask;
    private void Jump(InputAction.CallbackContext context)
    {
        Debug.Log(context);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -transform.up, GetComponent<CircleCollider2D>().radius*1.3f, m_LayerMask);
        if (hit.collider != null)
        {
            //Player Can Jump
            Debug.DrawLine(transform.position, hit.point, new Color(1, 0, 1), 2f);
            m_Rigidbody2D.AddForce(Vector2.up * m_Jump, ForceMode2D.Impulse);
        }
    }
}

