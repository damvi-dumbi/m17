using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerController : MonoBehaviour
{
    //exemples ScriptableObject
    [SerializeField]
    private ElMeuScriptable m_Vida;
    [SerializeField]
    private GameEvent m_VidaEvent;
    [SerializeField]
    private ElMeuScriptable m_Mana;
    [SerializeField]
    private GameEvent m_ManaEvent;

    //exemple Tilemap
    [SerializeField]
    private Tilemap m_Tilemap;
    [SerializeField]
    private GameObject m_Chonkus;

    //exemple mouse
    private bool m_EditMode = false;
    private GameObject m_EditModeObject;

    private void Update()
    {
        //exemple de ScriptableObject
        if (Input.GetKeyDown(KeyCode.V))
            ModificaVida(-1);

        if (Input.GetKeyDown(KeyCode.B))
            ModificaVida(1);

        if (Input.GetKeyDown(KeyCode.M))
            ModificaMana(-1);

        if (Input.GetKeyDown(KeyCode.N))
            ModificaMana(1);

        //Exemple de captura de ratol�
        if(Input.GetMouseButtonDown(1))
        {
            //coordenades pantalla
            Debug.Log(Input.mousePosition);
            //coordenades m�n segons la c�mera corresponent
            Debug.Log(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            //coordenades de casella del tilemap espec�fic
            Vector3Int coordenadesTilemap = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            Debug.Log(m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)));
            //obtenir la casella que hem clicat
            Debug.Log(m_Tilemap.GetTile(coordenadesTilemap));
            //obtenir la posici� en coordenades m�n del centre de la casella de tilemap
            Debug.Log(m_Tilemap.GetCellCenterWorld(coordenadesTilemap));
            Instantiate(m_Chonkus).transform.position = m_Tilemap.GetCellCenterWorld(coordenadesTilemap);
        }

        //Exemple de mode d'edici�
        if (Input.GetKeyDown(KeyCode.E))
        {
            m_EditMode = true;
            m_EditModeObject = Instantiate(m_Chonkus);
            Destroy(m_EditModeObject.GetComponent<Rigidbody2D>());
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            m_EditMode = false;
            Destroy(m_EditModeObject);
        }

        if(m_EditMode)
        {
            Vector3 pointerPosition = m_Tilemap.GetCellCenterWorld(m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)));
            pointerPosition.z = 0;
            m_EditModeObject.transform.position = pointerPosition;
        }
    }

    private void ModificaVida(int valor)
    {
        m_Vida.ValorActual+=valor;
        m_VidaEvent.Raise();
    }

    private void ModificaMana(int valor)
    {
        m_Mana.ValorActual += valor;
        m_ManaEvent.Raise();
    }
}
