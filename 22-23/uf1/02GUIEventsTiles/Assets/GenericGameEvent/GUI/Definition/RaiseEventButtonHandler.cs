using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RaiseEventButtonHandler<T> : MonoBehaviour
{
    [SerializeField]
    private T m_Data;
    [SerializeField]
    private GameEvent<T> m_GameEvent;

    public void RaiseEvent()
    {
        m_GameEvent.Raise(m_Data);
    }
}

public abstract class RaiseEventButtonHandler<T0, T1> : MonoBehaviour
{
    [SerializeField]
    private T0 m_Data0;
    [SerializeField]
    private T1 m_Data1;
    [SerializeField]
    private GameEvent<T0, T1> m_GameEvent;

    public void RaiseEvent()
    {
        m_GameEvent.Raise(m_Data0, m_Data1);
    }
}

public abstract class RaiseEventButtonHandler<T0, T1, T2> : MonoBehaviour
{
    [SerializeField]
    private T0 m_Data0;
    [SerializeField]
    private T1 m_Data1;
    [SerializeField]
    private T2 m_Data2;
    [SerializeField]
    private GameEvent<T0, T1, T2> m_GameEvent;

    public void RaiseEvent()
    {
        m_GameEvent.Raise(m_Data0, m_Data1, m_Data2);
    }
}

public abstract class RaiseEventButtonHandler<T0, T1, T2, T3> : MonoBehaviour
{
    [SerializeField]
    private T0 m_Data0;
    [SerializeField]
    private T1 m_Data1;
    [SerializeField]
    private T2 m_Data2;
    [SerializeField]
    private T3 m_Data3;
    [SerializeField]
    private GameEvent<T0, T1, T2, T3> m_GameEvent;

    public void RaiseEvent()
    {
        m_GameEvent.Raise(m_Data0, m_Data1, m_Data2, m_Data3);
    }
}
