using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - 2 float")]
public class GameEvent2Float : GameEvent<float, float> { }