// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
// 
// Author: Ryan Hipple
// Date:   10/04/17
// ----------------------------------------------------------------------------
// Modifying for generic parameter use

using UnityEngine.Events;
using UnityEngine;

public class GameEvent2FloatListener : GameEventListener<float, float>{ }