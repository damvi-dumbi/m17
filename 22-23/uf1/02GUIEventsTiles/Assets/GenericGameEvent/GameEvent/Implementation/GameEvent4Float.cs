using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - 4 float")]
public class GameEvent4Float : GameEvent<float, float, float, float> { }