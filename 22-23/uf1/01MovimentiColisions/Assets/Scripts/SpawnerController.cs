using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_ElementASpawnejar;

    [SerializeField]
    private Pool m_PoolElements;

    [SerializeField]
    private float m_SpawnRate = 3f;
    private float m_SpawnRateDelta = 3f;

    [SerializeField]
    private Transform[] m_SpawnPoints;

    void Start()
    {
        //m_SpawnRateDelta = m_SpawnRate;

        StartCoroutine(SpawnCoroutine());
        StartCoroutine(IncreaseDifficulty());
    }

    /*
    // Update is called once per frame
    void Update()
    {
        m_SpawnRateDelta -= Time.deltaTime;
        if(m_SpawnRateDelta <= 0f)
        {
            m_SpawnRateDelta += m_SpawnRate;
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.transform.position = transform.position;
        }
    }
    */

    IEnumerator SpawnCoroutine()
    {
        while(true)
        {
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            //GameObject spawned = m_PoolElements.GetElement();
            spawned.transform.position = m_SpawnPoints[Random.Range(0,m_SpawnPoints.Length)].position;
            yield return new WaitForSeconds(m_SpawnRate);
        }
    }

    IEnumerator IncreaseDifficulty()
    {
        while(true)
        {
            yield return new WaitForSeconds(5);
            m_SpawnRate -= 0.5f;

            if (m_SpawnRate <= 0.5f)
                m_SpawnRate = 0.5f;
        }
        
    }
}