using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleController : MonoBehaviour
{
	[SerializeField]
	private float m_MoveSpeed = 2f;
	[SerializeField]
	private float m_JumpSpeed = 10f;
    private bool m_onAir = true;
    private Rigidbody2D m_rigidBody;

	//Puntuacio
    private float m_Score;  // camp privat que utilitzem
    public float Score    // Accessor (getter) del camp anterior
    {
        get => m_Score;
    }
    //Definim un delegat per avisar que he tocat un enemic
    public delegate void EnemyHit();
    public event EnemyHit OnEnemyHit;


    void Awake()
	{
		print("m'he creat");
		m_rigidBody = GetComponent<Rigidbody2D>();
	}
	
    // Start is called before the first frame update
    void Start()
    {
        print("start, abans del primer frame");
    }

    // Update is called once per frame
    void Update()
    {
		if(Input.GetKey(KeyCode.A))
		{
			m_rigidBody.velocity = new Vector3(-m_MoveSpeed,m_rigidBody.velocity.y,0);
			//transform.position += new Vector3(0,m_Speed,0)*Time.deltaTime;
		}
		
		if(Input.GetKey(KeyCode.D))
		{
			
			m_rigidBody.velocity = new Vector3(m_MoveSpeed,m_rigidBody.velocity.y,0);
			//transform.position += new Vector3(0,m_Speed,0)*Time.deltaTime;
		}
		
		if(!m_onAir && 
			(Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W)))
		{
			m_rigidBody.velocity = new Vector3(m_rigidBody.velocity.x,m_JumpSpeed,0);;
		}

		m_Score += Time.deltaTime;
    }

	void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("COLLISION: La pilota ha entrat a una col·lisió amb " + col.gameObject);
		if(col.gameObject.tag == "Terra")
			m_onAir = false;
    }

	void OnCollisionExit2D(Collision2D col)
    {
        Debug.Log("COLLISION: La pilota ha sortit d'una col·lisió amb " + col.gameObject);
		if(col.gameObject.tag == "Terra")
			m_onAir = true;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log("TRIGGER: La pilota ha entrat a una col·lisió amb " + col.gameObject);
		//Destroy(col.gameObject);
		//Sistema amb Pool que fa active a false
		//col.gameObject.SetActive(false);
		OnEnemyHit.Invoke();
		GameManager.Instance.Score++;
    }
}
