using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIScore : MonoBehaviour
{
    [SerializeField]
    private CircleController m_Personatge;
    private TMPro.TextMeshProUGUI m_Text;

    private void Awake()
    {
        m_Text = GetComponent<TMPro.TextMeshProUGUI>();
        m_Personatge.OnEnemyHit += ColorChange;
    }

    private void Update()
    {
        m_Text.text = m_Personatge.Score.ToString();
    }

    private void ColorChange()
    {
        m_Text.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }
}
