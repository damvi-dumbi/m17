using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIGameOverScore : MonoBehaviour
{
    private void Awake()
    {
        print("la puntuaci� �s " + GameManager.Instance.Score);
        GetComponent<TextMeshProUGUI>().text = GameManager.Instance.Score.ToString();
    }
}
