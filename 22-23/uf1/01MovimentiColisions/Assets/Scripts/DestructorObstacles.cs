using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructorObstacles : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Pedra")
            Destroy(collision.gameObject);
    }
}
