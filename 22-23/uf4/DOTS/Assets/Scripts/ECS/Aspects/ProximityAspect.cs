using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

readonly partial struct ProximityAspect : IAspect
{
    private readonly Entity entity;

    //nom�s llegirem
    private readonly RefRO<WorldTransform> m_WorldTransform;
    //private readonly RefRO<ProximityTag> m_Proximity;

    //accessor
    public Entity Entity => entity;

    public bool CheckProximity(float3 position, float distanceSQ)
    {
        if(math.distancesq(position, m_WorldTransform.ValueRO.Position) <= distanceSQ)
        {
            return true;
        }
        return false;
    }

    public void EnableInRangeComponent(bool enabled, ComponentLookup<IsInRangeTag> lookup, EntityCommandBuffer ECB)
    {
        if(lookup.IsComponentEnabled(entity) != enabled)
            ECB.SetComponentEnabled<IsInRangeTag>(entity, enabled);
    }
}
