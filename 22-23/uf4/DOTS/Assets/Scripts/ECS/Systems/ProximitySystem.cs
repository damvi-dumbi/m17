using Unity.Burst;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

[BurstCompile]
[UpdateAfter(typeof(MovementSystem))]
[UpdateBefore(typeof(SpawnerSystem))]
partial struct ProximitySystem : ISystem
{
    ComponentLookup<IsInRangeTag> m_Lookup;

    [BurstCompile]
    public void OnCreate(ref SystemState state)
    {
        m_Lookup = state.GetComponentLookup<IsInRangeTag>();
    }

    [BurstCompile]
    public void OnDestroy(ref SystemState state)
    {

    }

    [BurstCompile]
    public void OnUpdate(ref SystemState state)
    {
        var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        EntityCommandBuffer ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

        //Agafar valors del player
        float playerDistanceSQ = PlayerBehaviour.Instance.RangeRadiusSQ;
        float3 playerPosition = PlayerBehaviour.Instance.Position;

        m_Lookup.Update(ref state);

        ProximityJob proximityJob = new ProximityJob
        {
            ECB = ecb,
            distanceSQ = playerDistanceSQ,
            position = playerPosition,
            lookup = m_Lookup
        };

        proximityJob.Schedule();
    }
}

[BurstCompile]
partial struct ProximityJob : IJobEntity
{
    public EntityCommandBuffer ECB;
    public float distanceSQ;
    public float3 position;
    public ComponentLookup<IsInRangeTag> lookup;

    void Execute(in ProximityAspect proximity, in ProximityTag tag)
    {
        if(proximity.CheckProximity(position, distanceSQ))
        {
            proximity.EnableInRangeComponent(true, lookup, ECB);
        }
        else
        {
            proximity.EnableInRangeComponent(false, lookup, ECB);
        }
    }

}