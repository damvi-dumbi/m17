using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

class SpawnerAuthoring : MonoBehaviour
{
    public GameObject prefab;
    public float spawnRate;

    [Header("Random")]
    public bool useSeed = false;
    public ushort seed = 1;

    class SpawnerBaker : Baker<SpawnerAuthoring>
    {
        public override void Bake(SpawnerAuthoring authoring)
        {
            AddComponent(new Spawner
            {
                entityPrefab = GetEntity(authoring.prefab),
                spawnRate = authoring.spawnRate,
                elapsedTime = authoring.spawnRate,
                random = authoring.useSeed ?
                      new Unity.Mathematics.Random(authoring.seed)
                    : new Unity.Mathematics.Random((ushort)UnityEngine.Random.Range(0, 65536))
            });
        }
    }
}

public struct Spawner : IComponentData
{
    public Entity entityPrefab;
    public float spawnRate;
    //Com ara tenim sistemes i les entitats guarden les dades,
    //el temps d'spawn ser� una dada a desar al seu component
    public float elapsedTime;
    //Per a fer random, cal desar l'estat del random en dades
    public Unity.Mathematics.Random random;
}