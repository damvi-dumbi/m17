using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

class TagsAuthoring : MonoBehaviour
{
    class TagsAuthoringBaker : Baker<TagsAuthoring>
    {
        public override void Bake(TagsAuthoring authoring)
        {
            AddComponent(new ProximityTag());
            AddComponent(new IsInRangeTag());
        }
    }
}

public struct ProximityTag : IComponentData
{
}

public struct IsInRangeTag : IComponentData, IEnableableComponent
{
}