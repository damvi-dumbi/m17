using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Spawner : NetworkBehaviour
{
    [SerializeField]
    private GameObject m_Cat;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
    }

    public void Update()
    {
        if(!IsOwner)
            return;

        //Aquest script funciona perqu� �s al player
        //Nom�s podeu efectuar RPC des d'un objecte que tingueu
        //ownership
        if (Input.GetKeyDown(KeyCode.G))
            SpawnCatServerRpc();
    }

    [ServerRpc]
    private void SpawnCatServerRpc()
    {
        Debug.Log("Spawning cat on server");

        GameObject cat = Instantiate(m_Cat);
        cat.GetComponent<NetworkObject>().Spawn();
    }
}
