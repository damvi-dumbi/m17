using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerBehaviourSimple : NetworkBehaviour
{
    //Variable senzilla, la pot updatejar el client -> Aix� no s'hauria de fer "que fan trampes"
    NetworkVariable<float> m_Speed = new NetworkVariable<float>(1, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Owner);

    // No es recomana fer servir perqu� estem en el m�n de la xarxa
    void Start()
    {
    }

    // Aix� s� que seria el nou awake
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        //Aquest awake nom�s per a qui li pertany, perqu� tocarem variables on nom�s
        //nosaltres podem modificar el seu valor
        if (!IsOwner)
            return;

        m_Speed.Value = Random.Range(1f, 5f);
    }

    
    void Update()
    {
        //Aquest update nom�s per a qui li pertany
        if (!IsOwner)
            return;

        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement += Vector3.up;
        if (Input.GetKey(KeyCode.S))
            movement -= Vector3.up;
        if (Input.GetKey(KeyCode.A))
            movement -= Vector3.right;
        if (Input.GetKey(KeyCode.D))
            movement += Vector3.right;

        //i com podem veure, aix� no funciona al client perqu� estem modificant la transform
        //que en principi nom�s la pot tocar el servidor ja que tenim el component de compartir
        //la transform via network. I recordem que: EL CLIENT NO HA DE TENIR PERM�S, "que fa trampes"
        transform.position += movement.normalized * m_Speed.Value * Time.deltaTime;
    }
}
