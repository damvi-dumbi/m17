using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerBehaviour : NetworkBehaviour
{
    //Variable senzilla, per defecte el client no la pot updatejar
    //recordem que aquestes variables nom�s poden ser de DATA
    NetworkVariable<float> m_Speed = new NetworkVariable<float>(1);

    Rigidbody2D m_Rigidbody;
    // No es recomana fer servir perqu� estem en el m�n de la xarxa
    // per� per initialitzar components i tal, s�
    void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
    }

    // Aix� s� que seria el nou awake
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        //Aquest awake nom�s per a qui li pertany, perqu� tocarem variables on nom�s
        //a nosaltres ens interessa llegir el seu valor
        if (!IsOwner)
            return;

        //Si no la podem updatejar, com ho fem aleshores?
        //Li demanem al servidor que ho faci via un RPC
        //Per a mostrar el resultat al nostre client, utilitzarem
        //els callback de modificaci�
        m_Speed.OnValueChanged += CallbackModificacio;
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        m_Speed.OnValueChanged -= CallbackModificacio;
    }

    //Sempre tindran aquest format amb oldValue i newValue
    private void CallbackModificacio(float oldValue, float newValue)
    {
        Debug.Log(string.Format("{0} => M'han modificat la velocitat i ara �s {1} en comptes de {2}", OwnerClientId, newValue, oldValue));
    }

    void Update()
    {
        //Aquest update nom�s per a qui li pertany
        if (!IsOwner)
            return;

        //Demanem al servidor que modifiqui la variable. Perqu� nosaltres no en som els propietaris
        if (Input.GetKeyDown(KeyCode.Space))
            ChangeSpeedServerRpc(new ServerRpcParams());

        //Com a servidor, enviem un missatge als nostres clients.
        //Si es vol, es pot passar com a par�metre els ClientRpcParams, del qual al seu send
        //es poden posar les ID de client que volem que rebin el missatge.
        //Alerta, aquest codi nom�s el pot invocar el servidor i far� que s'executi a tots els clients
        if (Input.GetKeyDown(KeyCode.M))
            SendMessageClientRpc("Aix� �s un missatge pels clients");


        //moviment a deltaTime
        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement += Vector3.up;
        if (Input.GetKey(KeyCode.S))
            movement -= Vector3.up;
        if (Input.GetKey(KeyCode.A))
            movement -= Vector3.right;
        if (Input.GetKey(KeyCode.D))
            movement += Vector3.right;

        //Qui far� els moviments ser� el servidor, alleugerim i nom�s canvis quan hi hagi input
        if(movement != Vector3.zero)
            MoveCharacterServerRpc(transform.position + movement.normalized * m_Speed.Value * Time.deltaTime, new ServerRpcParams());

        //moviment a f�sica
        movement = Vector3.zero;

        if (Input.GetKey(KeyCode.I))
            movement += Vector3.up;
        if (Input.GetKey(KeyCode.K))
            movement -= Vector3.up;
        if (Input.GetKey(KeyCode.J))
            movement -= Vector3.right;
        if (Input.GetKey(KeyCode.L))
            movement += Vector3.right;

        //Qui far� els moviments ser� el servidor, alleugerim i nom�s canvis quan hi hagi input
        MoveCharacterPhysicsServerRpc(movement.normalized * m_Speed.Value);

    }

    //De nou, nom�s data i tipus base com a par�metres.
    //Tamb� es pot enviar aquest ServerRpcParams que ens proporcionar� la ID del client
    [ServerRpc]
    private void ChangeSpeedServerRpc(ServerRpcParams serverRpcParams = default)
    {
        m_Speed.Value = Random.Range(1f, 5f);
        Debug.Log(string.Format("{0} => El client {1} vol modificar la velocitat {2}", OwnerClientId, serverRpcParams.Receive.SenderClientId, m_Speed.Value));
        
        //Client RPC target a nom�s les ID indicades
        SendMessageClientRpc("Aix� �s un missatge pel client 1" + serverRpcParams.Receive.SenderClientId,
                    new ClientRpcParams
                    {
                        Send = new ClientRpcSendParams
                        {
                            TargetClientIds = new ulong[] { serverRpcParams.Receive.SenderClientId }
                        }
                    }
                );
    }

    //Com veiem, controlar el moviment per servidor �s horrible si hem de treballar amb deltaTime
    //en aquest cas tenim dues opcions:
    //  A) Podem descarregar i instal�lar els m�duls addicionals de MP per a que els clients
    //      puguin modificar les seves transform
    //  B) Ad�u deltaTime i treballem per f�siques amb valors a la velocitat o acceleracions
    //      els c�lculs els seguiria fent el servidor via rpc (caldria el component networkrigidbody/2d)
    [ServerRpc]
    private void MoveCharacterServerRpc(Vector3 newPosition, ServerRpcParams serverRpcParams = default)
    {
        Debug.Log(string.Format("{0} => El client {1} vol modificar la posici�", OwnerClientId, serverRpcParams.Receive.SenderClientId));
        transform.position = newPosition;
    }

    //Aix� seria el moviment a f�sica
    [ServerRpc]
    private void MoveCharacterPhysicsServerRpc(Vector3 velocity, ServerRpcParams serverRpcParams = default)
    {
        m_Rigidbody.velocity = velocity;
    }

    //Funci� que nom�s ser� executada als clients
    [ClientRpc]
    private void SendMessageClientRpc(string message, ClientRpcParams clientRpcParams = default)
    {
        Debug.Log(string.Format("{0} => {1}", OwnerClientId, message));
    }
}
