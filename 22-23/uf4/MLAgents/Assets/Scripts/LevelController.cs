using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.VisualScripting;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField]
    private Despawner m_Despawner;

    [SerializeField]
    private float m_SpawnRate = 3f;

    [SerializeField]
    private GameObject m_OstaclePrefab;

    [SerializeField]
    private Transform[] m_SpawnPoints;

    [SerializeField]
    private PlayerController m_Player;

    public delegate void GUI();
    public event GUI OnGUI;


    private List<GameObject> m_SpawnedObstacles;
    private int m_Score = 0;
    public int Score => m_Score;

    void Awake()
    {
        m_SpawnedObstacles = new List<GameObject>();
        m_Player.OnResetSimulation += ResetLevel;
        m_Player.OnObstacleEvasion += IncreaseScore;
        m_Despawner.OnDestroyObstacle += DestroyObstacle;
    }

    private void ResetLevel()
    {
        StopAllCoroutines();

        foreach (GameObject obstacle in m_SpawnedObstacles)
            Destroy(obstacle);

        m_SpawnedObstacles.Clear();

        m_Score = 0;
        OnGUI?.Invoke();

        StartCoroutine(SpawnObstacles());
    }

    private void IncreaseScore()
    {
        m_Score++;
        OnGUI?.Invoke();
    }

    private IEnumerator SpawnObstacles()
    {
        while(true)
        {
            /*
             * Aix� amb un pool seria millor, sobretot per la quantitat de simulacions
             * per segon simult�nies que est� fent la ML al aprendre.
            */ 
            GameObject obstacle = Instantiate(m_OstaclePrefab, transform);
            obstacle.transform.position = m_SpawnPoints[Random.Range(0, m_SpawnPoints.Length)].position;
            obstacle.GetComponent<Rigidbody2D>().velocity = Vector2.down * 3f;
            m_SpawnedObstacles.Add(obstacle);

            yield return new WaitForSeconds(m_SpawnRate);
        }
    }

    private void DestroyObstacle(GameObject obstacle)
    {
        m_SpawnedObstacles.Remove(obstacle);
        Destroy(obstacle);
    }
}
