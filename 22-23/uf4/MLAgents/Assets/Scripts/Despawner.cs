using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawner : MonoBehaviour
{
    public delegate void DestroyObstacle(GameObject obstacle);
    public event DestroyObstacle OnDestroyObstacle;

    private void OnTriggerExit2D(Collider2D collision)
    {
        OnDestroyObstacle?.Invoke(collision.gameObject.transform.parent.gameObject);
    }
}
