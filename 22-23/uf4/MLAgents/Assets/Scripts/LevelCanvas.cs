using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelCanvas : MonoBehaviour
{
    private TextMeshProUGUI m_Text;

    [SerializeField]
    private LevelController m_LevelController;

    void Awake()
    {
        m_Text = GetComponent<TextMeshProUGUI>();
        m_LevelController.OnGUI += UpdateScore;
    }

    private void UpdateScore()
    {
        m_Text.text = "score: " + m_LevelController.Score;
    }

}
