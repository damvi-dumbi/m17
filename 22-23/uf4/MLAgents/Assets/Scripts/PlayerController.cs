using System.Collections;
using System.Collections.Generic;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Integrations.Match3;
using Unity.MLAgents.Sensors;
using UnityEngine;
using UnityEngine.Rendering;

public class PlayerController : Agent
{
    public bool DEBUG = false;

    public delegate void ResetSimulation();
    public event ResetSimulation OnResetSimulation;

    public delegate void ObstacleEvasion();
    public event ObstacleEvasion OnObstacleEvasion;

    private Vector3 m_InitialPosition;
    private Rigidbody2D m_RigidBody;
    private CircleCollider2D m_CircleCollider;
    private float m_Speed = 4f;
    [SerializeField]
    private LayerMask m_RaycastMask;



    private void Awake()
    {
        m_InitialPosition = transform.position;
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_CircleCollider = GetComponent<CircleCollider2D>();
    }

    private void ResetPlayer()
    {
        transform.position = m_InitialPosition;
        m_RigidBody.velocity = Vector2.zero;

        OnResetSimulation?.Invoke();
    }

    /*
     * Cada cop que es reinic�i la simulaci� 
    */
    public override void OnEpisodeBegin()
    {
        ResetPlayer();
    }

    /*
     * Quines dades hem d'enviar per a ser observades
     * i per tant que ompli les accions.
     * Les dades es posen a un vector d'observacions
     * en format float.
    */
    public override void CollectObservations(VectorSensor sensor)
    {
        //Farem raycasts per a obtenir informaci� que enviarem
        //en el nostre cas les observacions seran 4
        //  [0] dist�ncia al l�mit lateral esquerra
        //  [1] dist�ncia al l�mit lateral dreta
        //  [2] dist�ncia vertical fins a topar amb obstacle des esquerra nau
        //  [3] dist�ncia vertical fins a topar amb obstacle des dreta nau

        //Nota: les dist�ncies dreta i esquerra podrien ser nom�s redu�des
        //a dir al posici� relativa X que t� dins la pantalla. Aix� s'estalvia
        //un valor m�s del vector.
        //Amb les dist�ncies verticals, podria enviar-se nom�s una des del centre
        //i acabaria tamb� arribant a conclusions. Estalviant un altre valor.

        //obtenim les posicions des d'on disparar els Raycast
        Vector2 left = new Vector2(transform.position.x - m_CircleCollider.radius, transform.position.y);
        Vector2 right = new Vector2(transform.position.x + m_CircleCollider.radius, transform.position.y);
        RaycastHit2D hit;
        float distance;

        //[0] dist�ncia al l�mit lateral esquerra
        hit = Physics2D.Raycast(left, Vector2.left, 20f, m_RaycastMask);
        if (hit.collider)
            distance = hit.distance;
        else
            distance = 0f;
        sensor.AddObservation(distance);
        if(DEBUG) Debug.DrawLine(left, hit.point);

        //[1] dist�ncia al l�mit lateral dreta
        hit = Physics2D.Raycast(right, Vector2.right, 20f, m_RaycastMask);
        if (hit.collider)
            distance = hit.distance;
        else
            distance = 0f;
        sensor.AddObservation(distance);
        if (DEBUG) Debug.DrawLine(right, hit.point);

        //[2] dist�ncia vertical fins a topar amb obstacle des esquerra nau
        hit = Physics2D.Raycast(left, Vector2.up, 10f, m_RaycastMask);
        
        if (hit.collider)
            distance = hit.distance;
        else
            distance = 10f;

        sensor.AddObservation(distance);
        if (DEBUG) Debug.DrawLine(left, left + Vector2.up*distance);

        //[3] dist�ncia vertical fins a topar amb obstacle des dreta nau
        hit = Physics2D.Raycast(right, Vector2.up, 10f, m_RaycastMask);
        
        if (hit.collider)
            distance = hit.distance;
        else
            distance = 10f;

        sensor.AddObservation(distance);
        if (DEBUG) Debug.DrawLine(right, right + Vector2.up * distance);

        /*
         * Segons bones pr�ctiques, els valors haurien de
         * ser sempre entre 0~1 o -1~1.
         * En aquest exemple no ho fem, per�
         * per a fer-ho ho fariem amb % sobre el m�xim:
         *  distance01 = distance / maxDistance;
         *  on maxDistance �s 10f en el nostre cas.
        */
    }


    /*
     * Despr�s d'enviar les dades, caldr� prendre accions.
     * Les accions poden ser continues o discretes:
     *  ActionBuffers.ContinuousActions (float[])
     *  ActionBuffers.DiscreteActions (int[])
    */
    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        //En el nostre cas nom�s volem esquerra i dreta, sense intermig
        //Com el que rebem ser� { 0, 1, 2 } (discret, 2 valors)
        //ho desplacem 1 cap a l'esquerra i ja coincideix amb la direcci�
        //que volem prendre { -1, 0, 1 }
        int direction = actionBuffers.DiscreteActions[0] - 1;
        m_RigidBody.velocity = Vector2.right * direction * m_Speed;
    }

    /*
     * Aquest �s un m�tode per fer nosaltres a m� el vector d'accions.
     * Aix� podem testejar l'Agent movent-lo nosaltres.
    */
    
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        int direction = 1;
        if (Input.GetKey(KeyCode.A))
            direction -= 1;
        if (Input.GetKey(KeyCode.D))
            direction += 1;

        ActionSegment<int> actions = actionsOut.DiscreteActions;
        actions[0] = direction;
    }

    /*
     * Si hem de fer les accions sempre, podem afegir
     * un component DecisionRequester i ser� equivalent
     * a fer-ho d'aquesta forma.
     * D'altra banda, si nom�s cal fer-les sota algunes
     * condicions, o alguns triggers. Aleshores cridariem
     * la funci� RequestDecision(); manualment
    private void FixedUpdate()
    {
        RequestDecision();
    }
    */

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "obstacle")
        {
            //C�stig amb un valor negatiu
            AddReward(-10f);
            //I seria gameOver
            EndEpisode();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "goal")
        {
            OnObstacleEvasion?.Invoke(); 
            //Donem un premi, considerablement menor al c�stig per tal
            //que no noti que li surt a compte estrellar-se
            AddReward(0.1f);
        }
            
    }
}
