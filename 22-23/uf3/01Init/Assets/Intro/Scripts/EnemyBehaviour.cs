using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.VersionControl.Asset;

public class EnemyBehaviour : MonoBehaviour
{
    private enum States { IDLE, TRACK, ATTACK };
    private States m_CurrentState;

    private float m_StateDeltaTime;
    private Coroutine m_DisparoCoroutine;

    private Transform m_Target;

    private void Start()
    {
        ChangeState(States.IDLE);
    }

    private void Update()
    {
        UpdateState(m_CurrentState);
    }

    private void ChangeState(States newState)
    {
        if (newState == m_CurrentState)
            return;

        Debug.Log("Canviant estat a " + newState);
        ExitState(m_CurrentState);
        InitState(newState);
    }

    private void InitState(States initState)
    {
        m_CurrentState = initState;
        m_StateDeltaTime = 0;

        switch (m_CurrentState)
        {
            case States.IDLE:
                break;

            case States.TRACK:

                break;
            case States.ATTACK:
                m_DisparoCoroutine = StartCoroutine(Disparar());
                break;
            default:
                break;
        }
    }


    private void UpdateState(States updateState)
    {
        m_StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {
            case States.IDLE:

                break;
            case States.TRACK:
                transform.LookAt(m_Target);
                /*
                Vector3 direccion = (m_Target.position - transform.position).normalized;
                transform.forward = direccion;
                */
                break;
            case States.ATTACK:
                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (exitState)
        {
            case States.IDLE:
                break;
            case States.TRACK:
                break;
            case States.ATTACK:
                StopCoroutine(m_DisparoCoroutine);
                break;
            default:
                break;
        }
    }

    private IEnumerator Disparar()
    {
        transform.LookAt(m_Target);
        Debug.DrawLine(transform.position, m_Target.position, Color.red, 2f);
        yield return new WaitForSeconds(3f);
        ChangeState(States.TRACK);
    }

    public void OnPlayerFollow(Transform playerTransform)
    {
        m_Target = playerTransform;
        ChangeState(States.TRACK);
    }

    public void OnPlayerAttack(Transform playerTransform)
    {
        m_Target = playerTransform;
        ChangeState(States.ATTACK);
    }

}
