using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnemyAttack : MonoBehaviour
{
    [SerializeField]
    EnemyBehaviour m_EnemyBehaviour;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_EnemyBehaviour.OnPlayerAttack(other.gameObject.transform);
        }
    }
}
