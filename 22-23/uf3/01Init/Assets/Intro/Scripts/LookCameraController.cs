using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookCameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] m_Target;

    private int m_Index = 0;

    [SerializeField]
    private Vector3 m_RelativePositionFromPlayer;

    void Update()
    {
        transform.position = m_Target[m_Index].transform.position
            + m_Target[m_Index].transform.forward * m_RelativePositionFromPlayer.z
            + m_Target[m_Index].transform.up * m_RelativePositionFromPlayer.y
            + m_Target[m_Index].transform.right * m_RelativePositionFromPlayer.x;

        transform.LookAt(m_Target[m_Index].transform.position);

        if(Input.GetKeyDown(KeyCode.C))
            m_Index = (m_Index+1)%m_Target.Length;
    }
}
