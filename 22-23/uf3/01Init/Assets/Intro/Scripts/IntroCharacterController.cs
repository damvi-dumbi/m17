using System.Collections;
using System.Collections.Generic;
using UnityEditor.Rendering;
using UnityEngine;

public class IntroCharacterController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;

    [SerializeField]
    private float m_JumpHeight;

    CharacterController m_CharacterController;
    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        Vector3 movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            movement += transform.forward;
        if (Input.GetKey(KeyCode.S))
            movement -= transform.forward;

        //strafe
        if (Input.GetKey(KeyCode.Q))
            movement -= transform.right;
        if (Input.GetKey(KeyCode.E))
            movement += transform.right;

        movement.Normalize();
        movement *= m_Speed;

        if (m_CharacterController.isGrounded && Input.GetButtonDown("Jump"))
            movement.y = m_JumpHeight;


        m_CharacterController.SimpleMove(movement);
    }
}
