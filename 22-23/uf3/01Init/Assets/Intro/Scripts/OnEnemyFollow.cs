using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnemyFollow : MonoBehaviour
{
    [SerializeField]
    EnemyBehaviour m_EnemyBehaviour;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            m_EnemyBehaviour.OnPlayerFollow(other.gameObject.transform);
        }
    }
}
