using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;
using static UnityEditor.PlayerSettings;
using Color = UnityEngine.Color;

public class IntroController : MonoBehaviour
{
    [SerializeField]
    private float m_Speed;

    [SerializeField]
    private float m_RotationSpeed;

    Vector3 m_Movement = Vector3.zero;
    private Rigidbody m_Rigidbody;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //rotate
        if (Input.GetKey(KeyCode.A))
            transform.Rotate(-Vector3.up * m_RotationSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.up * m_RotationSpeed * Time.deltaTime);

        m_Movement = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
            m_Movement += transform.forward;
        if (Input.GetKey(KeyCode.S))
            m_Movement -= transform.forward;

        //strafe
        if (Input.GetKey(KeyCode.Q))
            m_Movement -= transform.right;
        if (Input.GetKey(KeyCode.E))
            m_Movement += transform.right;
        
        m_Movement.Normalize();

        //transform.position += m_Movement * m_Speed *Time.deltaTime;

        if (Input.GetMouseButtonDown(1))
            Teleport();
    }

    private void FixedUpdate()
    {
        m_Rigidbody.MovePosition(transform.position + m_Movement * m_Speed * Time.fixedDeltaTime);
    }

    [SerializeField]
    private LayerMask layerMask;
    private void Teleport()
    {
        //trobar un raig des de la c�mera en direcci� al punt que hem clicat d'aquesta
        Vector2 mousePosition = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(mousePosition);
        Debug.DrawRay(ray.origin, ray.direction, Color.red, 5f);

        //llancem un raig segons la direcci� trobada anteriorment per a saber qu� toquem
        //des del punt de vista de l'objecte c�mera
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, Mathf.Infinity, layerMask))
        {
            Debug.DrawLine(ray.origin, raycastHit.point, Color.blue, 5f);
            if (raycastHit.collider.gameObject.layer == LayerMask.NameToLayer("Walkable"))
            {
                //Un cop veiem que el que hem tocat des la c�mera �s un objecte del tipus walkable
                //fem que el player llenci un raig en direcci� al PUNT on la c�mera ha tocat
                Vector3 direction = (raycastHit.point - transform.position).normalized;
                Debug.DrawLine(transform.position, transform.position + direction * 20f, Color.red, 5f);
                if (Physics.Raycast(transform.position, direction, out raycastHit, Mathf.Infinity, layerMask))
                {
                    Debug.DrawLine(transform.position, raycastHit.point, Color.green, 5f);
                    if (raycastHit.collider.gameObject.layer == LayerMask.NameToLayer("Walkable"))
                    {
                        //Finalment, un cop disparem des del jugador al punt, mirem qu� toca el personatge
                        //Si �s walkable, ens teleportem fent que la normal de l'impacte sigui el nostre up
                        //per a tenir els peus contra el nou terra.
                        Debug.Log("He tocat el punt " + raycastHit.point);
                        transform.up = raycastHit.normal;
                        transform.position = raycastHit.point;
                    }
                }
            }
        }
    }
}
