using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollHit : MonoBehaviour
{
    [SerializeField]
    private EnableRagdoll m_RagdollController;


    public void Hit(int damage, Vector3 impactDirection)
    {
        Debug.Log(gameObject + " has been hit for " + damage);
        m_RagdollController.ReceiveDamage(damage, gameObject, impactDirection);
    }
}
