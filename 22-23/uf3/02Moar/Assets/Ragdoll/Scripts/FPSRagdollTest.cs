using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FPSRagdollTest : MonoBehaviour
{
    [SerializeField]
    private PlayerInput m_InputActions;

    [SerializeField]
    private float m_MoveSpeed = 3;

    [SerializeField]
    private float m_Sensitivity = 5;

    [SerializeField]
    private GameObject m_FPC;

    [SerializeField]
    private bool m_InvertY = false;

    [SerializeField]
    private LayerMask m_ShootMask;
    
    private Rigidbody m_RigidBody;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        m_RigidBody = GetComponent<Rigidbody>();
        m_InputActions = new PlayerInput();
        m_InputActions.Ground.Enable();

        m_InputActions.Ground.Shoot.performed += Shoot;
        /*
        m_InputActions.Ground.Shoot.started += Shoot;
        m_InputActions.Ground.Shoot.canceled += Shoot;
        */
    }
    
    void Update()
    {
        Vector2 lookInput = m_InputActions.Ground.Look.ReadValue<Vector2>();

        transform.Rotate(Vector3.up * lookInput.x * m_Sensitivity * Time.deltaTime);
        
        m_FPC.transform.Rotate((m_InvertY ? 1 : -1) * Vector3.right * lookInput.y * m_Sensitivity * Time.deltaTime);

        Vector2 input = m_InputActions.Ground.Movement.ReadValue<Vector2>();

        m_RigidBody.MovePosition(transform.position 
            + input.x * transform.right * m_MoveSpeed * Time.deltaTime
            + input.y * transform.forward * m_MoveSpeed * Time.deltaTime);
    }

    private void Shoot(InputAction.CallbackContext context)
    {
        RaycastHit hit;
            
        if (Physics.Raycast(m_FPC.transform.position, m_FPC.transform.forward, out hit, Mathf.Infinity, m_ShootMask))
        {
            Debug.DrawLine(m_FPC.transform.position, hit.point, new Color(Random.Range(0f,1f), Random.Range(0f, 1f), Random.Range(0f, 1f)), 2f);
            hit.transform.GetComponent<RagdollHit>().Hit(Random.Range(5, 20), m_FPC.transform.forward);
        }
    }
}
