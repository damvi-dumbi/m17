using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableRagdoll : MonoBehaviour
{
    [SerializeField]
    private Animator m_Animator;

    [SerializeField]
    private GameObject m_RagdollRoot;

    [SerializeField]
    private Rigidbody[] m_RagdollRigidbodies;

    public int HP = 20;

    private void Awake()
    {
        m_RagdollRigidbodies = m_RagdollRoot.GetComponentsInChildren<Rigidbody>();
        

        ActivateRagdoll(false);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
            ActivateRagdoll(true);

        if (Input.GetKeyDown(KeyCode.T))
            ActivateRagdoll(false);
    }

    public void ActivateRagdoll(bool state)
    {
        m_Animator.enabled = !state;

        foreach (Rigidbody rb in m_RagdollRigidbodies)
            rb.isKinematic = !state;

        //El base no l'estem guardant al array
        m_RagdollRoot.GetComponent<Rigidbody>().isKinematic = !state;
    }

    public void ReceiveDamage(int damage, GameObject impactPoint, Vector3 impactDirection)
    {
        HP -= damage;
        if(HP <= 0)
        {
            ActivateRagdoll(true);
            impactPoint.GetComponent<Rigidbody>().AddForce(impactDirection.normalized*damage, ForceMode.Impulse);
        }
    }
}
