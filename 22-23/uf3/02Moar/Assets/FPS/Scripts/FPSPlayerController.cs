using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FPSPlayerController : MonoBehaviour
{
    [SerializeField]
    private PlayerInput m_InputActions;

    [SerializeField]
    private float m_MoveSpeed = 3;

    [SerializeField]
    private float m_Sensitivity = 5;

    [SerializeField]
    private GameObject m_FPC;

    [SerializeField]
    private bool m_InvertY = false;

    [SerializeField]
    private LayerMask m_ShootMask;
    [SerializeField]
    private GameObject m_Projectil;

    [SerializeField]
    private GameObject m_Misil;

    private Rigidbody m_RigidBody;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;

        m_RigidBody = GetComponent<Rigidbody>();
        m_InputActions = new PlayerInput();
        m_InputActions.Ground.Enable();

        m_InputActions.Ground.Shoot.performed += Shoot;
        m_InputActions.Ground.ShootMissile.performed += ShootMissile;
        /*
        m_InputActions.Ground.Shoot.started += Shoot;
        m_InputActions.Ground.Shoot.canceled += Shoot;
        */
    }

    void Update()
    {
        Vector2 lookInput = m_InputActions.Ground.Look.ReadValue<Vector2>();

        transform.Rotate(Vector3.up * lookInput.x * m_Sensitivity * Time.deltaTime);

        m_FPC.transform.Rotate((m_InvertY ? 1 : -1) * Vector3.right * lookInput.y * m_Sensitivity * Time.deltaTime);

        Vector2 input = m_InputActions.Ground.Movement.ReadValue<Vector2>();

        m_RigidBody.MovePosition(transform.position
            + input.x * transform.right * m_MoveSpeed * Time.deltaTime
            + input.y * transform.forward * m_MoveSpeed * Time.deltaTime);
    }

    private void Shoot(InputAction.CallbackContext context)
    {
        RaycastHit hit;
        for (int i = 0; i < 12; i++)
        {
            float rightDispersion = Random.Range(-.1f, 0.1f);
            float upDispersion = Random.Range(-.1f, 0.1f);
            Vector3 direction = m_FPC.transform.forward + rightDispersion * m_FPC.transform.right + upDispersion * m_FPC.transform.up;
            if (Physics.Raycast(m_FPC.transform.position, direction, out hit, Mathf.Infinity, m_ShootMask))
            {
                Debug.DrawLine(m_FPC.transform.position, hit.point, new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)), 2f);
                GameObject go = Instantiate(m_Projectil);
                go.transform.position = hit.point + hit.normal * 0.1f;
                go.transform.up = hit.normal;
            }
        }
    }

    private void ShootMissile(InputAction.CallbackContext context)
    {
        GameObject go = Instantiate(m_Misil);
        go.transform.position = m_FPC.transform.position + m_FPC.transform.forward*2;
        go.transform.forward = m_FPC.transform.forward;
    }
}
